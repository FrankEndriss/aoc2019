/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++)
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/*
 * e
 * se
 * sw
 * w
 * nw
 * ne
 */
vector<tuple<string,int,int>> offs= {
    {  "e",  0, 1 },
    { "se", -1, 0 },
    { "sw", -1, -1},
    {  "w",  0, -1 },
    { "nw",  1, 0 },
    { "ne",  1, 1 }
};

int cntgrid(vvi &a) {
    int ans=0;
    for(size_t i=0; i<a.size(); i++) 
        for(size_t j=0; j<a[i].size(); j++) 
            ans+=a[i][j];
    return ans;
}

void solve() {
    string gs=gcin();
    string sep("\n");
    vs a=split(gs, sep);

    map<pii,int> flip;
    for(string s : a) {
        int pos=0;
        int x=0;
        int y=0;
        while(pos<s.size()) {
            string dir;
            if(s[pos]=='s' || s[pos]=='n') {
                dir+=s[pos];
                dir+=s[pos+1];
                pos+=2;
            } else {
                dir+=s[pos];
                pos++;
            }

            int dx=-3, dy=-3;
            for(int j=0; j<offs.size(); j++)  {
                if(get<0>(offs[j])==dir) {
                    dx=get<1>(offs[j]);
                    dy=get<2>(offs[j]);
                }
            }
            assert(dx>-3);
            x+=dx;
            y+=dy;
        }
        flip[ {x,y}]^=1;
    }

    /* add conways */
    int miX=0;
    int maX=0;
    int miY=0;
    int maY=0;
    for(auto ent : flip) {
        miX=min(miX, ent.first.first);
        maX=min(maX, ent.first.first);
        miY=min(miY, ent.first.second);
        maY=max(maY, ent.first.second);
    }

    const int szx=103+maX-miX+103;
    const int szy=103+maY-miY+103;
    vvi grid(szx, vi(szy));
    for(auto ent : flip) {
        if(ent.second==1)
            grid[szx/2+ent.first.first][szy/2+ent.first.second]=1;
    }

    int ans=cntgrid(grid);
    cout<<"star1: "<<ans<<endl;

    for(int i=0; i<100; i++) {
        vvi grid2(szx, vi(szy));
        for(int x=1; x+1<szx; x++) {
            for(int y=1; y+1<szy; y++) {
                int cnt=0;
                for(int k=0; k<6; k++) {
                    int xx=x+get<1>(offs[k]);
                    int yy=y+get<2>(offs[k]);

                    cnt+=grid[xx][yy];
                }

                grid2[x][y]=grid[x][y];

                if(grid[x][y]==1 && (cnt==0 || cnt>2))
                    grid2[x][y]=0;
                else if(grid[x][y]==0 && cnt==2)
                    grid2[x][y]=1;
            }
        }

        grid.swap(grid2);
    }

    ans=cntgrid(grid);
    cout<<"star2: "<<ans<<endl;
}

signed main() {
    solve();
}
