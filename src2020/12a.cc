/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/*
 * Once again a lot of text parsing/understand.
 * After getting how the rotation work (degrees!)
 * it is kind of annoying simple.
 *
 * It seems to be a pattern in this years aoc, less coding,
 * more text understanding. I assume eric realized that
 * the main audience is beginner programmers, but on the
 * other hand must make it look like not to trivial problems.
 */
void solve() {
    string gs=gcin();
    string sep("\n");
    vs s=split(gs, sep);

    vi dx={ 0, 1, 0, -1 };
    vi dy={ 1, 0, -1, 0 };
    string dirs="ESWN";

    int dir=0;  /* East */

    int x=0;
    int y=0;
    for(size_t i=0; i<s.size(); i++) {
        istringstream iss(s[i]);
        char c;
        iss>>c;
        int num;
        iss>>num;

        //cerr<<"c="<<c<<" num="<<num<<" x="<<x<<" y="<<y<<endl;

        if(c=='F') {
            x+=dx[dir]*num;
            y+=dy[dir]*num;
        } else if(c=='E') {
            x+=dx[0]*num;
            y+=dy[0]*num;
        } else if(c=='S') {
            x+=dx[1]*num;
            y+=dy[1]*num;
        } else if(c=='W') {
            x+=dx[2]*num;
            y+=dy[2]*num;
        } else if(c=='N') {
            x+=dx[3]*num;
            y+=dy[3]*num;
        } else if(c=='L') {
            num/=90;
            dir=(dir-num+400)%4;
        } else if(c=='R') {
            num/=90;
            dir=(dir+num)%4;
        } else
            assert(false);
    }
    cerr<<"x="<<x<<" y="<<y<<endl;
    int ans=abs(x)+abs(y);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
