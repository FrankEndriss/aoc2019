/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++) 
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}



/*
 * Find smallest common multiple with offset.
 * How to do that?
 * Brute force seems to not work :/
 *
 * diophantine equations
 * We got from each pair of busses a[i],a[j] 
 * and c=j-i the eq:
 * x*a[i] + y*a[j] = c
 *
 * How to combine more than two of those?
 * If we got a solution for some 0,j that means
 * at x*a[0] departs the bus a[0], and at x*a[0]+j departs a[j]
 *
 * So we can subst a[0] with x*a[0] and repeat.
 *
 * ...
 * brute force this, foreach pair
 * we need to find the best solution for that pair in O(1),
 * and then the offset becomes 
 * offs=a[i]*a[j]/gcd(a[i],a[j])
 * so the next a[i] is offs.
 * This works fine since the offsets in minutes in the table are
 * small values.
 */
const int INF=1e18;
const int N=1e9;
void solve() {
    string s=gcin();
    subst(s, ',', ' ');
    subst(s, 'x', '0');

    vi a=splitwsi(s);

    vector<pii> aa;
    for(int i=0; i<a.size(); i++)
        if(a[i]>0)
            aa.push_back({a[i], i});

    int t=0;
    int offs=aa[0].first;
    for(size_t i=1; i<aa.size(); i++) {
        cerr<<"aa[i].first="<<aa[i].first<<" offs="<<offs<<endl;
        while(true) {
            int ti=t/aa[i].first;
            ti*=aa[i].first;
            ti-=aa[i].second;
            while(ti<t)
                ti+=aa[i].first;

            if(t<ti)
                t+=offs;

            if(t==ti)
                break;
        }
        int g=gcd(offs, aa[i].first);
        offs=offs*aa[i].first/g;
    }

    cout<<t<<endl;

}

signed main() {
    solve();
}
