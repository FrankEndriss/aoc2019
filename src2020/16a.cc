/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++)
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


vvi ga1 = {
    { 41,598,  605,974 },
    { 30,617, 625,957},
    { 29,914, 931,960},
    { 39,734, 756,972},
    { 37,894, 915,956},
    { 48,54, 70,955},
    { 39,469, 491,955},
    { 47,269, 282,949},
    { 26,500, 521,960},
    { 26,681, 703,953},
    { 49,293, 318,956},
    { 25,861, 873,973},
    { 30,446, 465,958},
    { 50,525, 551,973},
    { 39,129, 141,972},
    { 37,566, 573,953},
    { 43,330, 356,969},
    { 32,770, 792,955},
    { 47,435, 446,961},
    { 30,155, 179,957}
};

vvi ga2= {
    { 1, 3, 5, 7},
    { 6,11, 33,44},
    { 13,40,45,50}
};


vi ticket= { 71,127,181,179,113,109,79,151,97,107,53,193,73,83,191,101,89,149,103,197 };

/*
 * Star1 we need to find if a ticket is "valid".
 */
void solve() {

    string gs=gcin();
    string sep("\n");
    vs s=split(gs, sep);
    int ans=0;
    for(size_t ii=0; ii<s.size(); ii++) {
        subst(s[ii], ',', ' ');
        vi a=splitwsi(s[ii]);

        for(size_t i=0; i<a.size(); i++) {
            bool valid=false;
            for(size_t j=0; j<ga1.size(); j++) {
                if(((a[i]>=ga1[j][0]) && (a[i]<=ga1[j][1])) ||
                        ((a[i]>=ga1[j][2]) && (a[i]<=ga1[j][3]))) {
                    valid=true;
                } 
            }
            if(!valid)
                ans+=a[i];
        }
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
