
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

//#include <bits/stdc++.h>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <math.h>
#include <vector>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

bool fini=false;
tuple<int,int,char,string> parse() {
    string s;
    getline(cin,s);
    if(s.size()==0) {
        fini=true;
        return { 0,0,' ',string("") };
    }

    for(size_t i=0; i<s.size(); i++)
        if(s[i]=='-' || s[i]==':')
            s[i]=' ';

    istringstream iss(s);
    int l,r;
    iss>>l>>r;
    char c;
    iss>>c;
    string w;
    iss>>w;
    return { l-1,r-1,c,w };
}

/*
* 2-9 c: cccccccc
* Use the parser from a but
* this time check if _exactly_ one of the
* both positions contain the char.
* Note that the examples are misleading, so it
* is basically a speedreading contest.
*/
void solve() {
    string s;
    int ans=0;
    while(true) {

        auto [l,r,c,w]=parse();
        if(fini)
            break;

        int cnt=0;
        if(w[l]==c)
            cnt++;
        if(w[r]==c)
            cnt++;

        if(cnt==1)
            ans++;
    }
    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
