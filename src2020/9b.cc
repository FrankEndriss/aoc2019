/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/*
 * FIRST UNDERSTAND THE QUESTION, then code!
 */
void solve() {
    vi a;
    vi aa;
    int ans1=-1;
    for(int i=0; i<1000; i++) {
        if(a.size()>25)
            a.erase(a.begin());

        cini(aux);
        aa.push_back(aux);

        if(a.size()==25) {
            bool match=false;
            for(int j=0; j<25; j++) {
                for(int k=j+1; k<25; k++) {
                    if(a[j]!=a[k] && a[j]+a[k]==aux) {
                        match=true;
                    }
                }
            }
            if(!match && ans1<0) {
                ans1=aux;
            }
        }

        a.push_back(aux);
    }
    assert(ans1>0);
    cerr<<"ans1="<<ans1<<endl;

    int l=0;
    int sum=aa[0];
    for(size_t i=1; i<aa.size(); i++) {
        sum+=aa[i];
        while(sum>ans1) {
            sum-=aa[l];
            l++;
        }
        if(sum==ans1) {
            int mi=*min_element(aa.begin()+l, aa.begin()+i+1);
            int ma=*max_element(aa.begin()+l, aa.begin()+i+1);
            cout<<mi+ma<<endl;
            return;
        }
    }
    assert(false);
   
}

signed main() {
    solve();
}
