
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

string fi(string &s) {
    string ans;
    for(char c : s)
        if(c==':')
            return ans;
        else
            ans+=c;

    return ans;
}

/*
 * We need to implement some greedy parser.
 */
void solve() {
    const vs f= { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};

    int rcnt=1;
    int ans=0;
    int cnt=0;
    while(true) { /* whole file */
        set<string> data;

        while(true) {   /* until emptyline */
            cnt++;
            string s;
            getline(cin,s);
            if(s.size()==0)
                break;

            istringstream iss(s);
            while(true) {   /*  a line */
                string w;
                iss>>w;
                if(w.size()==0)
                    break;

                data.insert(fi(w));
            }
        }

        int fcnt=0;
        for(string ss : f)
            fcnt+=data.count(ss);

        //cerr<<"fcnt="<<fcnt<<endl;
        if(fcnt>=(int)f.size()) {
            ans++;
            cerr<<"rcnt="<<rcnt<<endl;
        }

        data.clear();
        rcnt++;

        if(cnt>1101)
            break;
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
