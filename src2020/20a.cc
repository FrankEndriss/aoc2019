/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vvvvi= vector<vvvi>;
using vs= vector<string>;
using vvs= vector<vs>;
using vvvs= vector<vvs>;
using vvvvs= vector<vvvs>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++)
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}

string trim(string s) {
    string t;
    for(char c : s)
        if(c!=' ')
            t+=c;
    return t;
}

/*
 * 144 tiles, so 12x12
 * each tile 10x10
 * We can ignore inner cells, only the edges count.
 * Top/down and left/right each stay like this to each other.
 *
 * We can dfs. Start at any tile, then search for fitting tiles
 * at one side, check all sides. Every time a tile is placed
 * somewhere, check all sides. It is dfs.
 * Let cal the 12x12 grid the biggrid
 *
 * dp[i][j] = tilepos of tile at i,j, dp[i][j].id=-1 for free
 *
 * The check:
 * A tile can be "flipped", so
 * N/S change and E and W are reversed,     (horz flip)
 * E/W change and N and S are reversed      (vert flip)
 * or "rotated",
 * so N/E/S/W -> E/S/W/N
 *
 * We need two checks:
 * One finds matching tiles from unused ones.
 * One checks two adj tilepos to each other.
 */

int getid(string line) {
    //cerr<<"getid, line="<<line<<endl;
    subst(line, ':', ' ');
    vs ss=splitws(line);
    return atol(trim(ss[1]).c_str());
}

int myhash(string str) {
    assert(str.size()==10);
    int i=0;
    for(char c : str) {
        i<<=1;
        if(c=='#')
            i|=1;
    }
    return i;
}

/* first rot, then flip. N->N=0; N->E=1; N->S=2; N->W=3;
   hflip, vflip
   So we got 16 orientations. */
using orientation= tuple<int,bool,bool>;

struct tilepos {
    int i=-1;    /* index of tile in tiles */
    orientation ori;
};

const int N=0;
const int E=1;
const int S=2;
const int W=3;

struct tile {
    vs edg;  /* N, E, S, W */
    int id; /* informal id from input */
    vvvvi memo;

    tile() {
        id=-1;
        edg.resize(4);
        memo=vvvvi(4, vvvi(4, vvi(2, vi(2, -1))));
    }
    /* return edge e with orientation ori */
    int get(int e, orientation ori) {
        auto [rot,hflip,vflip]=ori;
        if(memo[e][rot][hflip][vflip]>=0)
            return memo[e][rot][hflip][vflip];

        vs aux=edg;
        assert(aux[N].back()==aux[E][0]);
        assert(aux[S].back()==aux[E].back());
        assert(aux[N][0]==aux[W][0]);
        assert(aux[S][0]==aux[W].back());

        if(rot!=0) {
            for(int i=0; i<(4-rot); i++) {
                rotate(aux.begin(), aux.begin()+3, aux.end());
                reverse(all(aux[N]));
                reverse(all(aux[S]));
            }
        }

        assert(aux[N].back()==aux[E][0]);
        assert(aux[S].back()==aux[E].back());
        assert(aux[N][0]==aux[W][0]);
        assert(aux[S][0]==aux[W].back());

        if(hflip) {
            swap(aux[0], aux[2]);
            reverse(all(aux[1]));
            reverse(all(aux[3]));
        }
        assert(aux[N].back()==aux[E][0]);
        assert(aux[S].back()==aux[E].back());
        assert(aux[N][0]==aux[W][0]);
        assert(aux[S][0]==aux[W].back());
        if(vflip) {
            swap(aux[1], aux[3]);
            reverse(all(aux[0]));
            reverse(all(aux[2]));
        }

        assert(aux[N].back()==aux[E][0]);
        assert(aux[S].back()==aux[E].back());
        assert(aux[N][0]==aux[W][0]);
        assert(aux[S][0]==aux[W].back());
        return memo[e][rot][hflip][vflip]=myhash(aux[e]);
    }
};

const vb bval= { false, true };

void solve() {
    string gs=gcin();
    string sep("\n\n");
    vs s=split(gs, sep);

    vector<tile> tiles;
    for(size_t i=0; i<s.size(); i++) {
        sep="\n";
        vs ss=split(s[i], sep);

        tile t;
        t.id=getid(ss[0]);
        t.edg[0]=ss[1];
        t.edg[2]=ss.back();
        for(size_t k=1; k<ss.size(); k++) {
            t.edg[3]+=ss[k][0];
            t.edg[1]+=ss[k].back();
        }

        tiles.push_back(t);
        /*
        cerr<<"tile.id   ="<<t.id<<endl;
        cerr<<"tile.north="<<bitset<10>(myhash(t.edg[0]))<<endl;
        cerr<<"tile.south="<<bitset<10>(myhash(t.edg[2]))<<endl;
        cerr<<"tile.east ="<<bitset<10>(myhash(t.edg[1]))<<endl;
        cerr<<"tile.west ="<<bitset<10>(myhash(t.edg[3]))<<endl;
        for(int dir=0; dir<4; dir++) {
            for(int r=0; r<4; r++) {
                for(bool h : bval) {
                    for(bool v : bval) {
                        cerr<<"dir="<<dir<<" r="<<r<<" h="<<h<<" v="<<v<<": "<<
                            bitset<10>(t.get(dir, {r,h,v}))<<endl;
                    }
                }
            }
        }
        */
    }

    /* perhabs shuffle random would be better 
    for(int i=0; i<200; i++) {
        int i1=rand()%tiles.size();
        int i2=rand()%tiles.size();
        if(i1!=i2)
            swap(tiles[i1], tiles[i2]);
    }
     * */

     //           if(i==4 || i==29 || i==99 || i==31)
     //          NW=41 NE=106 SW=58 SE=122
     swap(tiles[143], tiles[4]);
     swap(tiles[142], tiles[29]);
     swap(tiles[141], tiles[99]);
     swap(tiles[140], tiles[31]);
     swap(tiles[139], tiles[41]);
     swap(tiles[138], tiles[106]);
     swap(tiles[137], tiles[58]);
     swap(tiles[136], tiles[122]);


    /* tilememo[str]= list of tiles having an edge str in some orientation. */
    vvi tilememo(1024*1024);
    for(size_t i=0; i<tiles.size(); i++) {
        for(int dir=0; dir<4; dir++) {
            for(int r=0; r<4; r++) {
                for(bool h : bval) {
                    for(bool v : bval) {
                        int val=tiles[i].get(dir, {r,h,v});
                        tilememo[val].push_back(i);
                    }
                }
            }
        }
    }

    vi freq(1024*1024);
    vector<set<int>> adj(12*12);
    for(size_t i=0; i<tilememo.size(); i++) {
        if(tilememo[i].size()>1) {
            auto it=unique(all(tilememo[i]));
            tilememo[i].resize(distance(tilememo[i].begin(), it));
            freq[i]=tilememo[i].size();
            if(freq[i]>=2)
                cout<<tilememo[i][0]<<" "<<tilememo[i][1]<<endl;

            for(int ii=0; ii<tilememo[i].size(); ii++)  {
                for(int jj=ii+1; jj<tilememo[i].size(); jj++) {
                    adj[tilememo[i][ii]].insert(tilememo[i][jj]);
                    adj[tilememo[i][jj]].insert(tilememo[i][ii]);
                }
            }
        }
    }
    for(int i=0; i<adj.size(); i++) {
        cerr<<"adj[], i="<<i<<" adj[i].size()="<<adj[i].size()<<": ";
        for(int j : adj[i]) 
            cerr<<j<<" ";
        cerr<<endl;
    }
     int ans=tiles[136].id * tiles[137].id * tiles[138].id * tiles[139].id;
     cerr<<"136: "<<tiles[136].id<<" 137: "<<tiles[137].id<<" 138: "<<tiles[138].id<<" 139: "<<tiles[139].id<<endl;
     cout<<"ans="<<ans<<endl;
     return;

    /* dp[i][j] = tilepos of tile at i,j, dp[i][j].id=-1 for free
     * */
    vb vis(tiles.size());   /* used marker for tiles within dfs() */
    vector<vector<tilepos>> dp; /* positions and orientations of used tiles so far */

    int gcnt=0;
    /* return true if we found a setting of all tiles */
    function<bool(size_t,size_t)> dfs=[&](size_t ii, size_t jj) {
        if(jj==dp[ii].size()) {
            ii++;
            jj=0;
        }

        if(gcnt++%(1024*16)==0)
            cerr<<"gcnt="<<gcnt<<" ii="<<ii<<" jj="<<jj<<endl;

        if(ii==dp.size()) {
            /* check the borders for notfit */
            cout<<"NW="<<dp[0][0].i<<" NE="<<dp[0].back().i<<" SW="<<dp.back()[0].i<<" SE="<<dp.back().back().i<<endl;
            int ans=tiles[dp[0][0].i].id * tiles[dp[0].back().i].id * tiles[dp.back()[0].i].id * tiles[dp.back().back().i].id;
            cout<<"NW="<<dp[0][0].i<<" NE="<<dp[0].back().i<<" SW="<<dp.back()[0].i<<" SE="<<dp.back().back().i<<endl;
            cout<<"ans="<<ans<<endl;
            if(ans==47213728755493LL)
                return false;

            exit(0);
            return true;    /* last tile reached */
        }

        /* Find possibly fitting tiles from vis and tilememo
         * to speed things up.
         **/
        vi fitting;
        for(size_t i=0; i<tiles.size(); i++)
            if(!vis[i])
                fitting.push_back(i);

        if(jj>0) {  /* left */
            assert(dp[ii][jj-1].i>=0);
            int oth=tiles[dp[ii][jj-1].i].get(E, dp[ii][jj-1].ori);
            vi res(fitting.size());
            auto it=set_intersection(all(fitting), all(tilememo[oth]), res.begin());
            res.resize(it-res.begin());
            fitting.swap(res);
        }
        if(fitting.size()>0 && ii>0) { /* top */
            assert(dp[ii-1][jj].i>=0);
            int oth=tiles[dp[ii-1][jj].i].get(S, dp[ii-1][jj].ori);
            vi res(fitting.size());
            auto it=set_intersection(all(fitting), all(tilememo[oth]), res.begin());
            res.resize(it-res.begin());
            fitting.swap(res);
        }

        assert(dp[ii][jj].i<0);
        if((ii!=0 || jj!=0) && fitting.size()>=2) {
            cerr<<"fitting.size()="<<fitting.size()<<endl;
            assert(fitting.size()<2);
        }

        //cerr<<"fitting.size()="<<fitting.size()<<endl;
        /* now try to place all fitting tiles in all orientations
         * at current position, and recurse with next position. */
        for(int i : fitting) {
            if(ii==0 && jj==0) {
                cerr<<"first tile, i="<<i<<endl;
            }
            assert(!vis[i]);

            vis[i]=true;
            for(int r=0; r<4; r++) {
                for(bool h : bval) {
                    for(bool v : bval) {
                        /*
                        if(ii==0) {  
                            int val=tiles[i].get(N, {r,h,v});
                            vi match=tilememo[val];
                            assert(match.size()>0);
                            if(match.size()>1)
                                continue;
                        }
                        if(ii+1==dp.size()) { 
                            int val=tiles[i].get(S, {r,h,v});
                            vi match=tilememo[val];
                            assert(match.size()>0);
                            if(match.size()>1)
                                continue;
                        }
                        if(jj==0) {
                            int val=tiles[i].get(W, {r,h,v});
                            vi match=tilememo[val];
                            assert(match.size()>0);
                            if(match.size()>1)
                                continue;
                        }
                        if(jj+1==dp[0].size()) { 
                            int val=tiles[i].get(E, {r,h,v});
                            vi match=tilememo[val];
                            assert(match.size()>0);
                            if(match.size()>1)
                                continue;
                        }
                        */

                        bool ok=true;
                        /* check left */
                        if(jj>0) {
                            assert(vis[dp[ii][jj-1].i]);
                            int my=tiles[i].get(W, {r,h,v});
                            int oth=tiles[dp[ii][jj-1].i].get(E, dp[ii][jj-1].ori);
                            if(my!=oth)
                                ok=false;
                        }

                        if(ok && ii>0) {
                            assert(vis[dp[ii-1][jj].i]);
                            int my=tiles[i].get(N, {r,h,v});
                            int oth=tiles[dp[ii-1][jj].i].get(S, dp[ii-1][jj].ori);
                            if(my!=oth)
                                ok=false;
                        }

                        if(ok) {
                            /*
                            if(ii==0 && jj==1)
                                cerr<<"ii="<<ii<<" jj="<<jj<<" i="<<i<<" r="<<r<<" h="<<h<<" v="<<v<<
                                    " E="<<bitset<10>(tiles[i].get(E, {r,h,v}))<<
                                    " S="<<bitset<10>(tiles[i].get(S, {r,h,v}))<<
                                    " W="<<bitset<10>(tiles[i].get(W, {r,h,v}))<<
                                    " N="<<bitset<10>(tiles[i].get(N, {r,h,v}))<<
                                    endl;
                                    */

                            dp[ii][jj].i=i;
                            dp[ii][jj].ori= {r,h,v};
                            if(dfs(ii, jj+1))
                                return true;

                            dp[ii][jj].i=-1;
                        }
                    }
                }
            }
            vis[i]=false;
        }
        return false;   /* no fit */
    };

    /* it is 12x12 */
    dp=vector<vector<tilepos>>(12, vector<tilepos>(12));
    if(dfs(0,0))
        cerr<<"ok"<<endl;
    else
        cerr<<"not ok"<<endl;
}

/*
 * not correct:
 * 47213728755493
 * 47213728755493
 *
 */
signed main() {
    solve();
}
