/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++)
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


int score(list<int> a) {
    int ans=0;
    int mul=a.size();
    while(a.size()) {
        int aa=a.front();
        a.pop_front();
        ans+=mul*aa;
        mul--;
    }
    return ans;
}

/*
 * star2:
 * -rounds
 *  -at start of round: if a players configuration is met the second time, then
 *   player 1 wins.
 *  -if both players number of cards after playing their top card is
 *   at least the number of the card just played, then recurse.
 *   else
 *   the player with the higher card wins that round.
 *  -the winner of the round takes the both cards
 *  -if one of both has all cards, that one has won that game.
 *
 * create a sub game by putting that number of cards as shown on the played card
 * into a new deck.
 * Just simulate that, and memoize to speed things up.
 */
int gans=-1;
int gamecnt=1;
void solve2(list<int> ga, list<int> gb) {
    int gcnt=0;

    /* @return winner of this combat */
    function<int(list<int>&,list<int>&)> rec=[&](list<int> &a, list<int> &b) {
        int lgame=gamecnt++;

        /* decks in this round, prevent endless looping */
        set<pair<list<int>,list<int>>> decks;
        int rndcnt=1;
        while(a.size()>0 && b.size()>0) {
            /*
            cerr<<"Round "<<rndcnt<<" (Game "<<lgame<<") --"<<endl;
            cerr<<"a=";
            for(auto it=a.begin(); it!=a.end(); it++)
                cerr<<*it<<" ";
            cerr<<endl;
            cerr<<"b=";
            for(auto it=b.begin(); it!=b.end(); it++)
                cerr<<*it<<" ";
            cerr<<endl;
            */

            if(!decks.insert({a,b}).second) {
                //cerr<<"repetition, a wins"<<endl;
                return 0LL;
            }

            int aa=a.front();
            a.pop_front();
            int bb=b.front();
            b.pop_front();

            /* winner for current game is determined by subgame */
            if(aa<=a.size() && bb<=b.size()) {
                list<int> aaa;
                auto it=a.begin();
                for(int i=0; i<aa; i++, it++)
                    aaa.push_back(*it);

                list<int> bbb;
                it=b.begin();
                for(int i=0; i<bb; i++, it++)
                    bbb.push_back(*it);

                if(rec(aaa,bbb)==0) {
                    a.push_back(aa);
                    a.push_back(bb);
                } else {
                    b.push_back(bb);
                    b.push_back(aa);
                }
            } else { /* normal winner */
                if(aa>bb) {
                    a.push_back(aa);
                    a.push_back(bb);
                } else {
                    b.push_back(bb);
                    b.push_back(aa);
                }
            }

            gcnt++;
            if(gcnt%(1024*1024)==0) {
                cerr<<"gcnt="<<gcnt<<" a.size()=="<<a.size()<<" b.size()=="<<b.size()<<endl;
            }
            rndcnt++;
        }

        int res=0;
        if(a.size()==0) {
            swap(a,b);
            res=1;
        }
        //cout<<"ans="<<score(a)<<endl;
        gans=score(a);

        /*
            cerr<<"winner deck=";
            for(auto it=a.begin(); it!=a.end(); it++)
                cerr<<*it<<" ";
            cerr<<endl;
            */

        //cerr<<"game "<<lgame<<" won by "<<res<<endl;
        return res;
    };

    rec(ga, gb);
    cout<<"star2 ans="<<gans<<endl;
}

void solve() {
    string gs=gcin();
    string sep("\n");
    vs s=split(gs, sep);

    list<int> a;
    int i=1;
    while(true) {
        if(s[i].size()==0)
            break;
        a.push_back(atoll(s[i].c_str()));
        i++;
    }
    i++;
    i++;
    list<int> b;
    while(true) {
        if(i==s.size())
            break;
        b.push_back(atoll(s[i].c_str()));
        i++;
    }

    solve2(a,b);
}

signed main() {
    solve();
}
