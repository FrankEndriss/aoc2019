/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * star2:
 * Simple simulation seems to lame.
 *
 * We got 3 operations on the ring:
 * pos: find position of an element ( to find dest in each step ) 
 * rem: remove 3 elements from consecutive positions
 * ins: insert 3 elements at consecutive positions
 * ***
 * maintain a map<val,next> to do the operations in O(logn)
 * ***
 * Even simpler. Since the values are like a permutation,
 * we can maintain the values as indexes.
 */
const int M=1e7;
const int N=1e6;
void solve() {
    const vi a={8,7,1,3,6,9,4,5,2}; // original input

    vector<signed> next(N+1);

    for(size_t i=0; i+1<a.size(); i++) 
        next[a[i]]=a[i+1];

    next[2]=10;
    for(int i=10; i<N; i++) 
        next[i]=i+1;
    next[N]=8;      /* link back to first */

    int cur=a[0];   /* first element of input */

    for(int i=0; i<M; i++) {
        vector<signed> rmvals(3);
        for(int j=0; j<3; j++) {
            rmvals[j]=next[cur];
            next[cur]=next[rmvals[j]];
        }

        int dest=cur-1;
        if(dest==0)
            dest+=N;

        while(dest==rmvals[0] || dest==rmvals[1] || dest==rmvals[2]) {
            dest=dest-1;
            if(dest==0)
                dest+=N;
        }

        /* insert the three elements */
        next[rmvals[2]]=next[dest];
        next[rmvals[1]]=rmvals[2];
        next[rmvals[0]]=rmvals[1];
        next[dest]=rmvals[0];

        /* advance one position */
        cur=next[cur];
    }

    ll a1=next[1];
    ll a2=next[a1];

    ll ans=a1*a2;
    cout<<ans<<endl;
}

signed main() {
    solve();
}
