/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++)
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/*
 * Day 14
 * The most annoying one today...
 * The explanation how the mask works with the value sucks.
 */
void solve() {
    string gs=gcin();
    string sep("\n");
    vs lines=split(gs, sep);
    vi mask(36, -1);
    map<string,int> memo;
    for(size_t i=0; i<lines.size(); i++) {
        subst(lines[i], '[', ' ');
        subst(lines[i], ']', ' ');
        vs w=splitws(lines[i]);
        if(w.size()==3) {
            reverse(all(w[2])); /* reversed order of bits */
            for(int j=0; j<w[2].size(); j++)
                if(w[2][j]=='1')
                    mask[j]=1;
                else if(w[2][j]=='0')
                    mask[j]=0;
                else
                    mask[j]=-1;
        } else if(w.size()==4) {
            string addr=w[1];
            int val=atoll(w[3].c_str());
            int mem=memo[addr];
            cerr<<"before mask, val="<<val<<endl;
            for(int j=0; j<36; j++) {
                if(mask[j]>-1) {
                    int bit=mask[j];
 //                   if((val&(1LL<<j))) {
                        if(bit==0) {    /* switch off that bit */
                            val&=(~(1LL<<j));
                        } else if(bit==1) {
                            val|=(1LL<<j);
                        } else
                            assert(false);
//                    }
                }
            }
            cerr<<"addr="<<addr<<" val="<<val<<endl;
            memo[addr]=val;
        } else
            assert(false);
    }

    int ans=0;
    for(auto ent : memo) {
        cerr<<bitset<36>(ent.second)<<endl;
        ans+=ent.second;
    }
    cout<<ans<<endl;

}


signed main() {
    solve();
}

/*
mask = 1X01XXX001101X00001100X1010X10101101
mem[62085] = 231745
mem[14249] = 1252796
mem[34831] = 33366317
mem[31126] = 161974
mask = 10X000X000X0X1X1X11X101X11010001100X
mem[40311] = 636
mem[2321] = 5524
*/
