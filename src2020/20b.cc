/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vvvvi= vector<vvvi>;
using vs= vector<string>;
using vvs= vector<vs>;
using vvvs= vector<vvs>;
using vvvvs= vector<vvvs>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++)
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}

string trim(string s) {
    string t;
    for(char c : s)
        if(c!=' ')
            t+=c;
    return t;
}

/*
 * 144 tiles, so 12x12
 * each tile 10x10
 * We can ignore inner cells, only the edges count.
 * Top/down and left/right each stay like this to each other.
 *
 * We can dfs. Start at any tile, then search for fitting tiles
 * at one side, check all sides. Every time a tile is placed
 * somewhere, check all sides. It is dfs.
 * Let cal the 12x12 grid the biggrid
 *
 * dp[i][j] = tilepos of tile at i,j, dp[i][j].id=-1 for free
 *
 * The check:
 * A tile can be "flipped", so
 * N/S change and E and W are reversed,     (horz flip)
 * E/W change and N and S are reversed      (vert flip)
 * or "rotated",
 * so N/E/S/W -> E/S/W/N
 *
 * We need two checks:
 * One finds matching tiles from unused ones.
 * One checks two adj tilepos to each other.
 */

int getid(string line) {
    //cerr<<"getid, line="<<line<<endl;
    subst(line, ':', ' ');
    vs ss=splitws(line);
    return atol(trim(ss[1]).c_str());
}

int myhash(string str) {
    assert(str.size()==10);
    int i=0;
    for(char c : str) {
        i<<=1;
        if(c=='#')
            i|=1;
    }
    return i;
}

/* first rot, then flip. N->N=0; N->E=1; N->S=2; N->W=3;
   hflip, vflip
   So we got 16 orientations. */
using orientation= tuple<int,bool,bool>;

struct tilepos {
    int i=-1;    /* index of tile in tiles */
    orientation ori;
};

const int N=0;
const int E=1;
const int S=2;
const int W=3;

/* mask of seamonster */
const vs monster= {
    "                  # ",
    "#    ##    ##    ###",
    " #  #  #  #  #  #   "
};

vs frotpic(vs pic, orientation ori) {
    assert(pic.size()%2==0);
    vs ans=pic;
    auto [r,h,v]=ori;
    for(int i=0; i<r; i++) {
        vs pic2=ans;
        for(size_t ii=0; ii<ans.size(); ii++)
            for(size_t jj=0; jj<ans[ii].size(); jj++)
                pic2[jj][ans.size()-1-ii]=ans[ii][jj];
        ans=pic2;
    }
    if(h)
        for(size_t ii=0; ii<ans.size()/2; ii++)
            for(size_t jj=0; jj<ans.size(); jj++)
                swap(ans[ii][jj], ans[ans.size()-1-ii][jj]);
    if(v)
        for(size_t ii=0; ii<ans.size(); ii++)
            for(size_t jj=0; jj<ans.size()/2; jj++)
                swap(ans[ii][jj], ans[ii][ans.size()-1-jj]);

    return ans;
}

struct tile {
    vs edg;  /* N, E, S, W */
    int id; /* informal id from input */
    vvvvi memo;
    vs pic; /* pic of this tile, 8x8 */
    bool picrotated;

    tile() {
        id=-1;
        edg.resize(4);
        memo=vvvvi(4, vvvi(4, vvi(2, vi(2, -1))));
        picrotated=false;
    }

    /* rotate and flip the pic */
    void rotpic(orientation ori) {
        assert(!picrotated);
        pic=frotpic(pic, ori);
        picrotated=true;
    };

    /* return edge e with orientation ori */
    int get(int e, orientation ori) {
        auto [rot,hflip,vflip]=ori;
        if(memo[e][rot][hflip][vflip]>=0)
            return memo[e][rot][hflip][vflip];

        vs aux=edg;

        if(rot!=0) {
            for(int i=0; i<rot; i++) {
                rotate(aux.begin(), aux.begin()+3, aux.end());
                reverse(all(aux[N]));
                reverse(all(aux[S]));
            }
        }

        if(hflip) {
            swap(aux[0], aux[2]);
            reverse(all(aux[1]));
            reverse(all(aux[3]));
        }

        if(vflip) {
            swap(aux[1], aux[3]);
            reverse(all(aux[0]));
            reverse(all(aux[2]));
        }

        return memo[e][rot][hflip][vflip]=myhash(aux[e]);
    }
};

const vb bval= { false, true };

/* we do a bfs to find permutation of the solution.
 * The data is so that some tiles have only 2 possible
 * adj, so these are the corners.
 * The ones with three are borders, and the others are somewhere inner.
 * We use this fact to speed things up.
 */
void solve() {
    string gs=gcin();
    string sep("\n\n");
    vs s=split(gs, sep);

    vector<tile> tiles;
    for(size_t i=0; i<s.size(); i++) {
        sep="\n";
        vs ss=split(s[i], sep);

        tile t;
        t.id=getid(ss[0]);
        t.edg[0]=ss[1];
        t.edg[2]=ss.back();
        for(size_t k=1; k<ss.size(); k++) {
            if(k>1 && k<10)
                t.pic.push_back(ss[k].substr(1, 8));

            t.edg[3]+=ss[k][0];
            t.edg[1]+=ss[k].back();
        }
        assert(t.pic.size()==8);
        tiles.push_back(t);
    }

    //           if(i==4 || i==29 || i==99 || i==31)
    //          NW=41 NE=106 SW=58 SE=122
    /*
    swap(tiles[143], tiles[4]);
    swap(tiles[142], tiles[29]);
    swap(tiles[141], tiles[99]);
    swap(tiles[140], tiles[31]);
    swap(tiles[139], tiles[41]);
    swap(tiles[138], tiles[106]);
    swap(tiles[137], tiles[58]);
    swap(tiles[136], tiles[122]);
    */


    /* tilememo[str]= list of tiles having an edge str in some orientation. */
    vvi tilememo(1024*1024);
    for(size_t i=0; i<tiles.size(); i++) {
        for(int dir=0; dir<4; dir++) {
            for(int r=0; r<4; r++) {
                for(bool h : bval) {
                    for(bool v : bval) {
                        int val=tiles[i].get(dir, {r,h,v});
                        tilememo[val].push_back(i);
                    }
                }
            }
        }
    }

    vector<set<int>> adj(12*12);
    for(size_t i=0; i<tilememo.size(); i++) {
        if(tilememo[i].size()>1) {
            auto it=unique(all(tilememo[i]));
            tilememo[i].resize(distance(tilememo[i].begin(), it));
            for(size_t ii=0; ii<tilememo[i].size(); ii++)  {
                for(size_t jj=ii+1; jj<tilememo[i].size(); jj++) {
                    adj[tilememo[i][ii]].insert(tilememo[i][jj]);
                    adj[tilememo[i][jj]].insert(tilememo[i][ii]);
                }
            }
        }
    }

    set<int> adj2;
    set<int> adj3;
    set<int> adj4;
    for(int i=0; i<144; i++)  {
        if(adj[i].size()==2)
            adj2.insert(i);
        else if(adj[i].size()==3)
            adj3.insert(i);
        else if(adj[i].size()==4)
            adj4.insert(i);
        else
            assert(false);
    }

    /* dp[i][j] = tilepos of tile at i,j, dp[i][j].id=-1 for free
     * */
    vb vis(tiles.size());   /* used marker for tiles within dfs() */
    vector<vector<tilepos>> dp; /* positions and orientations of used tiles so far */

    int gcnt=0;
    function<void()> check_monster=[&]() {
        cerr<<"check monster at gcnt="<<gcnt<<endl;
        vb lvis(12*12);
        for(int i=0; i<12; i++)  {
            for(int j=0; j<12; j++)  {
                assert(dp[i][j].i>=0 && dp[i][j].i<12*12);
                if(lvis[dp[i][j].i]) {
                    cerr<<"dp not distinct, i="<<i<<" j="<<j<<endl;
                    assert(!lvis[dp[i][j].i]);
                }
                lvis[dp[i][j].i]=true;
                tiles[dp[i][j].i].rotpic(dp[i][j].ori);
            }
        }

        vs gimg(12*8, string(12*8, ' '));
        for(int i=0; i<12; i++) {
            for(int j=0; j<12; j++) {
                /* copy img[dp[i][j].i] to position */
                for(int k=0; k<8; k++) {
                    for(int kk=0; kk<8; kk++)
                        gimg[i*8+k][j*8+kk]=tiles[dp[i][j].i].pic[k][kk];
                }
            }
        }


        for(size_t i=0; i<vis.size(); i++)
            assert(vis[i]);

        for(int r=0; r<4; r++) {
            for(bool h : bval) {
                for(bool v : bval) {
                    gimg=frotpic(gimg, {r,h,v});
                    for(size_t i=0; i+3<gimg.size(); i++) {
                        assert(gimg[i].size()==96);
                        for(size_t j=0; j+20<gimg[i].size(); j++) {
                            vector<pii> lmon;
                            for(size_t ii=0; ii<3; ii++) {
                                for(size_t jj=0; jj<20; jj++) {
                                    if(monster[ii][jj]=='#' && (gimg[i+ii][j+jj]=='#' || gimg[i+ii][j+jj]=='x')) {
                                        lmon.push_back({i+ii, j+jj});
                                    }
                                }
                            }
                            if(lmon.size()==15) {   /* monster found */
                                for(pii p : lmon) {
                                    gimg[p.first][p.second]='x';
                                }
                            }
                        }
                    }
                    gimg=frotpic(gimg, {0,false,v});
                    gimg=frotpic(gimg, {0,h,false});
                    gimg=frotpic(gimg, {4-r,false,false});
                }
            }
        }

        for(size_t i=0; i<gimg.size(); i++)
            cerr<<gimg[i]<<endl;

        int ans=0;
        for(int i=0; i<96; i++) 
            for(int j=0; j<96; j++) 
                if(gimg[i][j]=='#')
                    ans++;
        cout<<ans<<endl;
    };

    /* check the monsert if we found a fitting setting of all tiles. */
    function<void(size_t,size_t)> dfs=[&](size_t ii, size_t jj) {
        if(jj==12) {
            ii++;
            jj=0;
        }

        if(gcnt++%(1024*16)==0)
            cerr<<"gcnt="<<gcnt<<" ii="<<ii<<" jj="<<jj<<endl;

        if(ii==12) {
            check_monster();
            return;
        }

        /* Find possibly fitting tiles from vis, tilememo and adjX
         **/
        vi fitting;
        if((ii==0 || ii+1==dp.size()) && (jj==0 || jj+1==dp.size())) {
            for(int i : adj2)
                if(!vis[i])
                    fitting.push_back(i);
        } else if(ii==0 || ii+1==dp.size() || jj==0 || jj+1==dp.size()) {
            for(int i : adj3)
                if(!vis[i])
                    fitting.push_back(i);
        } else {
            for(int i : adj4)
                if(!vis[i])
                    fitting.push_back(i);
        }

        if(jj>0) {  /* check left tile */
            assert(dp[ii][jj-1].i>=0);
            int oth=tiles[dp[ii][jj-1].i].get(E, dp[ii][jj-1].ori);
            vi res(fitting.size());
            auto it=set_intersection(all(fitting), all(tilememo[oth]), res.begin());
            res.resize(it-res.begin());
            fitting.swap(res);
        }
        if(fitting.size()>0 && ii>0) { /* check top tile */
            assert(dp[ii-1][jj].i>=0);
            int oth=tiles[dp[ii-1][jj].i].get(S, dp[ii-1][jj].ori);
            vi res(fitting.size());
            auto it=set_intersection(all(fitting), all(tilememo[oth]), res.begin());
            res.resize(it-res.begin());
            fitting.swap(res);
        }

        assert(dp[ii][jj].i<0);

        //cerr<<"fitting.size()="<<fitting.size()<<endl;
        /* now try to place all fitting tiles in all orientations
         * at current position, and recurse with next position. */
        for(int i : fitting) {
            if(ii==0 && jj==0) {
                cerr<<"first tile, i="<<i<<endl;
            }
            assert(!vis[i]);

            vis[i]=true;
            bool done=false;
            for(int r=0; !done && r<4; r++) {
                for(bool h : bval) {
                    for(bool v : bval) {
                        if(done)
                            continue;

                        bool ok=true;
                        /* check left */
                        if(jj>0) {
                            assert(vis[dp[ii][jj-1].i]);
                            int my=tiles[i].get(W, {r,h,v});
                            int oth=tiles[dp[ii][jj-1].i].get(E, dp[ii][jj-1].ori);
                            if(my!=oth)
                                ok=false;
                        }

                        if(ok && ii>0) {
                            assert(vis[dp[ii-1][jj].i]);
                            int my=tiles[i].get(N, {r,h,v});
                            int oth=tiles[dp[ii-1][jj].i].get(S, dp[ii-1][jj].ori);
                            if(my!=oth)
                                ok=false;
                        }

                        if(ok) {
                            done=true;
                            dp[ii][jj].i=i;
                            dp[ii][jj].ori= {r,h,v};
                            dfs(ii, jj+1);
                            dp[ii][jj].i=-1;
                        }
                    }
                }
            }
            vis[i]=false;
        }
    };

    /* it is 12x12 */
    dp=vector<vector<tilepos>>(12, vector<tilepos>(12));
    dfs(0,0);

    cerr<<"fini"<<endl;
}

/*
 * 47213728755493
 */
signed main() {
    solve();
}
