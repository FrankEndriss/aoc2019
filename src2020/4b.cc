
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

string fi(string &s) {
    string ans;
    for(char c : s)
        if(c==':')
            return ans;
        else
            ans+=c;

    assert(false);
}
string va(string s) {
    string ans;
    bool x=false;
    for(char c : s) {
        if(x)
            ans+=c;

        if(c==':')
            x=true;
    }
    return ans;
}

bool mimaint(string s, int mi, int ma) {
    int num=0;
    for(char c : s) {
        if(c<'0' || c>'9')
            return false;

        num*=10;
        num+=c-'0';
        if(num>ma)
            return false;
    }
    return num>=mi && num<=ma;
}

bool v_hgt(string s) {
    int num=0;

    string ss;
    for(char c : s) {
        if(c>='0' && c<='9') {
            if(ss.size()>0)
                return false;
            num*=10;
            num+=c-'0';
            if(num>193)
                return false;
        } else
            ss+=c;
    }

    if(ss=="in")
        return num>=59 && num<=76;
    else if(ss=="cm")
        return num>=150 && num<=193;
    else
        return false;
}

bool v_ecl(string s) {
    if(s=="amb" || s=="blu"||s=="brn"||s=="gry"||s=="grn"||s=="hzl"||s=="oth")
        return true;
    else
        return false;
}

bool v_hcl(string s) {
    if(s[0]!='#')
        return false;
    int cnt=0;
    for(size_t i=1; i<s.size(); i++)
        if((s[i]>='a' && s[i]<='z') || (s[i]>='0' && s[i]<='9'))
            cnt++;

    return cnt==6;
}

bool v_pid(string s) {
    int cnt=0;
    for(char c : s) {
        if(c>='0' && c<='9')
            cnt++;
    }
    return cnt==9;
}



/*
 * while we had to implement a fairly stupid parser for star 1
 * the second part asks us to implement some even more stupid
 * validations. sic :/
 */
void solve() {
    const vs f= { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};

    int ans=0;
    int cnt=0;
    while(true) { /* whole file */
        set<string> data;
        bool valid=true;

        while(true) {   /* until emptyline */
            cnt++;
            string s;
            getline(cin,s);
            if(s.size()==0)
                break;

            istringstream iss(s);
            while(true) {   /*  a line */
                string w;
                iss>>w;
                if(w.size()==0)
                    break;

                string field=fi(w);
                string val=va(w);
                bool valid=true;

                cerr<<"field="<<field<<" val="<<val<<endl;
                if(field=="byr" && mimaint(val, 1920, 2002)) {
                    data.insert(field);
                } else if(field=="iyr" && mimaint(val, 2010, 2020)) {
                    data.insert(field);
                } else if(field=="eyr" && mimaint(val, 2020, 2030)) {
                    data.insert(field);
                } else if(field=="hgt" && v_hgt(val)) {
                    data.insert(field);
                } else if(field=="ecl" && v_ecl(val)) {
                    data.insert(field);
                } else if(field=="hcl" && v_hcl(val)) {
                    data.insert(field);
                } else if(field=="pid" && v_pid(val)) {
                    data.insert(field);
                }
            }
        }

        int fcnt=0;
        for(string ss : f)
            fcnt+=data.count(ss);

        if(fcnt<f.size())
            valid=false;

        //cerr<<"fcnt="<<fcnt<<endl;
        if(valid)
            ans++;

        data.clear();


        if(cnt>1101)
            break;
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
