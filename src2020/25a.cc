/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Transform:
 * val
 * for(loop size)
 *   val=(val*subject)%20201227
 *   val%=20201227;
 */
int transform(int subject, int loop) {
    int val=1;
    for(int i=0; i<loop; i++)
        val=(val*subject)%20201227;
    return val;
}

/* First find the "loop size" by
 * transform(7,x) to v1 or v2
 */
void solve() {
    vi pub={
        10943862,
        12721030
    };
    vi loopsize(2);

    int cnt=0;
    int tval=1;
    int i=0;
    while(cnt<2) {
        i++;
        tval=(tval*7)%20201227;
        for(int j=0; j<2; j++) {
            if(tval==pub[j] && loopsize[j]==0) {
                loopsize[j]=i;
                cnt++;
            }
        }
    }

    int encKey1=transform(pub[0],loopsize[1]);
    int encKey2=transform(pub[1],loopsize[0]);
    assert(encKey1==encKey2);
    cout<<"encKey="<<encKey1<<endl;
}

signed main() {
    solve();
}
