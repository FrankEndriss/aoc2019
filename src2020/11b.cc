/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

//#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/*
 * FIRST UNDERSTAND THE QUESTION! Then code.
 */
void solve() {
    string gs=gcin();
    string sep("\n");
    vs m=split(gs, sep);

    vi dx= {-1, 0, 1, -1, 1, -1, 0, 1 };
    vi dy= {-1, -1, -1, 0, 0, 1, 1, 1  };

    int lcount=0;
    while(true) {
        cerr<<lcount<<endl;
        lcount++;
        vs m0=m;
        bool chg=false;
        for(int i=0; i<m.size(); i++) {
            //cerr<<m[i]<<endl;
            for(int j=0; j<m[i].size(); j++) {
                //cerr<<"i="<<i<<" j="<<j<<endl;

                if(m[i][j]=='.')
                    continue;

                vi vis(8,-1);
                for(int k=0; k<8; k++) {
                    for(int dist=1; dist<120; dist++) {
                        int ii=i+dx[k]*dist;
                        int jj=j+dy[k]*dist;
                        //cerr<<"ii="<<ii<<" jj="<<jj<<endl;
                        if(ii>=0 && ii<m.size() && jj>=0 && jj<m[ii].size()) {
                            if(vis[k]==-1) {
                                if(m[ii][jj]=='#')
                                    vis[k]=1;
                                else if(m[ii][jj]=='L')
                                    vis[k]=0;
                            } else
                                break;
                        } else 
                            break;
                    }
                }
                int cnt=0;
                for(int k=0; k<8; k++)
                    if(vis[k]==1)
                        cnt++;

                if(m[i][j]=='L' && cnt==0) {
                    m0[i][j]='#';
                    chg=true;
                } else if(m[i][j]=='#' && cnt>=5) {
                    m0[i][j]='L';
                    chg=true;
                }
            }
        }
        if(!chg)
            break;

        m=m0;
    }

    int ans=0;
    for(size_t i=0; i<m.size(); i++)
        for(size_t j=0; j<m[i].size(); j++)
            if(m[i][j]=='#')
                ans++;

    cout<<"ans="<<ans<<endl;
}

signed main() {
    solve();
}
