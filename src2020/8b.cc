/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/* one instruction with argument(s) */
struct instr {
    string cmd;
    int val;

    instr(string s) {
        istringstream iss(s);
        iss>>cmd;
        iss>>val;
    }
};

/* processor, process.
 * Note that each process used an own copy
 * of a program. */
struct proc {
    int ip=0;
    int acc=0;

    vector<instr> prog;

    proc(vector<instr> &_instr):prog(_instr) {
    }

    /* process */
    void exec() {
        vb vis(prog.size());
        while(ip>=0 && ip<(int)prog.size() && !vis[ip]) {
            vis[ip]=true;
            step();
        }
    }

    /* processor as of day 8, executes instruction at position ip. */
    void step() {
        assert(ip>=0 && ip<(int)prog.size());

        string cmd=prog[ip].cmd;
        int val=prog[ip].val;

        if(cmd=="acc") {
            acc+=val;
            ip++;
        } else if(cmd=="jmp")
            ip+=val;
        else if(cmd=="nop") {
            ip++;
        } else {
            cerr<<"error unknown cmd, cmd="<<cmd<<endl;
            assert(false);
        }
    }
};

/*
 * FIRST UNDERSTAND THE QUESTION, then code!
 */
void solve() {
    string gs=gcin();
    string sep("\n");
    vs data=split(gs, sep);

    vector<instr> prog;
    for(string s : data)
        prog.emplace_back(s);

    /* brute force try each position for the change. */
    for(size_t i=0; i<prog.size(); i++) {
        proc p(prog);
        if(p.prog[i].cmd=="jmp")
            p.prog[i].cmd="nop";
        else if(p.prog[i].cmd=="nop")
            p.prog[i].cmd="jmp";
        else 
            continue;

        p.exec();
        if(p.ip==(int)p.prog.size()) {
            cout<<p.acc<<endl;
            return;
        }
    }
    cout<<"notfound"<<endl;
}

signed main() {
    solve();
}
