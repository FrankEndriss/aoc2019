
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* call like:
 * a.exe < input.txt >b.cc
 * Then compile b.cc and run it.
 *
 * It substitutes all "+" in the input by "*",
 * and all "*" by "+".
 * Then all digits are wrapped in an type mytype,
 * and thats operators + and * are overloaded to 
 * to the opposite thing.
 *
 * So in fact only the precedence of * and + change.
 */
void solve() {
    string line;
    cout<<"#include <bits/stdc++.h>"<<endl;
    cout<<"using namespace std;"<<endl<<endl;
    cout<<"#define int long long"<<endl;
    cout<<"struct mytype {"<<endl;
    cout<<" int val;"<<endl;
    cout<<" mytype(int i):val(i) { }"<<endl;
    cout<<" mytype operator+(mytype other) { return mytype(other.val*val); }"<<endl;
    cout<<" mytype operator*(mytype other) { return mytype(other.val+val); }"<<endl;
    cout<<"};"<<endl;

    cout<<"signed main() {"<<endl;
    cout<<" int ans=0;"<<endl;
    while(true) {
        getline(cin, line);
        if(line.size()==0) 
            break;

        cout<<"ans+=(";
        for(char c : line) {
            if(c>='0' && c<='9')
                cout<<"mytype("<<c<<"LL)";
            else if(c=='+')
                cout<<'*';
            else if(c=='*')
                cout<<'+';
            else
                cout<<c;
        }
        cout<<").val;"<<endl;
    }
    cout<<"cout<<ans<<endl;"<<endl;
    cout<<"} // end main"<<endl;
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
