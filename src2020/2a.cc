
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
* 2-9 c: cccccccc
* We need to implement a simple parser
* and count the freq of the given char in word.
*/
void solve() {
    string s;
    int ans=0;
    while(true) {
        getline(cin,s);
        if(s.size()==0)
            break;

        for(int i=0; i<s.size(); i++)
            if(s[i]=='-' || s[i]==':')
                s[i]=' ';

        istringstream iss(s);
        int l,r;
        iss>>l>>r;
        char c;
        iss>>c;
        string w;
        iss>>w;
        int cnt=0;
        for(char cc : w)
            if(cc==c)
                cnt++;

        if(cnt>=l && cnt<=r)
            ans++;
    }
    cout<<ans<<endl;

}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
