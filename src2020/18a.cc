/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++) 
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


string trim(string s) {
    string t;
    for(char c : s)
        if(c!=' ')
            t+=c;

    return t;
}
/*
 * FIRST UNDERSTAND THE QUESTION! Then code.
 */
void solve() {
    string gs=gcin();
    string sep("\n");
    vs a=split(gs, sep);

    function<int(string,int&)> op=[&](string s, int &pos) {
        cerr<<"op, pos="<<pos<<endl;
        int start=pos;
        assert(s[pos]=='(');
        int val;
        pos++;
        if(s[pos]=='(') {
            val=op(s, pos);
        } else {
            assert(s[pos]>='0' && s[pos]<='9');
            val=s[pos]-'0';
            pos++;
        }
        
        while(s[pos]!=')') {
            char oper=s[pos];
            assert(oper=='+' || oper=='*');
            pos++;

            int val2;
            if(s[pos]=='(') {
                val2=op(s, pos);
            } else {
                val2=s[pos]-'0';
                pos++;
            }

            if(oper=='+')
                val+=val2;
            else {
                val*=val2;
            }
        }
        pos++;

        cerr<<"s="<<s<<"start="<<start<<" end="<<pos<<" val="<<val<<endl;
        return val;
    };

    int ans=0;
    for(size_t i=0; i<a.size(); i++) {
        string s=string("(")+a[i]+")";
        s=trim(s);
        int pos=0;
        ans+=op(s, pos);
    }
    cout<<ans<<endl;
}

signed main() {
    solve();
}
