/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++)
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/*
 * How to find if a rule matches?
 * We could create all possible strings createable by the rules (dfs)
 * and put them into a hashmap.
 * This will most likely be to much strings.
 * But it is simple, give it a try... no.
 *
 * Each rule has a length of the possible strings.
 *
 * First char must match a simple rule, second same.. and so on.
 * So we can convert it to a list of possible rules of len 1.
 * Then convert it to a list of possible rules of len 2
 * ...and so on.... no.
 *
 * First letter matches some single letter rule.
 * Second another one..and so on.
 * Then first, or first two rules match some other rules.
 * So it a dp with memo.
 *
 * Single rules: Some rules refer to other rules, ie
 * 8: 42
 * 9: 42 | 43;
 * The rules have same length of string as the parent rules :/
 * *******
 *
 * We need a function 
 * check(string, startidx, rule)
 * that return length of match if the rule rule is matched by substring 
 * starting at startidx.
 * then we can recursive check all possible alternatives of a given rule.
 */
struct rule {
    char c;
    vector<pii> orl;
};
void solve() {
    string gs=gcin();
    string sep("\n");
    vs s=split(gs, sep);

    map<int,rule> rules;
    vs search;
    for(size_t i=0; i<s.size(); i++) {
        if(s[i][0]>='0' && s[i][0]<='9') {
            rule r;
            r.c=0;
            int id=atoi(split(s[i], ":")[0].c_str());
            string ru=split(s[i], ":")[1];
            cerr<<"ru="<<ru<<endl;
            if(ru[1]=='"') {
                r.c=ru[2];
            } else {
                vs ors=split(ru, "|");
                for(string ss : ors) {
                    vi num=splitwsi(ss);
                    if(num.size()<2)
                        num.push_back(-1);
                    assert(num.size()==2);
                    r.orl.push_back({num[0], num[1]});
                }
            }
            rules[id]=r;
        } else if(s[i].size()>0) {
            search.push_back(s[i]);
        }
    }

    vector<set<int>> extMemo(200);
    vb extVis(200);

    /* return a list of rules where all rules in r are copied to their single rule parents. */
    function<void(set<int>&)> extSingle=[&](set<int> &r) {
        set<int> rr=r;
        for(int i : rr) {
            if(extVis[i]) {
                for(int j : extMemo[i])
                    r.insert(j);
            } else {
                set<int> ext;
                for(auto ent : rules) {
                    for(pii p : ent.second.orl) {
                        if(p.first==i && p.second==-1)
                            ext.insert(ent.first);
                    }
                }

                extVis[i]=true;
                extMemo[i]=ext;
                for(int j : ext) {
                    r.insert(j);
                }
            }
        }
    };

    vvb vis;
    vector<vector<set<int>>> memo;
    /* return a set of rule id the substring starting at i0 and ending in i1 matches
     * (both index including)
     */
    function<void(string&,int,int)> go=[&](string &t, const int i0, const int i1) {
        assert(i0>=0 && i1>=0);
        if(vis[i0][i1])
            return;

        //cerr<<"t="<<t<<" go, i0="<<i0<<" i1="<<i1<<endl;
        set<int> ans;
        if(i0==i1) {
            for(auto ent : rules) {
                if(ent.second.c>0 && ent.second.c==t[i0]) {
                    ans.insert(ent.first);
                }
            }
            extSingle(ans);
            memo[i0][i1]=ans;
            vis[i0][i1]=true;
            return;
        }

        for(int i=i0; i<i1; i++) {
            go(t, i0, i);
            go(t, i+1, i1);
            for(auto ent : rules) {
                for(pii p : ent.second.orl) {
                    if(p.second>=0) {
                        if(memo[i0][i].count(p.first) && memo[i][i1].count(p.second))
                            ans.insert(ent.first);
                    }
                }
            }
        }
        extSingle(ans);
        memo[i0][i1]=ans;
        vis[i0][i1]=true;
        return;
    };

    /** @return possible lengths of substrings starting at i matching rule r */
    function<set<int>(string,int,int)> check=[&](string t, int i, int r)  {
        set<int> ans;
        rule ru=rules[r];
        if(ru.c>0) {
            if(t[i]==ru.c)
                ans.insert(1);
            return ans;
        }

        for(pii p : ru.orl) {
            set<int> len1=check(t, i, p.first);
            for(int ii : len1) {
                if(p.second<0)
                    ans.insert(ii);
                else {
                    set<int> len2=check(t, i+ii, p.second);
                    for(int j : len2) 
                        ans.insert(ii+j);
                }
            }
        }
        return ans;
    };

    const int N=100;
    int cnt=0;
    for(size_t i=0; i<search.size(); i++) {
        cerr<<"search: "<<search[i]<<" size="<<search[i].size()<<" "<<i<<"/"<<search.size()<<endl;
        /*
        memo=vector<vector<set<int>>>(N, vector<set<int>>(N));
        vis=vvb(N, vb(N));

        go(search[i], 0, search[i].size()-1);
        if(memo[0][search[i].size()-1].count(0))
            cnt++;
            */

        set<int> lens=check(search[i], 0, 0);
        if(lens.count(search[i].size()))
            cnt++;
    }

    cout<<cnt<<endl;
}

signed main() {
    solve();
}
