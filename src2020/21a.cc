/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++)
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/*
 * FIRST UNDERSTAND THE QUESTION! Then code.
 * parser 8 minutes
 * ***
 * two foods with one (or more) same alergene in the list
 * contain some ingredients both.
 * These ingr contained in both can
 *
 * Combining two foods yield some rules:
 * At start each allergene can belong to each ingr.
 * With each food the list of ingredients per allergene
 * gets smaller.
 *
 *
 * ****
 * Note that each alergene belongs to _exactly_ one
 * ingredient!
 * We need to identify foreach allergene the ingredient.
 */
void solve() {
    string gs=gcin();
    string sep("\n");
    vs s=split(gs, sep);

    map<string,set<string>> a2i;   /* a2i[a]==possible foods of allergene a */

    vector<pair<set<string>,set<string>>> foods;

    set<string> uningr; /* set of ingredients with unknown  state */
    set<string> unalle; /* set of allergene with unknown  state */
    for(string line : s) {
        sep="(contains ";
        vs s2=split(line, sep);
        assert(s2.size()==2);
        subst(s2[1], ',', ' ');
        subst(s2[1], ')', ' ');
        set<string> fsetL;
        set<string> fsetR;
        vs w1=splitws(s2[0]);   /* left ingredients */
        for(string ss : w1) {
            uningr.insert(ss);
            fsetL.insert(ss);
        }
        vs w2=splitws(s2[1]);   /* right alergens */
        for(string ss : w2) {
            unalle.insert(ss);
            fsetR.insert(ss);
        }
        foods.emplace_back(fsetL, fsetR);
        /*
        for(string ss : w1)
            cerr<< ss<< " : ";
        cerr<<"  :::::   ";
        for(string ss : w2)
            cerr<< ss<< " : ";
            */
    }
    for(string w : unalle)
        a2i[w]=uningr;      /* copy list of all ingredients */

    cerr<<"foods.size()="<<foods.size()<<endl;
    cerr<<"uningr.size()="<<uningr.size()<<endl;
    cerr<<"unalle.size()="<<unalle.size()<<endl;

    for(size_t i=0; i<foods.size(); i++) {
        for(string a : foods[i].second) {
            /* remove all foods from a2i[a] which are not in foods[i].first */
            for(auto it=a2i[a].begin(); ; ) {
                if(foods[i].first.count(*it)==0)
                    it=a2i[a].erase(it);
                else
                    it++;

                if(it==a2i[a].end())
                    break;
            }
        }
    }

    /* step2: remove identified ingr from all other allergenes lists */
    function<void()> eraseSingles=[&]() {
        bool done=false;
        while(!done) {
            vector<pair<string,string>> todo;   /* known aller::ingr pair */
            done=true;
            for(auto ent : a2i) {
                if(ent.second.size()==1) {
                    string elem=*ent.second.begin();
                    cerr<<"erase single, a="<<ent.first<<" ingr="<<elem<<endl;
                    todo.emplace_back(ent.first, elem);
                    done=false;
                }
            }

            for(pair<string,string> p : todo) {
                unalle.erase(p.first);
                uningr.erase(p.second);

                cerr<<"eraseing "<<p.second<<" from all ingr lists "<<endl;
                for(auto it=a2i.begin(); it!=a2i.end(); it++)
                    it->second.erase(p.second);

                a2i.erase(p.first);
            }
        }
    };

    eraseSingles();

    /* step3: according to the example if we find 2 allers with
     * each have to ingr, and one of them is in both others, then
     * the one contained in both is not the ingr of the allers,
     * but the other two.
     * It is unclear why, but that is what the example says.
     */
    bool done=false;
    while(!done) {
        done=true;
        vector<string> a2list; /* list of allergenes with exactly 2 ingr */
        for(auto ent : a2i) {
            if(ent.second.size()==2)
                a2list.push_back(ent.first);
        }

        bool dobreak=false;
        for(size_t i=0; !dobreak && i<a2list.size(); i++) {
            for(size_t j=i+1; !dobreak && j<a2list.size(); j++) {
                const string a1=a2list[i];
                const string a2=a2list[j];
                cerr<<"a1="<<a1<<" a2="<<a2<<endl;

                vs both;
                set_intersection(all(a2i[a1]), all(a2i[a2]), back_inserter(both));
                cerr<<"both.size()="<<both.size()<<endl;
                if(both.size()>0) {
                    assert(both.size()==1);
                    a2i[a1].erase(both[0]);
                    a2i[a2].erase(both[0]);

                    eraseSingles();
                    done=false;
                    dobreak=true;
                }
            }
        }
    }

    assert(unalle.size()==0);


    cerr<<"uningr.size()="<<uningr.size()<<endl;
    for(auto ent : a2i) {
        cerr<<"alle="<<ent.first<<" ingrlist.size()="<<ent.second.size()<<": ";
        for(string ss : ent.second)
            cerr<<ss<<" ";
        cerr<<endl;
    }
    cerr<<"end uningr.size()="<<uningr.size()<<endl;
    cerr<<"end unalle.size()="<<unalle.size()<<endl;

    int ans=0;
    for(int i=0; i<foods.size(); i++) {
        for(string ingr : foods[i].first)
            if(uningr.count(ingr))
                ans++;
    }

    cout<<"star1="<<ans<<endl;
}

signed main() {
    solve();
}
