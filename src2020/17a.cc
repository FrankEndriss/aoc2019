/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vvvb= vector<vvb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++) 
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/*
 * FIRST UNDERSTAND THE QUESTION! Then code.
 */
void solve() {
    string gs=gcin();
    string sep("\n");
    vs s=split(gs, sep);

    const int N=50;
    const int NN=N*2;
    vvvb a(NN, vvb(NN, vb(NN)));
    for(int i=0; i<s.size(); i++) 
        for(int j=0; j<s[i].size(); j++) 
            if(s[i][j]=='#')
                a[i+N][j+N][N]=true;

    int ans;
    for(int ii=0; ii<6; ii++) {
        vvvb aa(NN, vvb(NN, vb(NN)));
        ans=0;
        for(int i=0; i<NN; i++) {
            for(int j=0; j<NN; j++)  {
                for(int k=0; k<NN; k++) {
                    int cnt=0;
                    for(int x=-1; x<=1; x++) {
                        for(int y=-1; y<=1; y++) {
                            for(int z=-1; z<=1; z++) {
                                if(x==0 && y==0 && z==0)
                                    continue;

                                if(i+x<0 || i+x>=NN || j+y<0 || j+y>=NN || k+z<0 || k+z>=NN)
                                    continue;

                                if(a[i+x][j+y][k+z])
                                    cnt++;
                            }
                        }
                    }
                    if(a[i][j][k] && (cnt==2 || cnt==3))
                        aa[i][j][k]=true;
                    if(!a[i][j][k] && cnt==3)
                        aa[i][j][k]=true;

                    if(aa[i][j][k])
                        ans++;
                }
            }
        }
        a=aa;
    }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
