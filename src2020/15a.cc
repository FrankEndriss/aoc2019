/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++) 
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/*
 * Text riddle.
 * 1 2 3 4 5   6   7
 * 0 3 6 0 4-1 5-2 1
 *          3  3
 *
 * 3 cases for prev:
 * 1 times: 0
 * 2 times: idx0-idx1
 */
const int N=1e6;
void solve() {
    //string gs=gcin();
    //string sep("\n");
    // vs a=split(gs, sep);
    //
    vi a={ 1, 12, 0, 20, 8, 16 };
    //vi a={ 2,1,3 };
    map<int,pii> m;
    int idx=1;
    for(int i=0; i<a.size(); i++) {
        m[a[i]]={idx,-1};
        idx++;
    }

    int prev=a.back();
    while(idx<=2020) {
        auto it=m.find(prev);
        assert(it!=m.end());

        int next;
        if(it->second.second<0)
            next=0;
        else
            next=it->second.first-it->second.second;

        it=m.find(next);

        if(it==m.end())
            m[next]={ idx, -1 };
        else {
            m[next].second=m[next].first;
            m[next].first=idx;
        }

        idx++;
        prev=next;
    }
    cout<<"ans="<<prev<<endl;
}

signed main() {
    solve();
}
