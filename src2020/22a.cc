/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++) 
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/*
 * FIRST UNDERSTAND THE QUESTION! Then code.
 * star1: just simulate.
 */
void solve() {
    string gs=gcin();
    string sep("\n");
    vs s=split(gs, sep);

    list<int> a;
    int i=1;
    while(true) {
        if(s[i].size()==0)
            break;
        a.push_back(atoll(s[i].c_str()));
        i++;
    }
    i++;
    i++;
    list<int> b;
    while(true) {
        if(i==s.size())
            break;
        b.push_back(atoll(s[i].c_str()));
        i++;
    }

    int cnt=0;
    while(a.size()>0 && b.size()>0) {
        /*
        cerr<<"a=";
        for(auto it=a.begin(); it!=a.end(); it++)
            cerr<<*it<<" ";
        cerr<<endl;
        cerr<<"b=";
        for(auto it=b.begin(); it!=b.end(); it++)
            cerr<<*it<<" ";
        cerr<<endl;
        */

        int aa=a.front();
        a.pop_front();
        int bb=b.front();
        b.pop_front();
        if(aa>bb) {
            a.push_back(aa);
            a.push_back(bb);
        } else {
            b.push_back(bb);
            b.push_back(aa);
        }

        cnt++;
        if(cnt%(1024*1024)==0) {
            cerr<<"cnt="<<cnt<<" a.size()=="<<a.size()<<" b.size()=="<<b.size()<<endl;
        }
    }

    if(a.size()==0)
        a.swap(b);
    int ans=0;
    int mul=a.size();
    while(a.size()) {
        int aa=a.front();
        a.pop_front();
        ans+=mul*aa;
        mul--;
    }
    cout<<ans<<endl;


}

signed main() {
    solve();
}
