/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 */
void solve() {
    string gs=gcin();
    vs s=splitws(gs);

    assert(s.size()==10);

    vvi a(10,vi(10));
    for(int i=0; i<10; i++)
        for(int j=0; j<10; j++)
            a[i][j]=s[i][j]-'0';

    const vi dx= { -1, -1, -1, 0, 0, 1, 1, 1 };
    const vi dy= { -1, 0, 1, -1, 1, -1, 0, 1 };

    for(int step=0; true; step++) {
        //cerr<<"step "<<step<<endl;
        vvb vis(10, vb(10));
        vector<pii> q;
        int idx=0;

        for(int i=0; i<10; i++)
            for(int j=0; j<10; j++)
                a[i][j]++;

        while(true) {
            for(int i=0; i<10; i++)
                for(int j=0; j<10; j++) {
                    if(a[i][j]>9 && !vis[i][j]) {
                        q.emplace_back(i,j);
                        vis[i][j]=true;
                    }
                }


            if(idx==q.size())
                break;

            while(idx<q.size()) {
                auto [x,y]=q[idx];
                idx++;
                for(int k=0; k<8; k++) {
                    int xx=x+dx[k];
                    int yy=y+dy[k];
                    if(xx>=0 && xx<10 && yy>=0 && yy<10)
                        a[xx][yy]++;
                }
            }
        }

        for(auto [x,y] : q)
            a[x][y]=0;

        if(q.size()==100) {
            cout<<"star2="<<step+1<<endl;
            return;
        }
    }
}

signed main() {
    solve();
}
