/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++) 
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 * Note that we can move in all 4 directions :/
 * So I solved again some other problem.
 * Less fun with text parsing :/
 */
const int INF=1e9;
void solve() {
    string gs=gcin();
    vs a=splitws(gs);

    const int n=a.size();
    const int m=a[0].size();
    const int n5=n*5;
    const int m5=m*5;

    vs aa(n5, string(m5,' '));

    for(int i=0; i<n5; i++) {
        for(int j=0; j<m5; j++)  {
            int inc=i/n + j/m;
            int val=(a[i%n][j%m]-'0'+inc);
            while(val>9)
                val-=9;
            aa[i][j]=(char)('0'+val);
        }
        cerr<<aa[i]<<endl;
    }



    //cerr<<"made big maze"<<endl;

    vvi dp(n5, vi(m5, INF));

    dp[0][0]=0; /* start at 0, not at a[0][0] */

    for(int i=0; i<n5; i++)  {
        for(int j=0; j<m5; j++) {
            if(i>0) {
                dp[i][j]=min(dp[i][j], dp[i-1][j]+aa[i][j]-'0');
            }
            if(j>0)  {
                dp[i][j]=min(dp[i][j], dp[i][j-1]+aa[i][j]-'0');
            }
        }
    }

    /*
    for(size_t i=aa.size()-12; i<aa.size(); i++)  {
        for(size_t j=aa[0].size()-12; j<aa[0].size(); j++) {
            cerr<<aa[i][j];
        }
        cerr<<endl;
    }
    */

    cout<<dp.back().back()<<endl;
}

signed main() {
    solve();
}
