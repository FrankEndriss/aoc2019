/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string, 
 * substitute lineends by "\n" if they not allready are.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+='\n';
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* vector<string> to vector<int> */
vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++) 
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
string subst(string s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
    return s;
}

string subst(string s, char c, string su) {
    string ans;
    for(char cc : s)
        if(cc==c)
            ans+=su;
        else
            ans+=cc;

    return ans;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}

/* split each input string and flatten result */
vs split(vs ss, string sep) {
    vs ans;
    for(size_t i=0; i<ss.size(); i++) 
        for(string aux : split(ss[i], sep))
            ans.push_back(aux);
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}

/**
 * Consider two scanners with 12 overlapping beacons.
 * There exist 12 beacons, where there are 11 other
 * beacons in same distance to the first one in both scanners.
 *
 * So, to check if two scanners overlap, check all pairs of
 * beacons if there are 11 others.
 * But do this check 48 times, 6 permutations of the axes,
 * and 8 permutations of the axis directions.
 *
 * Once a overlap is found, store it in usual adj matrix
 * with the 6-tuple as cost.
 *
 * Finally, all scanners should be in one component.
 * Then start with an arbitrary, and dfs all scanners.
 * With each step, execute all transformations on the
 * goto scanner.
 *
 * ****
 * For part2 we also need to take care where the scanners are :/
 */
void solve() {
    string gs=gcin();

    
    vvvi sc;

    for(string s0 : split(subst(gs, ',', ' '), "\n\n")) {
        sc.resize(sc.size()+1);
        vs lines=split(s0, "\n");
        //cerr<<"scanner: "<<sc.size()<<endl;
        for(size_t i=1; i<lines.size(); i++) {
            vi data=splitwsi(lines[i]);
            sc.back().push_back(data);
            //cerr<<data[0]<<" "<<data[1]<<" "<<data[2]<<endl;
        }
        /* for star2 add the scanner position as last beacon */
        vi data={ 0, 0, 0};
        sc.back().push_back(data);
    }
    const int n=sc.size();

    /* permutation of the 3 axes, and multiplicator of each axis after perm */
    using t6=tuple<int,int,int,int,int,int>;
    /* all possible permutations */
    vector<t6> transforms;
    vi p={ 0, 1, 2 };
    do {
        for(int x=1; x>=-1; x-=2) 
            for(int y=1; y>=-1; y-=2) 
                for(int z=1; z>=-1; z-=2) 
                    transforms.emplace_back(p[0], p[1], p[2], x, y, z);
    }while(next_permutation(all(p)));
    assert(transforms.size()==48);

    /* @return a scanner that is a transformation of scanner sci,
     * where trans is applied and sci[j] is moved to 0,0,0 */
    function<vvi(vvi,int,t6)> transform=[](vvi sci, int j, t6 trans) {
        vvi ans=sci;
        vi data=sci[j];
        for(size_t k=0; k<sci.size(); k++) {
            for(int kk=0; kk<3; kk++)
                sci[k][kk]-=data[kk];

            auto [p0,p1,p2,x,y,z]=trans;
            ans[k][p0]=x*sci[k][0];
            ans[k][p1]=y*sci[k][1];
            ans[k][p2]=z*sci[k][2];
        }

        return ans;
    };

    const t6 NONE={-1,-1,-1,-1,-1,-1};
    /* @return the transformation from sc[i] to sc[j], or NONE */
    function<t6(int,int,size_t&,size_t&)> overlap=[&](int i, int j, size_t &ii, size_t &jj) {
        for(ii=0; ii+1<sc[i].size(); ii++) {
            vvi sci=transform(sc[i], ii, transforms[0]);
            sort(sci.begin(), sci.end()-1);

            for(jj=0; jj+1<sc[j].size(); jj++) {
                /* sc[i][ii] and sc[j][jj] are the pivot points */
                for(t6 trans : transforms) {
                    vvi scj=transform(sc[j], jj, trans);
                    sort(scj.begin(), scj.end()-1);
                    vvi inter;
                    set_intersection(sci.begin(), sci.end()-1,
                            scj.begin(), scj.end()-1, back_inserter(inter));

                    if(inter.size()>=12)
                        return trans;
                }
            }
        }
        return NONE;
    };

    vvi adj(n);
    /* check all scanner pairs */
    for(int i=0; i<n; i++) 
        for(int j=i+1; j<n; j++) {
            size_t ii,jj;
            t6 trans=overlap(i,j,ii,jj);
            if(trans!=NONE) {
                cerr<<"trans found, i="<<i<<" j="<<j<<endl;
                adj[i].push_back(j);
                adj[j].push_back(i);
            }
        }

    vb vis(sc.size());

    function<void(int,int)> dfs=[&](int v, int p) {
        if(vis[v])
            return;

        vis[v]=true;

        cerr<<"dfs, v="<<v<<endl;

        for(int chl : adj[v]) {
            if(chl==p)
                continue;

            size_t ii,jj;
            t6 tr=overlap(v,chl,ii,jj);
            assert(tr!=NONE);
            sc[chl]=transform(sc[chl], jj, tr);
            vi data={   sc[v][ii][0]-sc[chl][jj][0],
                        sc[v][ii][1]-sc[chl][jj][1],
                        sc[v][ii][2]-sc[chl][jj][2]
            };
            for(int j=0; j<sc[chl].size(); j++) {
                for(int k=0; k<3; k++)
                    sc[chl][j][k]+=sc[v][ii][k];
            }
            dfs(chl,v);
        }
    };

    sc[0]=transform(sc[0], 0, transforms[0]);
    dfs(0,-1);

    set<vi> all;
    for(size_t i=0; i<sc.size(); i++) 
        for(size_t j=0; j<sc[i].size(); j++) 
            all.insert(sc[i][j]);

    cout<<"star1: "<<all.size()<<endl;

    int ans=0;
    for(size_t i=0; i<sc.size(); i++) 
        for(size_t j=i+1; j<sc.size(); j++) {
            ans=max(ans, 
                     abs(sc[i].back()[0]-sc[j].back()[0])
                    +abs(sc[i].back()[1]-sc[j].back()[1])
                    +abs(sc[i].back()[2]-sc[j].back()[2]));

        }

    cout<<"star2: "<<ans<<endl;

}

signed main() {
    solve();
}
