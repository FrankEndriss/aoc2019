/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
const int N=137;
void solve() {
    cinas(s,N);

    string line;
    while(line.size()<s[0].size())
        line+=".";

    bool done=false;
    int ans=0;
    while(!done) {
        done=true;

        for(int p=0; p<2; p++) {
            vs s0=vs(N,line);
            for(int i=0; i<N; i++) {
                for(size_t j=0; j<s[i].size(); j++) {
                    if(p==0) {
                        if(s[i][j]=='>') {
                            const int jj=(j+1)%s[i].size();
                            if(s[i][jj]=='.') {
                                s0[i][jj]='>';
                                s0[i][j]='.';
                                done=false;
                            } else
                                s0[i][j]='>';
                        } else if(s[i][j]=='v')
                            s0[i][j]='v';
                    } else if(p==1) {
                        if(s[i][j]=='v') {
                            const int ii=(i+1)%N;
                            if(s[ii][j]=='.') {
                                s0[ii][j]='v';
                                s0[i][j]='.';
                                done=false;
                            } else
                                s0[i][j]='v';
                        } else if(s[i][j]=='>')
                            s0[i][j]='>';
                    } else
                        assert(false);
                }
            }
            s.swap(s0);
        }
        ans++;
    }

    cout<<"star1="<<ans<<endl;


}

signed main() {
    solve();
}
