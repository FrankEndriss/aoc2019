/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++)
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/**
 * Do what is written.
 *
 * Consider "the first bit" to be the leftmost in input data.
 *
 * Note that converting the strings to integer is
 * counterproductive here. Just work on the strings instead.
 *
 * star2=1007985
 */
void solve() {
    string gs=gcin();
    string sep("\n");
    vs a=split(gs, sep);

    /* @ret -1, 0, 1 if in position i there are more 0, eq, 1 */
    function<int(int,vs&)> cntb=[&](int i, vs &v) {
        vi cnt(2);
        for(size_t j=0; j<v.size(); j++)
            cnt[v[j][i]-'0']++;

        return cnt[0]>cnt[1]?-1:cnt[0]==cnt[1]?0:1;
    };

    vector<vs> nnums;
    nnums.push_back(a);
    nnums.push_back(a);

    const int len=(int)a[0].size();
    for(int j=0; j<len; j++) {
            /* less */
            if(nnums[0].size()>1) {
                vs next;
                int val=cntb(j, nnums[0]);

                for(size_t i=0; i<nnums[0].size(); i++) {
                    int b=nnums[0][i][j]-'0';
                    if(b==0 && val>0)
                        next.push_back(nnums[0][i]);
                    else if(b==1 && val<0)
                        next.push_back(nnums[0][i]);
                    else if(val==0 && b==0)
                        next.push_back(nnums[0][i]);
                }
                nnums[0].swap(next);
            }

            /* more */
            if(nnums[1].size()>1) {
                vs next;
                int val=cntb(j, nnums[1]);

                for(size_t i=0; i<nnums[1].size(); i++) {
                    int b=nnums[1][i][j]-'0';
                    if(b==0 && val<0)
                        next.push_back(nnums[1][i]);
                    else if(b==1 && val>0)
                        next.push_back(nnums[1][i]);
                    else if(val==0 && b==1)
                        next.push_back(nnums[1][i]);
                }
                nnums[1].swap(next);
            }
    }

    assert(nnums[0].size()==1);
    assert(nnums[1].size()==1);

    int n1=0; int n2=0;
    for(int i=0; i<len; i++) {
        n1*=2;
        n2*=2;
        n1+=nnums[0][0][i]-'0';
        n2+=nnums[1][0][i]-'0';
    }
    int res=n1*n2;
    cout<<res<<endl;
}

signed main() {
    solve();
}
