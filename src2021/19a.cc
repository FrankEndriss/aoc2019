/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vvvvi= vector<vvvi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 * We need to transform the list of detected beacons
 * to a list of the relative offset of the beacons.
 *
 * We know scanner 0 is at 0,0,0
 * so find any that overlaps with that one.
 *
 * We convert each scanners list into a list vvi(26) of relative
 * positions per beacon.
 * aa[i][j][k]= {dx,dy,dz} of beacon j and k of scanner i
 * Then 12 of them must be same between 12 beacons of two scanners.
 *
 * ***
 * Also we need to transform a set of points in 24 orientations.
 * That is, we face in one of 6 directions of a cube, and are rotated in
 * one of 4 directions.
 * How to implement that correctly?
 * It seems that switching the axes by all 6 permutatios does not
 * do the job.
 *
 * ***
 * Copied a working solution at 3:00h to prevent myself from
 * waisting more time on this problem.
 * ...and also that did not work :/
 * ***
 * However, we can construct the 24 orientations like 6-tuples,
 * that is a permutation of the axes, and a signedness of the axes.
 * For simplyfication we simply to all of them, then it
 * is 6*8=48 cases, most likely we have each one doubled.
 */
void solve() {
    string gs=gcin();
    vs s=split(gs,"\n");
    cerr<<"s[0]="<<s[0]<<endl;

    //using t3=tuple<int,int,int>;
    vvvi a(26);
    int idx=0;

    for(int i=0; i<ssize(s); i++) {
        if(s[i].substr(0,3)=="---") {
            cerr<<s[i]<<endl;
            i++;
            while(s[i].size()>1) {
                a[idx].push_back(spliti(s[i],','));
                assert(ssize(a[idx].back())==3);
                i++;
            }
            cerr<<"size()="<<a[idx].size()<<endl;
            idx++;
        }
    }

    /* create the diffs matrix of one scanners beacons */
    function<vvvi(vvi&)> diffs=[&](vvi &sc) {
        //cerr<<"diffs"<<endl;
        vvvi ans(ssize(sc), vvi(ssize(sc), vi(3)));
        assert(sc[0].size()==3);

        for(int j=0; j<ssize(sc); j++) {
            for(int k=j+1; k<ssize(sc); k++) {
                /* beacon j and k of scanner */
                for(int d=0; d<3; d++) {
                    ans[j][k][d]=sc[j][d]-sc[k][d];
                    ans[k][j][d]=-ans[j][k][d];
                }
            }
        }

        //cerr<<"fini diffs"<<endl;
        return ans;
    };


    /* positions and a permutation of axes, create copy */
    function<vvi(vvi&, vi&)> cp=[&](vvi &pos, vi &p) {
        vvi ans(pos.size(), vi(3));
        for(int i=0; i<ssize(pos); i++) {
            assert(pos[i].size()==3);
            for(int k=0; k<3; k++)
                ans[i][p[k]]=pos[i][k];
        }
        return ans;
    };

    /* rotate points arround first axes by 90 degree */
    function<void(vvi&)> rotx=[&](vvi &pos) {
        for(int i=0; i<ssize(pos); i++)  {
            swap(pos[i][1],pos[i][2]);
            pos[i][1]=-pos[i][1];
        }
    };

    /* return the points in a that are also in b */
    function<set<vi>(vvi&,vvi&)> inter=[&](vvi &pa, vvi &b) {
        assert(pa.size()>20);
        assert(b.size()>20);
        set<vi> ans;
        for(int i=0; i<ssize(pa); i++) {
            assert(pa[i].size()==3);
            for(int j=0; j<ssize(b); j++) {
                assert(b[j].size()==3);
                if(pa[i]==b[j])
                    ans.insert(pa[i]);
            }
        }
        return ans;
    };

    /** check if there are 12 overlapping points the scanner i and scanner j; i!=j */
    function<bool(int,int)> match=[&](int i, int j) {
        cerr<<"match i="<<i<<" j="<<j<<endl;
        /* now we need to rotate/switch scanner j in 24 orientations.
         * This is, we need to iterate the 6 permutations of axis swap,
         * and foreach one the 4 possible rotations about one
         * of the axes. 
         * Then compare the distances between the beacons seen by the scanners.
         * If two such beacons have 12 same distances, then we can expect them
         * to be the same beacon.
         * */
        vvvi di=diffs(a[i]);
        vi perm= {0,1,2};
        do {
            /* copy/switch axes of scanner j */
            vvi pj=cp(a[j],perm);
            assert(pj.size()==a[j].size());
            for(int r=0; r<4; r++) {
                /* rotate pj three times about x axis */
                rotx(pj);
                vvvi dj=diffs(pj);

                /* now find a beacon in di and dj that
                 * have at least 12 other beacons distances in common 
                 * Hope that there are no false positives.
                 */
                //int cnt=0;
                //cerr<<"a[i].size()="<<a[i].size()<<" a[j].size()="<<a[j].size()<<endl;
                //cerr<<"di.size()="<<di.size()<<" dj.size()="<<dj.size()<<endl;
                //assert(di.size()==dj.size());
                for(size_t i1=0; i1<di.size(); i1++) {
                    for(size_t i2=0; i2<dj.size(); i2++) {
                        if(inter(di[i1],dj[i2]).size()>=12) {
                            cerr<<"seems match i="<<i<<" j="<<j<<endl;
                            return true;
                        }
                    }
                }

            }

        } while(next_permutation(all(perm)));
        return false;
    };

    /** here the program starts */
    set<int> notdone;
    for(int i=1; i<26; i++)
        notdone.insert(i);

    set<int> done;
    done.insert(0);

    while(notdone.size()) {
        bool fini=false;
        for(int i : done) {
            if(fini)
                break;
            for(int j : notdone) {
                if(match(i,j)) {
                    notdone.erase(j);
                    done.insert(j);
                    fini=true;
                    break;
                }
            }
        }
        assert(fini);
    }

    vvi ans;
    ans.push_back({0,0,0}); /* position scanner 0 */
    cerr<<"star2="<<ans[0][0]<<endl;

}

signed main() {
    solve();
}
