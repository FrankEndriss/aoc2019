
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
We got 16 pieces in 27 positions.
But only 4 different pieces.

#############
#...........#
###B#A#B#C###
  #D#C#B#A#
  #D#B#A#C#
  #C#D#D#A#
  #########

positions:
0..10 upper line
11-14 left slot upper, left slott lower
15,18 second slot upper, lower
19,22 third slot
23,26 right slot

pieces:
None=0
*/
#define A (1)
#define B (2)
#define C (3)
#define D (4)

using lstate=pair<vector<char>,vb>;

// return the Id of the piece in position pos */
inline signed piece(lstate state, signed pos) {
    return state.first[pos];
}

// set pi at pos, pi may be 0
inline lstate setpos(lstate state, signed pos, signed pi) {
    state.first[pos]=(char)pi;
    if(pos>10 && pi>0)
        state.second[pos]=true;
    return state;
}

/* some dumb dump */
void dump(lstate state) {
    string c=".ABCD";
    cerr<<"#";
    for(signed i=0; i<=10; i++)
        cerr<<c[piece(state,i)];
    cerr<<"#\n";
    for(int i=0; i<4; i++) {
        cerr<<"  #";
        for(int j=0; j<4; j++)
            cerr<<c[piece(state,11+i+j*4)]<<"#";
        cerr<<endl;
    }
}

const vector<signed> SC= { 0, 1, 10, 100, 1000 };

/* Well, brute force seems to be slow :/
 *
 * But, caused by the rules, the movement is quite limited:
 * If a piece is in own home in stays there
 * If a piece is in other home it moves to some pos in hallway
 * If a piece is in hallway, it moves into own home
 */
void solve() {
    lstate start;
    start.first.resize(27);
    start.second.resize(27);
    // above start position
    start=setpos(start,11,B);
    start=setpos(start,12,D);
    start=setpos(start,13,D);
    start=setpos(start,14,C);

    start=setpos(start,15,A);
    start=setpos(start,16,C);
    start=setpos(start,17,B);
    start=setpos(start,18,D);

    start=setpos(start,19,B);
    start=setpos(start,20,B);
    start=setpos(start,21,A);
    start=setpos(start,22,D);

    start=setpos(start,23,C);
    start=setpos(start,24,A);
    start=setpos(start,25,C);
    start=setpos(start,26,A);

    for(int i=0; i<27; i++)
        start.second[i]=false;

    lstate end;
    end.first.resize(27);
    end.second.resize(27);
    end=setpos(end,11,A);
    end=setpos(end,12,A);
    end=setpos(end,13,A);
    end=setpos(end,14,A);

    end=setpos(end,15,B);
    end=setpos(end,16,B);
    end=setpos(end,17,B);
    end=setpos(end,18,B);

    end=setpos(end,19,C);
    end=setpos(end,20,C);
    end=setpos(end,21,C);
    end=setpos(end,22,C);

    end=setpos(end,23,D);
    end=setpos(end,24,D);
    end=setpos(end,25,D);
    end=setpos(end,26,D);

    for(int i=0; i<27; i++)
        end.second[i]=false;

    vector<vector<signed>> adj(27);
    adj[0]= {1};
    adj[1]= {0,2};
    adj[2]= {1,3,11};
    adj[3]= {2,4};
    adj[4]= {3,5,15};
    adj[5]= {4,6};
    adj[6]= {5,7,19};
    adj[7]= {6,8};
    adj[8]= {7,9,23};
    adj[9]= {8,10};
    adj[10]= {9};
    adj[11]= {2,12};
    adj[12]= {11,13};
    adj[13]= {12,14};
    adj[14]= {13};
    adj[15]= {4,16};
    adj[16]= {15,17};
    adj[17]= {16,18};
    adj[18]= {17};
    adj[19]= {6,20};
    adj[20]= {19,21};
    adj[21]= {20,22};
    adj[22]= {21};
    adj[23]= {8,24};
    adj[24]= {23,25};
    adj[25]= {24,26};
    adj[26]= {25};

    const vector<vector<signed>> endpos= {
        {},
        { 11,12,13,14 },
        { 15,16,17,18 },
        { 19,20,21,22 },
        { 23,24,25,26 }
    };

    priority_queue<pair<signed,lstate>> q;	/* <score,state> */

    q.emplace(0LL, start);

    map<vector<char>,vector<pii>> path;
    map<vector<char>,signed> dp;
    dp[start.first]=0;
    int cnt=0;
    while(q.size()) {
        auto [score,state]=q.top();
        q.pop();
        cnt++;
        if(cnt%1024==0) {
        	cerr<<"score="<<-score<<" q.size()="<<q.size()<<" dp.size()="<<dp.size()<<endl;
        	dump(state);
        }

        if(state.first==end.first) {
            cout<<"star2= "<<-score<<endl;
            for(pii p : path[state.first])
                cerr<<"from: "<<p.first<<" to: "<<p.second<<endl;
            cerr<<"cnt="<<cnt<<endl;
            return;
        }

        auto it=dp.find(state.first);
        assert(it!=dp.end());
        assert(it->second<=-score);
        if(it->second!=-score)
            continue;

        /* move a piece from one to the other position, to should be emtpy */
        function<void(signed,signed,signed,signed)> domove=[&](signed pi, signed from, signed to, signed len) {
            //cerr<<"domove pi="<<pi<<" from="<<from<<" to="<<to<<" len="<<len<<endl;
            signed nscore=len*SC[pi]-score;
            lstate nstate=setpos(state,to,pi);
            nstate=setpos(nstate,from,0);
            it=dp.find(nstate.first);
            if(it==dp.end() || it->second>nscore) {
                //cerr<<"domove, push"<<endl;
                vector<pii> v=path[state.first];
                v.emplace_back(from, to);
                path[nstate.first]=v;
                dp[nstate.first]=nscore;
                q.emplace(-nscore,nstate);
            }
        };

        /* return possible pathlengths from from to to */
        function<vector<signed>(signed)> canMove=[&](signed from) {
            vector<signed> ans(27);
            queue<pair<signed,signed>> qq;	/* vertex,pathlen */
            qq.emplace(from,0);
            vb vis(27);
            vis[from]=true;
            while(qq.size()) {
                auto [v,len]=qq.front();
                qq.pop();
                for(signed chl : adj[v]) {
                    if(!vis[chl] && piece(state,chl)==0) {
                        ans[chl]=len+1;
                        qq.emplace(chl,len+1);
                        vis[chl]=true;
                    }
                }
            }
            ans[2]=ans[4]=ans[6]=ans[8]=0;	// never stop immediately outside any room
            if(from>10) { // dont move from room to room
                for(int j=11; j<27; j++)
                    ans[j]=0;
            }
            return ans;
        };

        for(signed pos=0; pos<27; pos++) {
            if(state.second[pos]) {
                continue;
            }

            const signed pi=piece(state,pos);
            if(pi==0)
                continue;

            //cerr<<"pi="<<pi<<" pos="<<pos<<endl;

            /* RULES:
            * If a piece is in own home it stays there
            * If a piece is in other home it moves to some pos in hallway
            * If a piece is in hallway, it moves into own home
            */

            vector<signed> to=canMove(pos);
            /*
                cerr<<"canMove: ";
                for(int i=0; i<27; i++)
                    cerr<<to[i]<<" ";
                cerr<<endl;
            */

            if(pos>10) {	// some home pos
                /* move to hallway */
                for(signed j=0; j<=10; j++) {
                    if(to[j]>0)
                        domove(pi, pos, j, to[j]);
                }
            }

            else if(pos<=10) {
                for(int epos=3; epos>=0; epos--) {
                    if(to[endpos[pi][epos]]>0) {
                        domove(pi, pos, endpos[pi][epos], to[endpos[pi][epos]]);
                    } else if(piece(state, endpos[pi][epos])!=pi) {
                        break;
                    }
                }
            }
        }
    }
    cerr<<"cnt="<<cnt<<endl;
    assert(false);

}
signed main() {
    solve();
}


