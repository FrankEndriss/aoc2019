/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}

/* Some complex tree operations :/
 * We got some definition of 
 * "the left snail" and "the right snail".
 */
struct snail {
    int val=-1;
    bool isL=false;
    snail *l=0,*r=0;
    snail *p;

    snail(bool pisL, snail* pp, int pval) {
        isL=pisL;
        p=pp;
        val=pval;
        l=r=0;
    }

    snail(bool pisL, snail* pp, snail *left, snail *right) {
        isL=pisL;
        p=pp;
        l=left;
        r=right;
    }

    /* @return the rightmost child of this */
    snail* getR() {
        if(r)
            return r->getR();
        else
            return this;
    }

    /* @return the leftmost child of this */
    snail* getL() {
        if(l)
            return l->getL();
        else
            return this;
    }

    /* @return the snail value left of this */
    snail* findL() {
        //cerr<<"findL, this="<<this<<endl;
        if(!isL) {
            if(p)
                return p->l->getR();
            else
                return 0;
        } else {
            if(p)
                return p->findL();
            else
                return 0;
        }
        assert(false);
    }

    /* @return the snail value right of this */
    snail* findR() {
        //cerr<<"findR, this="<<this<<endl;
        if(isL) {
            if(p)
                return p->r->getL();
            else
                return 0;
        } else {
            if(p)
                return p->findR();
            else
                return 0;
        }
        assert(false);
    }


    bool explode(int lvl) {
        if(l==0)
            return false;

        assert(l!=0);
        assert(r!=0);
        //cerr<<"explode this="<<this<<" lvl="<<lvl<<endl;

        if(lvl==4) {
            if(l->l!=0) {  /* l is not a val, so need to explode l */
                cerr<<"do explode l="<<l<<" sl="<<l->to_string()<<endl;
                assert(l->l->l==0); /* l->l is a val */
                assert(l->r->l==0); /* l->r is a val */
                snail *x=findL();
                if(x)
                    x->val+=l->l->val;
                else  {
                    //cerr<<"x notfound"<<endl;
                }

                r->getL()->val+=l->r->val;
                l=new snail(true,this,0); /* make l a val */
                return true;
            } else if(r->l!=0) { /* r is not a val, so need to explode r */
                cerr<<"do explode r="<<r<<endl;
                assert(r->l->l==0); /* r->l is a val */
                assert(r->r->l==0); /* r->r is a val */
                snail *x=findR();
                if(x) {
                    x->val+=r->r->val;
                } else {
                    //cerr<<"x notfound"<<endl;
                }

                l->getR()->val+=r->l->val;
                r=new snail(false,this,0);
                return true;
            }

            return false;

        } else {
            //cerr<<"explode, l="<<l<<" r="<<r<<endl;
            if(l && l->explode(lvl+1))
                return true;
            else if(r && r->explode(lvl+1))
                return true;
            else {
                //cerr<<"explode, ret=false"<<endl;
                return false;
            }
        }
        assert(false);
    }

    bool split() {
        if(l!=0) {
            if(l->split())
                return true;
            else if(r->split())
                return true;
            else
                return false;
        }

        // I am a number 
        if(val<10)
            return false;


        cerr<<"split, this="<<this<<endl;
        l=new snail(true, this,  val/2),
        r=new snail(false, this, (val+1)/2);
        return true;
    }

    void reduce() {
        do {
            while(explode(1));
        } while(split());
    }

    int mag() {
        if(l==0)
            return val;
        else
            return 3*l->mag() + 2*r->mag();
    }

    string to_string() {
        if(l==0)
            return to_string(val);
        else {
            string ans="[";
            ans+=l->to_string();
            ans+=',';
            ans+=r->to_string();
            ans+=']';
            return ans;
        }
    }

    string to_string(int i) {
        if(i==0)
            return "0";

        string ans;
        while(i) {
            ans+=(char)('0'+i%10);
            i/=10;
        }
        reverse(all(ans));
        return ans;
    }

    snail* copy(snail *pp) {
        snail *ans=new snail(isL, pp, val);
        if(l) {
            ans->l=l->copy(ans);
            ans->r=r->copy(ans);
        }
        return ans;
    }

};

/**
 */
const int N=1e5;
void solve() {
    string gs=gcin();
    vs ss=splitws(gs);

    vector<snail*> data(ss.size());

    for(int i=0; i<ssize(ss); i++) {
        gs=ss[i];
        int pos=0;

        function<int()> num=[&]() {
            assert(gs[pos]>='0' && gs[pos]<='9');
            int ans=0;
            while(gs[pos]>='0' && gs[pos]<='9') {
                ans*=10;
                ans+=gs[pos]-'0';
                pos++;
            }
            return ans;
        };

        function<snail*(bool,snail*)> parse=[&](bool isL, snail *p) {
            snail *ans=new snail(isL,p,0);

            assert(gs[pos]=='[');
            pos++;
            if(gs[pos]=='[')
                ans->l=parse(true,ans);
            else {
                ans->l=new snail(true, ans,num());
            }
            assert(gs[pos]==',');
            pos++;
            if(gs[pos]=='[')
                ans->r=parse(false,ans);
            else {
                ans->r=new snail(false, ans,num());
            }
            assert(gs[pos]==']');
            pos++;
            return ans;
        };

        data[i]=parse(true,0);
    }

    for(int i=0; i<ssize(data); i++)  {
        cerr<<"init reducing bvr="<<data[i]->to_string()<<endl;
        data[i]->reduce();
        cerr<<"init reducing aft="<<data[i]->to_string()<<endl;
    }

    int ans=0;
    for(int i=0; i<ssize(data); i++) {
        for(int j=i+1; j<ssize(data); j++) {

            snail *pI=data[i]->copy(0);
            snail *pJ=data[j]->copy(0);
            snail *aux=new snail(true, 0, pI, pJ);
            pI->p=aux;
            pI->isL=true;
            pJ->p=aux;
            pI->isL=false;
            aux->reduce();
            ans=max(ans, aux->mag());

            pI=data[i]->copy(0);
            pJ=data[j]->copy(0);
            aux=new snail(true, 0, pJ, pI);
            pJ->p=aux;
            pJ->isL=true;
            pI->p=aux;
            pI->isL=false;
            aux->reduce();
            ans=max(ans, aux->mag());
        }
    }

    cout<<"star2="<<ans<<endl;

}

signed main() {
    solve();
}
