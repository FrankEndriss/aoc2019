/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/**
 * We want to shot with minimal x speed, so it flys
 * as long as possible.
 * Then brute force with some guessed value windows.
 */
const int INF=1e9;
void solve() {
    int xmi=88;
    int xma=125;
    int ymi=-157;
    int yma=-103;

    int ans=0;
    int ans2=0;
    for(int vvx=3; vvx<1000; vvx++) {
        //cerr<<"vvx="<<vvx<<endl;
        for(int vvy=-158; vvy<3000; vvy++) {

            int vx=vvx;
            int vy=vvy;
            int ma=-INF;
            int lans=-INF;
            bool ok=false;

            int x=0;
            int y=0;

            while(y>ymi) {
                x+=vx;
                y+=vy;

                ma=max(ma, y);
                if(!ok && x>=xmi && x<=xma && y>=ymi && y<=yma) {
                    ok=true;
                    ans2++;
                }

                if(ok)
                    lans=ma;

                if(vx>0)
                    vx--;

                vy--;
            }

            if(lans>ans) {
                //cerr<<"lans="<<lans<<endl;
                ans=lans;
            }
        }
    }

    cout<<"star1="<<ans<<endl;
    cout<<"star2="<<ans2<<endl;
}

signed main() {
    solve();
}
