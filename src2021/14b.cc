/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++) 
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 */
void solve() {
    string s="PHVCVBFHCVPFKBNHKNBO";

    const int n=100;
    vector<vector<char>> m(256, vector<char>(256));
    for(int i=0; i<n; i++) {
        string s1, tmp;
        char s2;
        cin>>s1>>tmp>>s2;
        //cerr<<"s1="<<s1<<" s2="<<s2<<endl;
        m[s1[0]][s1[1]]=s2;
    }

    /* dp[I][J]=number of pairs IJ */
    vector<vector<int>> dp(256, vector<int>(256));

    for(int j=0; j+1<ssize(s); j++) {
        dp[s[j]][s[j+1]]++;
    }

    for(int i=0; i<40; i++) {
        vector<vector<int>> dp0(256, vector<int>(256));

        for(char c1='A'; c1<='Z'; c1++) {
            for(char c2='A'; c2<='Z'; c2++) {
                if(m[c1][c2]>0) {
                    dp0[c1][m[c1][c2]]+=dp[c1][c2];
                    dp0[m[c1][c2]][c2]+=dp[c1][c2];
                } else
                    dp0[c1][c2]+=dp[c1][c2];
            }
        }

        dp.swap(dp0);
    }

    vector<int> f(26);
    for(int i=0; i<26; i++) 
        for(int j=0; j<26; j++) {
            f[i]+=dp[i+'A'][j+'A'];
            f[j]+=dp[i+'A'][j+'A'];
        }

    f[s[0]-'A']++;
    f[s.back()-'A']++;

    for(int i=0; i<26; i++) 
        f[i]/=2;

    int ma=0;
    for(int i=0; i<26; i++) 
        if(f[i]>ma)
            ma=f[i];

    int mi=ma;
    for(int i=0; i<26; i++) 
        if(f[i]>0 && f[i]<mi)
            mi=f[i];

    int ans=ma-mi;
    cout<<"star2="<<ans<<endl;
}

signed main() {
    solve();
}
