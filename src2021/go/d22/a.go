package main

import (
	"bufio"
	. "fmt"
	"os"
	"sort"
)

func solve() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	offon := []string{"off", "on"}
	g := make([][]int, 0)
	for sc.Scan() {
		a := make([]int, 7)
		a[6] = sort.SearchStrings(offon, sc.Text())

		sc.Scan()
		s := sc.Text()
		Sscanf(s, "x=%d..%d,y=%d..%d,z=%d..%d", &a[0], &a[1], &a[2], &a[3], &a[4], &a[5])
		a[1]++
		a[3]++
		a[5]++
		g = append(g, a)

		if a[0] >= a[1] || a[2] >= a[3] || a[4] >= a[5] {
			panic("wrong input")
		}
	}

	isOn := func(x int, y int, z int) int {
		for i := 19; i >= 0; i-- {
			if x >= g[i][0] && x < g[i][1] && y >= g[i][2] && y < g[i][3] && z >= g[i][4] && z < g[i][5] {
				return g[i][6]
			}
		}
		return 0
	}

	/* simple brute force for part 1 */
	solve1 := func() int {
		ans := 0
		for x := -50; x <= 50; x++ {
			for y := -50; y <= 50; y++ {
				for z := -50; z <= 50; z++ {
					ans += isOn(x, y, z)
				}
			}
		}
		return ans
	}

	/*
		Part 2 is some wiered inclusion/exclusion.
		Consider only the on cubes. We can maintain a 2D-list of intersection counts.
		That is, a new cube is added to list[1], then all intersections with
		existing cubes in list[1] are edded to list[2], then all intersections with
		cubes in list[2] are added to list[3]... and so on.
		In the end we add all odd cubes, and remove all even.

		Then, what about swiching a cube off?
		Do the same, but do not add that cube to list[0]
		I guess :/
		...but somehow this does not work out :/
		****************
		We should better create a positive list, with cubes that have
		all on and do not intersect.
		Then on add a new cube, two cases:
		-cut off the intersection from all existing cubes,
		-and if on then add the new cube.
		Base of this is a function that cuts one cube out of another
		and returns the (mostly 6) remaining parts.
		That could work, at least for weak test data.
		*****************
		Other strategy is coordinate compression.
		For this we consider each value on each axis, there are at most 2*n each.
		If all coordinates where in that range, we could simply count the on/off pixels.
		And that what we do. Then, each pixel is worth the size of its cube.
		Note that this is O(n^4), which is theoretically up to 5e10 operations.
		However, with this testdata it goes only up to ~1e9
	*/
	solve2 := func() int64 {
		// slice of 3 maps, sic construction, thank you golang
		v := make([]map[int]bool, 3, 3)
		for i := range v {
			v[i] = make(map[int]bool)
		}

		for i := range g {
			for j := 0; j < 6; j++ {
				v[j/2][g[i][j]] = true
			}
		}
		vv := make([][]int, 3, 1000)
		for i := range v {
			for j := range v[i] {
				vv[i] = append(vv[i], j)
			}
			sort.Ints(vv[i])
		}

		// create the universe
		// again, thank you golang, what a sic construction
		//u := [len(vv[0])][len(vv[1])][len(vv[2])]bool
		u := make([][][]bool, len(vv[0]))
		for i := range u {
			u[i] = make([][]bool, len(vv[1]))
			for j := range u[i] {
				u[i][j] = make([]bool, len(vv[2]))
			}
		}

		cnt := int64(0)
		for i := range g { // add or remove
			// create mapping of g[i] to universe
			gg := [6]int{
				sort.SearchInts(vv[0], g[i][0]),
				sort.SearchInts(vv[0], g[i][1]),
				sort.SearchInts(vv[1], g[i][2]),
				sort.SearchInts(vv[1], g[i][3]),
				sort.SearchInts(vv[2], g[i][4]),
				sort.SearchInts(vv[2], g[i][5])}
			// set the pixels to on/off
			for i1 := gg[0]; i1 < gg[1]; i1++ {
				for i2 := gg[2]; i2 < gg[3]; i2++ {
					for i3 := gg[4]; i3 < gg[5]; i3++ {
						u[i1][i2][i3] = (g[i][6] == 1)
						cnt++
					}
				}
			}
		}

		ans := int64(0)
		cnt2 := int64(0)
		for i1 := 0; i1+1 < len(vv[0]); i1++ {
			for i2 := 0; i2+1 < len(vv[1]); i2++ {
				for i3 := 0; i3+1 < len(vv[2]); i3++ {
					cnt2++
					if u[i1][i2][i3] {
						// sic typecasting necessary with int64 :/
						val1 := int64(vv[0][i1+1] - vv[0][i1])
						val2 := int64(vv[1][i2+1] - vv[1][i2])
						val3 := int64(vv[2][i3+1] - vv[2][i3])
						ans += val1 * val2 * val3
					}
				}
			}
		}
		Fprintf(os.Stderr, "%d %d %d cnt=%d cnt2=%d\n", len(vv[0]), len(vv[1]), len(vv[2]), cnt, cnt2)
		return ans
	}

	Println("star1=", solve1())
	Println("star2=", solve2())
}

func main() {
	solve()
}
