package main

import (
	"bufio"
	. "fmt"
	"os"
	"strconv"
	//"sort"
	//"container/heap"
)

const _0 int64 = 0
const _1 int64 = 1
const INF int64 = 1e18 + 7

func btoi(b bool) int64 {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 int64, i2 int64) int64 { return []int64{i1, i2}[btoi(i1 < i2)] }
func min(i1 int64, i2 int64) int64 { return []int64{i2, i1}[btoi(i1 < i2)] }
func abs(i1 int64) int64           { return max(i1, -i1) }

func makeii(n int64, m int64) [][]int64 {
	a := make([][]int64, n)
	for i := _0; i < n; i++ {
		a[i] = make([]int64, m)
	}
	return a
}

var scanner *bufio.Scanner

func cini() int64 {
	scanner.Scan()
	ans, err := strconv.ParseInt(scanner.Text(), 10, 64)
	if err != nil {
		panic("cini failed")
	}
	return ans
}

func cinai(n int64) []int64 {
	ans := make([]int64, n)
	for i := range ans {
		ans[i] = cini()
	}
	return ans
}
func next() string {
	scanner.Scan()
	return scanner.Text()
}

/*
14 blocks of "inp", then calculation
We need to analyze block by block backwards to recover
what is done.
*/

type Alu struct {
	r [4]int64
}

func (a Alu) dump() {
	Fprintln(os.Stderr, a)
}

func (a *Alu) execProg(instr [][3]string, in int64) {
	if instr[0][0] != "inp" {
		panic("does not start with inp")
	}
	a.r[arg2r(instr[0][1])] = in

	Fprint(os.Stderr, "start execProg ")
	a.dump()
	for i := 1; i < len(instr); i++ {
		a.exec(instr[i][0], instr[i][1], instr[i][2])
	}
	Fprint(os.Stderr, "fini  execProg ")
	a.dump()
}

func cmd2op(cmd string) string {
	if cmd == "add" {
		return "+"
	} else if cmd == "mul" {
		return "*"
	} else if cmd == "div" {
		return "/"
	} else if cmd == "mod" {
		return "%"
	} else if cmd == "eql" {
		return "=="
	}
	panic("unknown cmd")
}

func exec1(cmd string, i1 int64, i2 int64) int64 {
	if cmd == "add" {
		return i1 + i2
	} else if cmd == "mul" {
		return i1 * i2
	} else if cmd == "div" {
		return i1 / i2
	} else if cmd == "mod" {
		return i1 % i2
	} else if cmd == "eql" {
		return btoi(i1 == i2)
	}
	Fprintln(os.Stderr, "unknown cmd=", cmd)
	panic("unknown cmd")
}

func arg2r(arg1 string) int {
	if arg1 == "w" {
		return 0
	} else if arg1 == "x" {
		return 1
	} else if arg1 == "y" {
		return 2
	} else if arg1 == "z" {
		return 3
	}
	return -1
}

func (a *Alu) exec(cmd string, arg1 string, arg2 string) {
	r1 := arg2r(arg1)
	v1 := a.r[r1]
	r2 := arg2r(arg2)
	v2 := _0
	if r2 < 0 {
		v2, _ = strconv.ParseInt(arg2, 10, 64)
	} else {
		v2 = a.r[r2]
	}
	//Fprintln(os.Stderr,"exec, cmd=", cmd, "arg1=", arg1, "arg2=",arg2, "r1=",r1,"r2=",r2,"v1=",v1,"v2=",v2)
	a.r[r1] = exec1(cmd, v1, v2)
	//a.dump()
}

func (a *Alu) reset() {
	a.r[0] = _0
	a.r[1] = _0
	a.r[2] = _0
	a.r[3] = _0
}

func (a *Alu) set(state [4]int64) {
	for i := range a.r {
		a.r[i] = state[i]
	}
}

func qexec0(in int64) [4]int64 {
	return [4]int64{in, 1, in + 2, in + 2}
}

func qexec1(in int64) [4]int64 {
	return [4]int64{in, 1, in + 16, in + 94}
}

func gen(cmd string, arg1 string, arg2 string) {
	Println("\t", arg1, "=", arg1, cmd2op(cmd), arg2, ";")
}

func solve() {
	progs := make([][][3]string, 0)
	for scanner.Scan() {
		op := scanner.Text()
		if op == "inp" {
			arg := next()
			ins := [3]string{"inp", arg, ""}
			p := make([][3]string, 0)
			p = append(p, ins)
			progs = append(progs, p)
		} else {
			left := next()
			right := next()
			ins := [3]string{op, left, right}
			i := len(progs) - 1
			progs[i] = append(progs[i], ins)
		}
		//i:=len(progs)-1
		//j:=len(progs[i])-1
		//Fprintln(os.Stderr, "op=", progs[i][j][0], " left=", progs[i][j][1], " right=", progs[i][j][2])
	}

	//a:=Alu{}

	Println("signed main() {")
	for j := 0; j < len(progs); j++ {
		if j==6 {
			Println("cerr<<i0<<i1<<i2<<i3<<i4<<i5<<endl;")
		}
		Printf("int xx%d=x; int yy%d=y; int zz%d=z;\n", j, j, j)
		Printf("for(signed i%d=1; i%d<=9; i%d++) {\n", j, j, j)
		Printf("x=xx%d; y=yy%d; z=zz%d;\n", j, j, j)
		Printf("w=i%d;\n",j)
		for i := 1; i < len(progs[0]); i++ {
			gen(progs[j][i][0], progs[j][i][1], progs[j][i][2])
		}
		if(j==13) {
			Printf("if(z==0) { cerr<<\"star1=\";")
			for k:=0; k<14; k++ {
				Printf("cerr<<i%d; ", k)
			}
			Printf("cerr<<endl; exit(0); }\n")
		}
	}
	for j := 0; j < len(progs); j++ {
		Println("}")
	}
	Println("assert(false);");
	Println("} // end main");
}

func main() {
	scanner = bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)
	solve()
}
