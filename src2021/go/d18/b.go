
package main;

import (
    "fmt"
    "bufio"
    "os"
    "strings"
)


/* Some complex tree operations :/
 * We got some definition of 
 * "the left snail" and "the right snail".
 */
type snail struct {
    val int
    isL bool
    l *snail
    r *snail
    p *snail
}


/*
    snail *p;

    snail(bool pisL, snail* pp, int pval) {
        isL=pisL;
        p=pp;
        val=pval;
        l=r=0;
    }

    snail(bool pisL, snail* pp, snail *left, snail *right) {
        isL=pisL;
        p=pp;
        l=left;
        r=right;
    }

    // @return the rightmost child of this 
    snail* getR() {
        if(r)
            return r->getR();
        else
            return this;
    }

    // @return the leftmost child of this
    snail* getL() {
        if(l)
            return l->getL();
        else
            return this;
    }

    // @return the snail value left of this 
    snail* findL() {
        //cerr<<"findL, this="<<this<<endl;
        if(!isL) {
            if(p)
                return p->l->getR();
            else
                return 0;
        } else {
            if(p)
                return p->findL();
            else
                return 0;
        }
        assert(false);
    }

    // @return the snail value right of this
    snail* findR() {
        //cerr<<"findR, this="<<this<<endl;
        if(isL) {
            if(p)
                return p->r->getL();
            else
                return 0;
        } else {
            if(p)
                return p->findR();
            else
                return 0;
        }
        assert(false);
    }


    bool explode(int lvl) {
        if(l==0)
            return false;

        assert(l!=0);
        assert(r!=0);
        //cerr<<"explode this="<<this<<" lvl="<<lvl<<endl;

        if(lvl==4) {
            if(l->l!=0) {
                cerr<<"do explode l="<<l<<" sl="<<l->to_string()<<endl;
                assert(l->l->l==0);
                assert(l->r->l==0);
                snail *x=findL();
                if(x)
                    x->val+=l->l->val;
                else  {
                    //cerr<<"x notfound"<<endl;
                }

                r->getL()->val+=l->r->val;
                l=new snail(true,this,0);
                return true;
            } else if(r->l!=0) {
                cerr<<"do explode r="<<r<<endl;
                assert(r->l->l==0);
                assert(r->r->l==0);
                snail *x=findR();
                if(x) {
                    x->val+=r->r->val;
                } else {
                    //cerr<<"x notfound"<<endl;
                }

                l->getR()->val+=r->l->val;
                r=new snail(false,this,0);
                return true;
            }

            return false;

        } else {
            //cerr<<"explode, l="<<l<<" r="<<r<<endl;
            if(l && l->explode(lvl+1))
                return true;
            else if(r && r->explode(lvl+1))
                return true;
            else {
                //cerr<<"explode, ret=false"<<endl;
                return false;
            }
        }
        assert(false);
    }

    bool split() {
        if(l!=0) {
            if(l->split())
                return true;
            else if(r->split())
                return true;
            else
                return false;
        }

        // I am a number 
        if(val<10)
            return false;


        cerr<<"split, this="<<this<<endl;
        l=new snail(true, this,  val/2),
        r=new snail(false, this, (val+1)/2);
        return true;
    }

    void reduce() {
        do {
            while(explode(1));
        } while(split());
    }

    int mag() {
        if(l==0)
            return val;
        else
            return 3*l->mag() + 2*r->mag();
    }

    string to_string() {
        if(l==0)
            return to_string(val);
        else {
            string ans="[";
            ans+=l->to_string();
            ans+=',';
            ans+=r->to_string();
            ans+=']';
            return ans;
        }
    }

    string to_string(int i) {
        if(i==0)
            return "0";

        string ans;
        while(i) {
            ans+=(char)('0'+i%10);
            i/=10;
        }
        reverse(all(ans));
        return ans;
    }

    snail* copy(snail *pp) {
        snail *ans=new snail(isL, pp, val);
        if(l) {
            ans->l=l->copy(ans);
            ans->r=r->copy(ans);
        }
        return ans;
    }

};
*/

type parser struct {
    s string
    pos int
}

func (this parser) front() rune {
    return this.s[this.pos]
}
func (this parser) pop() {
    this.pos++;
}
func (this parse) size() int {
    return len(this.s)-this.pos;
}

func (this parser) psnail(isL bool, p *snail) *snail {
        ans := snail{isL: isL, p: p}
        if(this.gc()!='[') {
            panic("psnail not [")
        }

        this.pos++
        if(this.s[this.pos]=='[') {
            ans.l=psnail(true, ans)
            if(this.s[this.pos]!=',') { panic("psnail not ,") }
            pos++;
            if(s[pos]!='[') { panic("psnail not [ 2") }
            ans.r=psnail(false, ans)
            if(s[pos]!=']') { panic("psnail not ]") }
            pos++
        } else {
            //if(s[pos]<'0' || s[pos]>'9') { panic("psnail not digit") }

            for pos<len(s) && s[pos]>='0' && s[pos]<='9' {
                ans.val*=10
                ans.val+=s[pos]-'0'
                pos++
            }
        }

        return &ans
}

func main() {
    in := bufio.NewReader(os.Stdin)

    data := make([]*snail)  // list of snail pointers

    for {
        line,err=in.ReadString('\n')
        if(err==nil) {
            p parser{line,0};
            append(data, p.psnail(true,nil))
        } else {
            break;
        }
    }
}

/**
void solve() {
    string gs=gcin();
    vs ss=splitws(gs);

    vector<snail*> data(ss.size());

    for(int i=0; i<ssize(ss); i++) {
        gs=ss[i];
        int pos=0;

        function<int()> num=[&]() {
            assert(gs[pos]>='0' && gs[pos]<='9');
            int ans=0;
            while(gs[pos]>='0' && gs[pos]<='9') {
                ans*=10;
                ans+=gs[pos]-'0';
                pos++;
            }
            return ans;
        };

        function<snail*(bool,snail*)> parse=[&](bool isL, snail *p) {
            snail *ans=new snail(isL,p,0);

            assert(gs[pos]=='[');
            pos++;
            if(gs[pos]=='[')
                ans->l=parse(true,ans);
            else {
                ans->l=new snail(true, ans,num());
            }
            assert(gs[pos]==',');
            pos++;
            if(gs[pos]=='[')
                ans->r=parse(false,ans);
            else {
                ans->r=new snail(false, ans,num());
            }
            assert(gs[pos]==']');
            pos++;
            return ans;
        };

        data[i]=parse(true,0);
    }

    for(int i=0; i<ssize(data); i++)  {
        cerr<<"init reducing bvr="<<data[i]->to_string()<<endl;
        data[i]->reduce();
        cerr<<"init reducing aft="<<data[i]->to_string()<<endl;
    }

    int ans=0;
    for(int i=0; i<ssize(data); i++) {
        for(int j=i+1; j<ssize(data); j++) {

            snail *pI=data[i]->copy(0);
            snail *pJ=data[j]->copy(0);
            snail *aux=new snail(true, 0, pI, pJ);
            pI->p=aux;
            pI->isL=true;
            pJ->p=aux;
            pI->isL=false;
            aux->reduce();
            ans=max(ans, aux->mag());

            pI=data[i]->copy(0);
            pJ=data[j]->copy(0);
            aux=new snail(true, 0, pJ, pI);
            pJ->p=aux;
            pJ->isL=true;
            pI->p=aux;
            pI->isL=false;
            aux->reduce();
            ans=max(ans, aux->mag());
        }
    }

    cout<<"star2="<<ans<<endl;

}

signed main() {
    solve();
}
 */
