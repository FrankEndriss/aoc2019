package main

import (
	"bufio"
	. "fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

const _0 int64 = 0
const _1 int64 = 1
const INF int64 = 1e18 + 7

func btoi(b bool) int64 {
	if b {
		return 1
	} else {
		return 0
	}
}

func max(i1 int64, i2 int64) int64 { return []int64{i1, i2}[btoi(i1 < i2)] }
func min(i1 int64, i2 int64) int64 { return []int64{i2, i1}[btoi(i1 < i2)] }
func abs(i1 int64) int64           { return max(i1, -i1) }

func makeii(n int64, m int64) [][]int64 {
	a := make([][]int64, n)
	for i := _0; i < n; i++ {
		a[i] = make([]int64, m)
	}
	return a
}

var sc *bufio.Scanner
var wr *bufio.Writer

//var er *bufio.Writer

func cini() int64 {
	sc.Scan()
	ans, err := strconv.ParseInt(sc.Text(), 10, 64)
	if err != nil {
		panic("cini failed")
	}
	return ans
}

type Sca struct {
	data [][3]int64
	diff [][][3]int64
}

func NewSca(n int) Sca {
	return Sca{
		make([][3]int64, n),
		make([][][3]int64, n)}
}

func cmpI(i1, i2 int64) int {
	if i1 == i2 {
		return 0
	} else if i1 < i2 {
		return -1
	} else {
		return 1
	}
}

func cmpBea(a1, a2 [3]int64) int {
	for i := 0; i < 3; i++ {
		if cmpI(a1[i], a2[i]) != 0 {
			return cmpI(a1[i], a2[i])
		}
	}
	return 0
}

func (sca *Sca) InitDiff() {
	//Fprintln(os.Stderr, "InitDiff")
	sca.diff = make([][][3]int64, len(sca.data))
	for i := range sca.data {
		sca.diff[i] = make([][3]int64, len(sca.data))
		for j := range sca.data {
			for k := 0; k < 3; k++ {
				sca.diff[i][j][k] = sca.data[j][k] - sca.data[i][k]
			}
		}
		sort.Slice(sca.diff[i], func(ii, jj int) bool {
			return cmpBea(sca.diff[i][ii], sca.diff[i][jj]) < 0
		})
		Fprintln(os.Stderr, "sca.diff[i]=", sca.diff[i])
	}
}

type Trans struct {
	p   [3]int64 // permutation of axes, 0,1,2
	neg [3]int64 // inverse of axes; 1,-1
}

// Transform a Sca by a Trans, return new Sca
func (sca Sca) trans(t Trans) Sca {
	//Fprintln(os.Stderr, "trans")
	ans := NewSca(len(sca.data))
	for i := range sca.data {
		for j := 0; j < 3; j++ {
			ans.data[i][t.p[j]] = sca.data[i][j] * t.neg[j]
		}
	}
	ans.InitDiff()
	return ans
}

// match the beacons of two Sca and if found a match returnt the offset
// of both scanners.
// This is done by finding same diffs in the diffs list of both scanners.
// Therefore we iterate all beacons of the first scanner, and foreach
// find the a beacon of the other scanner with same diff values.
// If it is 12 or more we assume it is the same beacon.
// If such one is found, return the position diff other-this.
func (sca Sca) intersection(other Sca) ([3]int64, bool) {
	Fprintln(os.Stderr, "intersection, sca.len=", len(sca.data), " other.len=", len(other.data))
	for i := range sca.data {
		for j := range other.data {

			d1, d2 := 0, 0
			cnt := 0
			for d1 < len(sca.diff[i]) && d2 < len(other.diff[j]) {
				c := cmpBea(sca.diff[i][d1], other.diff[j][d2])
				if c == 0 {
					cnt++
					d1++
					d2++
				} else if c < 0 {
					d1++
				} else {
					d2++
				}
			}
			if cnt > 12 {
				Fprintln(os.Stderr, "some 12")
				return [3]int64{
					other.data[j][0] - sca.data[i][0],
					other.data[j][1] - sca.data[i][1],
					other.data[j][2] - sca.data[j][2]}, true
			} else {
				Fprintln(os.Stderr, "i=", i, "j=", j, "cnt=", cnt, "sca.diff[i].len=", len(sca.diff[i]), "other.diff[j].len=", len(other.diff[j]), "d1=", d1, "d2=", d2)
			}
		}
	}

	return [3]int64{}, false
}

func parse() []Sca {
	a := make([]Sca, 0)
	for sc.Scan() {
		line := sc.Text()
		if strings.HasPrefix(line, "---") {
			a = append(a, NewSca(0))
		} else if len(line) < 3 {
			continue
		} else {
			var u, v, w int64
			Sscanf(line, "%d,%d,%d", &u, &v, &w)
			a[len(a)-1].data = append(a[len(a)-1].data, [3]int64{u, v, w})
		}
	}
	return a
}

var p = [6][3]int64{
	{0, 1, 2},
	{0, 2, 1},
	{1, 0, 2},
	{1, 2, 0},
	{2, 0, 1},
	{2, 1, 0}}

func transformations() []Trans {
	Fprintln(os.Stderr, "transformations()")
	tlist := make([]Trans, 0)
	for i := range p {
		for x := int64(-1); x <= 1; x += 2 {
			for y := int64(-1); y <= 1; y += 2 {
				for z := int64(-1); z <= 1; z += 2 {
					tlist = append(tlist, Trans{
						p[i],
						[3]int64{x, y, z}})
				}
			}
		}
	}
	if len(tlist) != 48 {
		panic("tlist.len!=48")
	}
	return tlist
}

func solve() {
	a := parse()
	for i := range a {
		a[i].InitDiff()
	}

	tlist := transformations()

	ans := make([][3]int64, len(a))
	ans[0] = [3]int64{0, 0, 0}
	done := make([]bool, len(a))
	done[0] = true
	cntdone := 1
	for cntdone < len(a) {
		Fprintln(os.Stderr, "cntdone=", cntdone)
		ldone := false
		for i := range ans {
			if !done[i] {
				continue
			}
			for j := 1; !ldone && j < len(a); j++ {
				if done[j] {
					continue
				}
				Fprintln(os.Stderr, "compare i=", i, "j=", j)

				for k := range tlist {
					tsca := a[j].trans(tlist[k])
					tsca.InitDiff()
					off, ok := a[i].intersection(tsca)
					if ok {
						panic("DEBUG, intersection ok")
						ans[j] = [3]int64{ans[i][0] + off[0], ans[i][1] + off[1], ans[i][2] + off[2]}
						done[j] = true
						cntdone++
						ldone = true
					}
					if ldone {
						break
					}
				}
			}
			if ldone {
				break
			}
		}
		if !ldone {
			Fprintln(os.Stderr, "cntdone=", cntdone)
			panic("something wrong")
		}
	}

	Fprintln(os.Stderr, len(a))
}

/*
Does not work, sic :/
*/
func main() {
	sc = bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanLines)
	wr = bufio.NewWriter(os.Stdout)
	//er = bufio.NewWriter(os.Stderr)
	defer wr.Flush()
	//defer er.Flush()
	//t:=cini()
	//for ; t>0; t-- {
	solve()
	//}
}
