/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}

int toint(string s) {
    istringstream iss(s);
    int i;
    iss>>i;
    return i;
}

/**
 */
void solve() {
    string gs=gcin();
    vs lines=split(gs, "\n");
    assert(lines.size()==895);

    vector<pii> fol;
    set<pii> p;
    for(int i=0; i<ssize(lines); i++) {
        if(lines[i].substr(0,3)=="fol") {
            subst(lines[i], '=', ' ');
            vs s=splitws(lines[i]);
            assert(s.size()==4);
            //cerr<<"lines[i]="<<lines[i]<<endl;
            //cerr<<"s.size()="<<s.size()<<endl;
            //cerr<<" s[2]="<<s[2]<<" s[3]="<<s[3]<<endl;
            int xy=(s[2]=="y");
            int val=toint(s[3]);

            fol.emplace_back(xy,val);
        } else {
            subst(lines[i], ',', ' ');
            vi a=splitwsi(lines[i]);
            assert(a.size()==2);

            p.emplace(a[0],a[1]);
        }
    }

    assert(fol.size()==12);


    for(size_t i=0; i<fol.size(); i++) {
        set<pii> p0;

        cerr<<"fol[i]="<<fol[i].first<<" "<<fol[i].second<<endl;
        for(auto [x,y] : p) {
            if(fol[i].first==0) { // fold x
                int nx=x;
                if(x>fol[i].second) {
                    int d=x-fol[i].second;
                    nx=x-d*2;
                }
                if(nx<fol[i].second)
                    p0.emplace(nx, y);
            } else {
                int ny=y;
                if(y>fol[i].second) {
                    int d=y-fol[i].second;
                    ny=y-d*2;
                }
                if(ny<fol[i].second)
                    p0.emplace(x, ny);
            }
        }
        p.swap(p0);
        if(i==0) {
            cout<<"star1="<<p.size()<<endl;
        }
    }


    vs ans(20, string(80,' '));
    for(auto [x,y] : p)  {
        ans[y][x]='#';
    }

    for(size_t i=0; i<ans.size(); i++)
        cout<<ans[i]<<endl;

}

signed main() {
    solve();
}
