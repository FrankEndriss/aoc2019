/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
#define int long long
using vi= vector<int>;


/** Evaluator of hex expressions like given in day16 puzzle.
 */
struct c16b {
    vi a;   /* current program */
    int pos; /* current offset */

    c16b(string s) {
        parse(s);
        pos=0;
    }

    /* load a new program and restart at begin, pos=0 */
    void parse(string s) {
        a.clear();
        pos=0;
        for(char c : s) {
            //cerr<<c;
            int i;
            if(c>='0' && c<='9')
                i=c-'0';
            else
                i=c-'A'+10;

            for(int j=3; j>=0; j--) {
                int b=(1&(i>>j));
                a.push_back(b);
                cerr<<b;
            }
            cerr<<' ';
        }
    }

    /* reads cnt bits as an integer */
    int r(int cnt) {
        int i=0;
        for(int j=0; j<cnt; j++)  {
            i*=2;
            i+=a[pos+j];
        }
        pos+=cnt;
        return i;
    };

    /* Read a literal value encoded as 5-bit packages. */
    int r4val() {
        cerr<<"\tr4val, pos="<<pos;
        int ans=0;
        int b;
        do {
            b=r(1);
            ans*=16;
            ans+=r(4);
        } while(b==1);
        cerr<<" ans="<<ans<<endl;
        return ans;
    };

    /** Exectes an operation on two values, returning the result */
    int op(int t, int aa, int bb) {
        if(t==0)
            return aa+bb;
        else if(t==1)
            return aa*bb;
        else if(t==2)
            return min(aa,bb);
        else if(t==3)
            return max(aa,bb);
        else if(t==5)
            return (int)(aa>bb);
        else if(t==6)
            return (int)(aa<bb);
        else if(t==7)
            return (int)(aa==bb);
        else
            assert(false);
    }

    /* Read the current suppackage, recursive.
     */
    int rSub() {
        cerr<<"rSub pos="<<pos<<endl;
        const int v=r(3);
        const int t=r(3);

        cerr<<"\tt="<<t<<" v="<<v<<endl;

        if(t==4) {
            return r4val();
        } else {
            int ltID=r(1);
            cerr<<"\tltID="<<ltID<<endl;
            if(ltID==0) {
                int len=r(15);  /* number of bits in subpackage */
                cerr<<"\tlen="<<len<<endl;
                int end=pos+len;
                int val=rSub();
                while(pos<end)
                    val=op(t,val,rSub());
                return val;
            } else {
                int cnt=r(11);  /* number of subpackages */
                cerr<<"\tsubpackages="<<cnt<<endl;
                int val=rSub();
                for(int i=1; i<cnt; i++)
                    val=op(t,val,rSub());
                return val;
            }
        }
        assert(false);
    }
};

/**
 * Todays puzzle expects us to read and debug for an hour.
 * Main errors:
 * -did parse letters 'A' to 'F' wrong binary code
 * -did not understand length/count length type ID
 * -implemented clumsy op() function and called it wrongly, ignoring its return value
 * -did not use a global position variable, instead a local one as parameter to the
 *  parsing functions
 */
void solve() {
    string gs;
    cin>>gs;

    c16b prog(gs);

    int ans=prog.rSub();
    cout<<"star2="<<ans<<endl;

}

signed main() {
    solve();
}
