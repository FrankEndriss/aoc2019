
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
We got 8 pieces in 19 positions.
But only 4 different pieces.
19*3=57

#############
#...........#
###B#A#B#C###
  #C#D#D#A#
  #########

positions:
0..10 upper line
11,12 left slot upper, left slott lower
13,14 second slot upper, lower
15,16 third slot
17,18 right slot

pieces:
None=0
*/
#define A (1)
#define B (2)
#define C (3)
#define D (4)

using lstate=pair<int,vb>;

// return the Id of the piece in position pos */
inline signed piece(lstate state, signed pos) {
    return (state.first>>(pos*3)) & 7;
}

// set pi at pos, pi may be 0
inline lstate setpos(lstate state, signed pos, signed pi) {
    int m=(7LL<<(pos*3));
    state.first&=~m;
    state.first|=(((int)pi)<<(pos*3));
    if(pos>10 && pi>0)	// if a piece was moved to home that position is done
        state.second[pos]=true;
    return state;
}

/* some dumb dump */
void dump(lstate state) {
    string c=".ABCD";
    cerr<<"#";
    for(signed i=0; i<=10; i++)
        cerr<<c[piece(state,i)];
    cerr<<"#\n";
    cerr<<"###"<<c[piece(state,11)]<<"#"
        <<c[piece(state,13)]<<"#"
        <<c[piece(state,15)]<<"#"
        <<c[piece(state,17)]<<"###\n";
    cerr<<"  #"<<c[piece(state,12)]<<"#"
        <<c[piece(state,14)]<<"#"
        <<c[piece(state,16)]<<"#"
        <<c[piece(state,18)]<<"#  \n";
}

const vector<signed> SC= { 0, 1, 10, 100, 1000 };

/* Well, brute force seems to be slow :/
 *
 * But, caused by the rules, the movement is quite limited:
 * If a piece is in own home in stays there
 * If a piece is in other home it moves to some pos in hallway
 * If a piece is in hallway, it moves into own home
 */
void solve() {
    lstate start;
    start.first=0;
    start.second.resize(19);
    // above start position
    start=setpos(start,11,B);
    start=setpos(start,12,C);
    start=setpos(start,13,A);
    start=setpos(start,14,D);
    start=setpos(start,15,B);
    start=setpos(start,16,D);
    start=setpos(start,17,C);
    start=setpos(start,18,A);
    cerr<<"sta="<<bitset<57>(start.first)<<endl;
    for(int i=0; i<19; i++)
        start.second[i]=false;

    lstate end;
    end.first=0;
    end.second.resize(19);
    end=setpos(end,11,A);
    end=setpos(end,12,A);
    end=setpos(end,13,B);
    end=setpos(end,14,B);
    end=setpos(end,15,C);
    end=setpos(end,16,C);
    end=setpos(end,17,D);
    end=setpos(end,18,D);
    cerr<<"end="<<bitset<57>(end.first)<<endl;
    for(int i=0; i<19; i++)
        end.second[i]=false;

    vector<vector<signed>> adj(19);
    adj[0]= {1};
    adj[1]= {0,2};
    adj[2]= {1,3,11};
    adj[3]= {2,4};
    adj[4]= {3,5,13};
    adj[5]= {4,6};
    adj[6]= {5,7,15};
    adj[7]= {6,8};
    adj[8]= {7,9,17};
    adj[9]= {8,10};
    adj[10]= {9};
    adj[11]= {2,12};
    adj[12]= {11};
    adj[13]= {4,14};
    adj[14]= {13};
    adj[15]= {6,16};
    adj[16]= {15};
    adj[17]= {8,18};
    adj[18]= {17};

    const vector<vector<signed>> endpos= {
        {},
        { 2, 11, 12 },
        { 4, 13, 14 },
        { 6, 15, 16 },
        { 8, 17, 18 }
    };

    priority_queue<pair<signed,lstate>> q;	/* <score,state> */

    q.emplace(0LL, start);

    map<int,vector<pii>> path;
    map<int,signed> dp;
    dp[start.first]=0;
    int cnt=0;
    while(q.size()) {
        cnt++;
        auto [score,state]=q.top();
        q.pop();

        if(state.first==end.first) {
            cout<<"star1= "<<-score<<endl;
            for(pii p : path[state.first])
                cerr<<"from: "<<p.first<<" to: "<<p.second<<endl;
            cerr<<"cnt="<<cnt<<endl;
            return;
        }

        auto it=dp.find(state.first);
        assert(it!=dp.end());
        assert(it->second<=-score);
        if(it->second!=-score)
            continue;

        if(cnt%100==0) {
        cerr<<"score="<<-score<<" state="<<bitset<57>(state.first)<<" q.size()="<<q.size()<<" dp.size()="<<dp.size()<<endl;
        dump(state);
        }

        /* move a piece from one to the other position, to should be emtpy */
        function<void(signed,signed,signed,signed)> domove=[&](signed pi, signed from, signed to, signed len) {
            //cerr<<"domove pi="<<pi<<" from="<<from<<" to="<<to<<" len="<<len<<endl;
            signed nscore=len*SC[pi]-score;
            lstate nstate=setpos(state,to,pi);
            nstate=setpos(nstate,from,0);
            it=dp.find(nstate.first);
            if(it==dp.end() || it->second>nscore) {
		    //cerr<<"domove, push"<<endl;
                vector<pii> v=path[state.first];
                v.emplace_back(from, to);
                path[nstate.first]=v;
                dp[nstate.first]=nscore;
                q.emplace(-nscore,nstate);
            }
        };

        /* return possible pathlengths from from to to */
        function<vector<signed>(signed)> canMove=[&](signed from) {
            vector<signed> ans(19);
            queue<pair<signed,signed>> qq;	/* vertex,pathlen */
            qq.emplace(from,0);
            vb vis(19);
            vis[from]=true;
            while(qq.size()) {
                auto [v,len]=qq.front();
                qq.pop();
                for(signed chl : adj[v]) {
                    if(!vis[chl] && piece(state,chl)==0) {
                        ans[chl]=len+1;
                        qq.emplace(chl,len+1);
                        vis[chl]=true;
                    }
                }
            }
            ans[2]=ans[4]=ans[6]=ans[8]=0;	// never stop immediately outside any room
            return ans;
        };

        for(signed pos=0; pos<19; pos++) {
            if(state.second[pos]) {
                continue;
            }

            const signed pi=piece(state,pos);
            if(pi==0)
                continue;

            //cerr<<"pi="<<pi<<" pos="<<pos<<endl;

            if(endpos[pi][1]==pos || endpos[pi][2]==pos)
                continue;	// own homepos

            /* RULES:
            * If a piece is in own home it stays there
            * If a piece is in other home it moves to some pos in hallway
            * If a piece is in hallway, it moves into own home
            */

            vector<signed> to=canMove(pos);
	    /*
            cerr<<"canMove: ";
            for(int i=0; i<19; i++)
                cerr<<to[i]<<" ";
            cerr<<endl;
	    */

            if(pos>10) {	// some home pos
                /* move to hallway */
                for(signed j=0; j<=10; j++) {
                    if(to[j]>0)
                        domove(pi, pos, j, to[j]);
                }
            }

            if(to[endpos[pi][2]]>0)
                domove(pi, pos, endpos[pi][2], to[endpos[pi][2]]);
            else if(piece(state, endpos[pi][2])==pi && to[endpos[pi][1]]>0) {
                domove(pi, pos, endpos[pi][1], to[endpos[pi][1]]);
            }
        }

    }
    	cerr<<"cnt="<<cnt<<endl;
    	assert(false);

}
signed main() {
    solve();
}


