/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/** bfs?
 * There should be no circles.
 *
 * **
 * Annoying description of rules for day2, needed to guess.
 */
void solve() {
    string gs=gcin();
    subst(gs, '-', ' ');
    istringstream iss(gs);

    map<string,int> id;
    id["start"]=0;
    id["end"]=1;

    vb big(20);
    vvi adj(2);
    for(int i=0; i<21; i++) {
        string s1, s2;
        iss>>s1>>s2;
        cerr<<"s1="<<s1<<" s2="<<s2<<endl;
        int i1,i2;
        auto it=id.find(s1);
        if(it==id.end()) {
            i1=id.size();
            id[s1]=i1;
            adj.resize(adj.size()+1);
            if(s1[0]>='A' && s1[0]<='Z')
                big[i1]=true;
        } else
            i1=it->second;


        it=id.find(s2);
        if(it==id.end()) {
            i2=id.size();
            id[s2]=i2;
            adj.resize(adj.size()+1);
            if(s2[0]>='A' && s2[0]<='Z')
                big[i2]=true;
        } else
            i2=it->second;

        adj[i1].push_back(i2);
        adj[i2].push_back(i1);

        cerr<<s1<<"="<<i1<<" "<<s2<<"="<<i2<<endl;
    }
    cerr<<"adj.size()="<<adj.size()<<endl;

    int ans=0;
    queue<pii> q;   /* vertex,visited */
    q.push({0,1});

    int twice=(int)adj.size();
    cerr<<"twice="<<twice<<endl;
    int twiceB=1<<twice;
    while(q.size()) {
        auto [v,m]=q.front();
        q.pop();

        //cerr<<"v="<<v<<" m="<<m<<endl;
        if(v==1) {
            ans++;
            continue;
        }

        for(int chl : adj[v]) {
            if(big[chl] || (m&(1<<chl))==0 || (m&twiceB)==0) {
                int mm=m|(1<<chl);
                if(!big[chl] && (m&(1<<chl))) {
                    assert((m&twiceB)==0);
                    mm|=twiceB;
                }

                if(v!=1 && chl!=0)
                    q.emplace(chl, mm);
            }
        }
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
