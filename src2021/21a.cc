#include <bits/stdc++.h>
using namespace std;

#define int ll
using vi= vector<int>;

/**
 */
int dp[2][32][32][10][10];
void solve() {

    /* dp[p][sc1][sc2][p1][p2]=nums of universes with that state
     * Note that c is not in state, since we get allways all three possibilities.
     *
     * How to handle the "roll die three times, then score"?
     * -> rolls twice without score, then once with score.
     *
     * But how comes the score of last three throws?
     * We somehow need to take care for all three throws at once.
     * Consider updating allways after 3 throws.
     * The universe created 27 copies, with 27 outcomes.
     */

    dp[0][0][0][6][1]=1;    /* player 1 start at position 7, player 2 at position 2 */

    for(int sc1=0; sc1<21; sc1++) {
        for(int sc2=0; sc2<21; sc2++) {
            for(int p1=0; p1<10; p1++) {
                for(int p2=0; p2<10; p2++) {
                    for(int r1=1; r1<=3; r1++) {
                        for(int r2=1; r2<=3; r2++) {
                            for(int r3=1; r3<=3; r3++) {
                                const int r=r1+r2+r3;
                                dp[1][sc1+(p1+r)%10+1][sc2][(p1+r)%10][p2]+=dp[0][sc1][sc2][p1][p2];
                                dp[0][sc1][sc2+(p2+r)%10+1][p1][(p2+r)%10]+=dp[1][sc1][sc2][p1][p2];
                            }
                        }
                    }
                }
            }
        }
    }

    vi w(2);
    for(int sc1=21; sc1<32; sc1++) {
        for(int sc2=0; sc2<21; sc2++) {
            for(int p1=0; p1<10; p1++) {
                for(int p2=0; p2<10; p2++) {
                    w[0]+=dp[1][sc1][sc2][p1][p2];
                }
            }
        }
    }

    for(int sc1=0; sc1<21; sc1++) {
        for(int sc2=21; sc2<32; sc2++) {
            for(int p1=0; p1<10; p1++) {
                for(int p2=0; p2<10; p2++) {
                    w[1]+=dp[0][sc1][sc2][p1][p2];
                }
            }
        }
    }

    cout<<max(w[0],w[1])<<endl;;
}

signed main() {
    solve();
}
