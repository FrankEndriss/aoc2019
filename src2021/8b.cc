/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++) 
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}

set<char> to_set(string s) {
    set<char> ans;
    for(char c : s)
        ans.insert(c);
    return ans;
}

/* some operator overloading for sets operations 
 * -, ^, |, & 
 * minus, xor, or, and
 **/
template <typename T>
set<T> to_set(const vector<T> s) {
    set<T> ans;
    copy(all(s), inserter(ans, ans.end()));
    return ans;
}

template <typename T>
vector<T> to_vector(const set<T> s) {
    vector<T> ans;
    copy(all(s), back_inserter(ans));
    return ans;
}

template <typename T>
set<T> operator-(const set<T> &s1, const set<T> &s2) {
    set<T> ans;
    set_difference(all(s1), all(s2), inserter(ans, ans.end()));
    return ans;
}
template <typename T>
set<T>& operator-=(set<T> &s1, const set<T> &s2) {
    return (s1=s1-s2);
}

template <typename T>
set<T> operator^(const set<T> &s1, const set<T> &s2) {
    set<T> ans;
    set_symmetric_difference(all(s1), all(s2), inserter(ans, ans.end()));
    return ans;
}
template <typename T>
set<T>& operator^=(set<T> &s1, const set<T> &s2) {
    return (s1=s1^s2);
}

template <typename T>
set<T> operator|(const set<T> &s1, const set<T> &s2) {
    set<T> ans;
    set_union(all(s1), all(s2), inserter(ans, ans.end()));
    return ans;
}
template <typename T>
set<T>& operator|=(set<T> &s1, const set<T> &s2) {
    return (s1=s1|s2);
}

template <typename T>
set<T> operator&(const set<T> &s1, const set<T> &s2) {
    set<T> ans;
    set_intersection(all(s1), all(s2), inserter(ans, ans.end()));
    return ans;
}
template <typename T>
set<T>& operator&=(set<T> &s1, const set<T> &s2) {
    return (s1=s1&s2);
}

const vvi seg= {
    { 1, 1, 1, 0, 1, 1, 1 },    // 0
    { 0, 0, 1, 0, 0, 1, 0 },
    { 1, 0, 1, 1, 1, 0, 1 },
    { 1, 0, 1, 1, 0, 1, 1 },
    { 0, 1, 1, 1, 0, 1, 0 },
    { 1, 1, 0, 1, 0, 1, 1 }, // 5
    { 1, 1, 0, 1, 1, 1, 1 },
    { 1, 0, 1, 0, 0, 1, 0 }, // 7
    { 1, 1, 1, 1, 1, 1, 1 }, // 8
    { 1, 1, 1, 1, 0, 1, 1 } // 9
};

int cnt(string s, int c) {
    for(size_t i=0; i<s.size(); i++) 
        if(s[i]==c)
            return 1;

    return 0;
}

/**
 * decrypting:
 * We need to find the permutation p[]
 * mixing the 7 signals.
 *
 * Consider the numbers 1,4,7,8
 * The 8 tells us nothing
 * The 1 tells us the which segs the 3 and 6 are, but maybe switched
 * R1: If we find a digit with only one of 3,6 in it, it is a 2
 *
 * R2: The third char in the seven is p[0]
 *
 * R3: If we find a nine and a eight, then the missig digit
 * in the nine is p[5]
 *
 * ...but that does not work out :/
 * ***
 * More general:
 * Each p[k] can be any of the 7 values.
 * Maintain a set for each p[k], containing the possible 
 * values. Whenever the size of the set eq 1 that position is solved.
 *
 * Then, when we can detect a number in the input, we know its set
 * of segments, and therefore maybe can remove values from the sets
 * of this segments.
 * How to detect numbers:
 * 2: contains p[2] but not p[5]
 *
 * If 6 segs are used, seg 0,3,6 are allways used.
 * If 7 segs are used, seg 0,1,5,6 are allways used.
 *
 *
 */

void dump(vector<set<char>> &ans) {
    for(size_t i=0; i<ans.size(); i++) {
        cerr<<"seg="<<i<<" ";
        for(char c : ans[i])
            cerr<<c;
        cerr<<endl;
    }
}

/** 
 * if  any letter is used in only one of the given sets in idx,
 * then we are sure that letter is ans for that one index.
 * So we can remove it from all others, and all other letters from that set.
 **/
void check_single(vector<set<char>> &ans, vi idx) {
    for(char c='a'; c<='g'; c++) {
        int cnt=0;
        int lidx=-1;
        for(size_t i=0; i<idx.size(); i++) {
            int lcnt=ans[idx[i]].count(c);
            cnt+=lcnt;
            if(lcnt==1)
                lidx=idx[i];
        }

        if(cnt==1) {    /* only segment lidx contains c */
            for(size_t i=0; i<7; i++) {
                if(i==lidx) {
                    if(ans[i].size()>1) {
                        ans[i].clear();
                        ans[i].insert(c);
                        cerr<<"found char of seg="<<i<<" c="<<c<<endl;
                    }
                } else {
                    if(ans[i].count(c)>0) {
                        ans[i].erase(c);
                        cerr<<"removed char from seg="<<i<<" c="<<c<<endl;
                        cerr<<"after:"<<endl;
                        dump(ans);
                    }
                }
            }
        }
    }
}

set<char> solved(vector<set<char>> &ans) {
    set<char> ret;
    for(size_t i=0; i<ans.size(); i++) 
        if(ans[i].size()==1)
            ret.insert(*ans[i].begin());
    return ret;
}

/** Some WA at input line==1
 * most likely some error in the logic of
 * recognizing the numbers :/
 * ******
 * Sol:
 * Check all permutations of size()==7.
 * At one of those all 10 numbers in input makes sense, and that is ans.
 * Its a bit complecated caused by all those indirections :/
 */

void solve() {
    vs nums={ 
        "abcefg",
        "cf",
        "acdeg",
        "acdfg",
        "bcdf",
        "abdfg",
        "abdefg",
        "acf",
        "abcdefg",
        "abcdfg"
    };
    vs nums2=nums;
    sort(all(nums));


    const int n=200;

    int ans=0;
    for(int i=0; i<n; i++) {
        cinas(le,10);
        string _;
        cin>>_;
        cinas(ri,4);

        vi perm={ 0, 1, 2, 3, 4, 5, 6 };
        do {
            vs prod=le;
            assert(prod.size()==10);
            for(size_t k=0; k<prod.size(); k++) {
                for(size_t l=0; l<prod[k].size(); l++)
                    prod[k][l]='a'+perm[prod[k][l]-'a'];
                sort(all(prod[k]));
            }
            sort(all(prod));

            if(prod==nums) {
                cerr<<"found sol at i="<<i<<" sol="<<perm[0]<<perm[1]<<perm[2]<<perm[3]<<perm[4]<<perm[5]<<perm[6]<<endl;
                int lans=0;
                for(int k=0; k<4; k++) {
                    for(size_t l=0; l<ri[k].size(); l++)
                        ri[k][l]='a'+perm[ri[k][l]-'a'];
                    sort(all(ri[k]));
                    auto it=find(all(nums2), ri[k]);
                    assert(it!=nums2.end());

                    int dig=distance(nums2.begin(), it);
                    lans*=10;
                    lans+=dig;
                }
                cerr<<"lans="<<lans<<endl;
                ans+=lans;
                break;
            }

        }while(next_permutation(all(perm)));
    }

    cout<<"ans="<<ans<<endl;
    cout<<"fini"<<endl;


}

signed main() {
    solve();
}
