/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 */
void solve() {
    string gs;
    cin>>gs;
    vi a;
    for(char c : gs) {
        //cerr<<c;
        int i;
        if(c>='0' && c<='9')
            i=c-'0';
        else
            i=c-'A'+10;

        for(int j=3; j>=0; j--) {
            int b=1&(i>>j);
            a.push_back(b);
            cerr<<b;
        }
        cerr<<' ';
    }
    assert(a.size()==gs.size()*4);
    cerr<<endl;


    cerr<<"a.size()="<<a.size()<<endl;

    function<int(int&,int)> r=[&](int &pos, int cnt) {
        int i=0;
        for(int j=0; j<cnt; j++)  {
            i*=2;
            i+=a[pos+j];
        }
        pos+=cnt;
        return i;
    };

    /* Read package of type 4, a literal value */
    function<int(int&)> r4val=[&](int &pos) {
        cerr<<"r4val, pos="<<pos<<endl;
        int ans=0;
        int b;
        do {
            b=r(pos,1);
            ans*=16;
            ans+=r(pos,4);
        }while(b==1);
        return ans;
    };

    int ans=0;
    /* Read the current suppackage, recursive,
     * and add ans+=t;
     */
    function<void(int&)> rSub=[&](int &pos) {
        cerr<<"rSub pos="<<pos<<endl;
        int v=r(pos,3);
        int t=r(pos,3);

        cerr<<"\tt="<<t<<" v="<<v<<endl;
        ans+=v;

        if(t==4) {
            r4val(pos);
            return;
        } else {
            int ltID=r(pos,1);
            cerr<<"\tltID="<<ltID<<endl;
            if(ltID==0) {
                int len=r(pos,15);  /* number of bits in subpackage */
                cerr<<"\tlen="<<len<<endl;
                int end=pos+len;
                while(pos<end)
                    rSub(pos);

            } else {
                int cnt=r(pos,11);  /* number of subpackages */
                cerr<<"\tsubpackages="<<cnt<<endl;
                for(int i=0; i<cnt; i++) 
                    rSub(pos);
            }
        }
    };

    int pos=0;
    rSub(pos);

    cout<<ans<<endl;

}

signed main() {
    solve();
}
