/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, int c1, int c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++) 
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


int cmp3(int a, int b) {
    return (b>a)-(a>b);
}

/**
 * Note: "Just consider horizontal and vertical lines" means that there
 * are also diagonal ones, and we need to check.
 */
void solve() {
    char c;
    vvi dp(1000, vi(1000));
    int cnt=0;

    int ans=0;
    while(true) {
        int i1,j1,i2,j2;
        cin>>i1>>c>>j1>>c>>c>>i2>>c>>j2;

        if(!cin.good())
            break;

        cnt++;

        int di=cmp3(i1,i2);
        int dj=cmp3(j1,j2);
        //cerr<<"i1="<<i1<<" j1="<<j1<<" i2="<<i2<<" j2="<<j2<<" di="<<di<<" dj="<<dj<<endl;
        i2+=di;
        j2+=dj;

        if(i1==i2 || j1==j2) {
            for(int i=i1,j=j1; i!=i2 || j!=j2; i+=di, j+=dj) {
                dp[i][j]++;
                ans+=(dp[i][j]==2);
            }
        }
    }

    assert(cnt==500);
    cout<<ans<<endl;
}

signed main() {
    solve();
}
