/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vvvb= vector<vvb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string &s) {
    vs ss=splitws(s);
    vi ans(ss.size());
    for(size_t i=0; i<ans.size(); i++)
        ans[i]=atoi(ss[i].c_str());
    return ans;
}

/* split by separator */
vs split(string &s, string &sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}


/**
 */
const int N=100;
void solve() {
    //string gs=gcin();
    //string sep("\n");
    //vs a=split(gs, sep);

    string line;
    getline(cin,line);
    subst(line, ',', ' ');
    cerr<<"in line="<<line<<endl;

    vvvi a(N, vvi(5, vi(5)));
    int sum=0;
    for(int i=0; i<N; i++) {
        for(int j=0; j<5; j++) {
            for(int k=0; k<5; k++) {
                cin>>a[i][j][k];
                sum+=a[i][j][k];
            }
        }
    }

    vvvb vis(N, vvb(5, vb(5)));
    istringstream iss(line);
    bool fini=false;
    while(!fini && iss.good()) {
        int aux;
        iss>>aux;
        cerr<<"in="<<aux<<endl;
        for(int i=0; !fini && i<N; i++) {
            for(int j=0; !fini && j<5; j++) {
                for(int k=0; !fini && k<5; k++) {

                    if(a[i][j][k]==aux && !vis[i][j][k]) {
                        vis[i][j][k]=true;
                        sum-=a[i][j][k];
                    }
                }
            }
        }

        for(int i=0; !fini && i<N; i++) {
            for(int j=0; !fini && j<5; j++) {
                for(int k=0; !fini && k<5; k++) {

                    int cnt1=0;
                    int cnt2=0;
                    for(int l=0; l<5; l++) {
                        if(vis[i][j][l])
                            cnt1++;
                        if(vis[i][l][k])
                            cnt2++;
                    }
                    if(cnt1==5 || cnt2==5) {

                        sum=0;
                        for(int i2=0; i2<5; i2++) {
                            for(int j2=0; j2<5; j2++) {
                                if(!vis[i][i2][j2])
                                        sum+=a[i][i2][j2];
                            }
                        }
                        fini=true;
                        cout<<sum*aux<<endl;
                        return;
                    }
                }
            }
        }
    }

    assert(false);
}

signed main() {
    solve();
}
