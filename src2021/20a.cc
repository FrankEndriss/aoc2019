/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 * This puzzle has some twist, since all the infinite
 * pixels flip at each round, but since we need to calc
 * the number of pixels after _even_ number of rounds,
 * that can be kind of ignored.
 * However, we need to simulate that flickering for
 * a part of the universe, namely the border of the image.
 */
const int N=101;
void solve() {
    string gs=gcin();
    vs s=split(gs,"\n");

    string alg=s[0];
    assert(alg.size()==512);
    assert(s[1].size()==0);
    assert(s[2].size()==100);

    s.erase(s.begin());
    s.erase(s.begin());

    vs a;
    string aux(s[0].size()+2*N, '.');
    for(int i=0; i<N; i++)
        a.push_back(aux);

    string ss(N, '.');
    for(size_t i=0; i<s.size(); i++)  {
        a.push_back(ss+s[i]+ss);
    }
    for(int i=0; i<N; i++)
        a.push_back(aux);
    assert(a.size()==s.size()+2*N);

    function<int(int,int)> v9=[&](int i, int j) {
        int v=0;
        for(int ii=i-1; ii<=i+1; ii++)
            for(int jj=j-1; jj<=j+1; jj++)  {
                v*=2;
                if(a[ii][jj]=='#')
                    v++;
            }
        return v;
    };

    int ans=0;
    for(int k=0; k<50; k++) {

        int cnt=0;
        vs a0(a.size(), string(a[0].size(), '.'));

        for(int i=1; i+1<ssize(a); i++) {
            for(int j=1; j+1<ssize(a[0]); j++) {
                int v=v9(i,j);
                a0[i][j]=alg[v];
                cnt+=(a0[i][j]=='#');
            }
        }

        /* flip the border pixels */
        for(int i=0; i<ssize(a0); i++)
            a0[0][i]=a0.back()[i]=a0[i][0]=a0[i].back()="#."[k%2];

        a=a0;
        ans=cnt;
        //cerr<<"k="<<k<<" cnt="<<cnt<<endl;
    }

    cout<<ans<<endl;

}

signed main() {
    solve();
}
