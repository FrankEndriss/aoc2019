/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++) 
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 */
void solve() {
    string gs=gcin();
    vs s=splitws(gs);

    const int N=s.size();
    const int M=s[0].size();

    vvi a(N, vi(M));
    for(int i=0; i<N; i++) 
        for(int j=0; j<M; j++) 
            a[i][j]=s[i][j]-'0';

    vvb vis(N, vb(M));

    for(int i=0; i<N; i++) {
        /* rowwise check cols left to right */
        vis[i][0]=true;
        int ma=a[i][0];
        for(int j=1; j<M; j++) {
            if(a[i][j]>ma) {
                vis[i][j]=true;
                ma=a[i][j];
            }
        }

        /* rowwise check cols right to left */
        vis[i][M-1]=true;
        ma=a[i][M-1];
        for(int j=M-2; j>=0; j--) {
            if(a[i][j]>ma) {
                vis[i][j]=true;
                ma=a[i][j];
            }
        }
    }

    for(int j=0; j<M; j++) {
        /* colwise check rows from top to down */
        vis[0][j]=true;
        int ma=a[0][j];
        for(int i=1; i<N; i++) {
            if(a[i][j]>ma) {
                vis[i][j]=true;
                ma=a[i][j];
            }
        }

        /* colwise check rows from bottom to top */
        vis[N-1][j]=true;
        ma=a[N-1][j];
        for(int i=N-2; i>=0; i--) {
            if(a[i][j]>ma) {
                vis[i][j]=true;
                ma=a[i][j];
            }
        }
    }

    int ans=0;
    for(int i=0; i<N; i++) 
        ans+=accumulate(all(vis[i]), 0LL);

    cout<<ans<<endl;

}

signed main() {
    solve();
}
