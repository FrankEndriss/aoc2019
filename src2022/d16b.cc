/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


template<typename T>
void floyd_warshal(vector<vector<T>> &d, int n) {
    for (int k = 0; k < n; ++k) {
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                d[i][j] = min(d[i][j], d[i][k] + d[k][j]);
            }
        }
    }
}

map<string,int> sym;
int toId(string s) {
    auto it=sym.find(s);
    if(it!=sym.end())
        return it->second;

    int id=(int)sym.size();
    sym[s]=id;
    return id;
}

/**
 * start at AA
 *
 * dp
 * dp[v][i][m]= max release we can get if entering valve v at time i with mask m
 *  where mask m denotes the allready visited valves.
 */
const int INF=1e9;
void solve() {
    string gs=gcin();

    subst(gs, ',', ' ');
    subst(gs, '=', ' ');

    set<string> vv; /* relevant valves */

    map<string,set<string>> adjM;
    map<string,int> flows;
    for(string s : split(gs, "\n")) {
        vs ss=splitws(s);

        string valve=ss[1];
        int flow=stol(ss[5]);
        flows[valve]=flow;
        if(flow>0 || valve=="AA")
            vv.insert(valve);
        cerr<<"valve="<<valve<<" flow="<<flow<<" ";
        for(size_t j=10; j<ss.size(); j++) {
            cerr<<ss[j]<<" ";
            adjM[valve].insert(ss[j]);
        }
        cerr<<endl;
    }

    cerr<<"fini parsing"<<endl;

    set<int> vvs;   /* IDs of valves flow>0 */
    vi iflows(vv.size());
    for(string sid : vv) {
        int id=toId(sid);
        vvs.insert(id);
        iflows[id]=flows[sid];
    }
    cerr<<"after iflows"<<endl;

    const int N=(int)flows.size();
    vvi cost(N, vi(N, INF));

    for(auto ent : adjM) {
        int id=toId(ent.first);
        for(string chl : ent.second)
            cost[id][toId(chl)]=1;
    }

    floyd_warshal(cost, cost.size());
    cerr<<"after floydw"<<endl;



    vvi adj(vv.size()); /* adjacenty matrix of valves flow>0, cost are in cost[][] */
    for(int id : vvs) {
        for(size_t j=0; j<cost.size(); j++) {
            if(vvs.count(j)>0 && cost[id][j]<INF) {
                adj[id].push_back(j);
            }
        }
    }
    cerr<<"after adj"<<endl;

    /* now iterate the dp 
     * We do like in bfs here, but simply iterating
     * the masks from 0 to 1<<MM would work the same.
     **/

    const int aa=toId("AA");
    const int M=27;   /* max minute */
    cerr<<"adj.size()="<<adj.size()<<endl;
    const int MM=16;
    assert(MM==(int)adj.size());

    vvvi dp(MM, vvi(M, vi(1<<MM, 0)));
    using t4=tuple<int,int,int,int>;
    queue<t4> q;
    q.emplace(aa, 0, 1<<aa, 0);

    while(q.size()) {
        auto [v,t,mask,val]=q.front();
        q.pop();
        if(t+1==M || dp[v][t][mask]!=val)
            continue;

        cerr<<"v="<<v<<" t="<<t<<" mask="<<bitset<MM>(mask)<<" val="<<val<<endl;

        for(int chl : adj[v]) {
            const int cmask=mask|(1<<chl);
            if(cmask!=mask) { /* valve chl not open jet */
                int tt=t+cost[v][chl]+1;    /* one minute opening the valve */
                if(tt<M) {
                    const int nval=dp[v][t][mask]+(M-tt-1)*iflows[chl];
                    if(dp[chl][tt][cmask] < nval) {
                        dp[chl][tt][cmask]=nval;
                        q.emplace(chl, tt, cmask, nval);
                    }
                }
            }
        }
    }

    /* optimize the dp
     * that is, on each minute, we can update all following minutes with the max
     * value.
     * then we have to check afterwards only minute M-1
     */
    for(int j=0; j<(1<<MM); j++)
        for(int i=0; i<MM; i++)
            for(int k=1; k<M; k++)
                dp[i][k][j]=max(dp[i][k][j], dp[i][k-1][j]);

    /* For star2 observe that
     * the pathes of both actors are disjoint.
     * So, check the sum of two disjoint pathes.
     */
    int ans=0;
    for(size_t j=0; j<(1<<MM); j++) {
        if((j&(1<<aa))==0)
            continue;

        cerr<<"j="<<bitset<MM>(j)<<endl;

        int jjj=0;
        for(int idx=0; idx<MM; idx++) {
            if((j&(1<<idx))==0) {
                jjj+=(1<<idx);
            }
        }
        jjj+=(1<<aa);

        assert((j|jjj) == (1<<MM)-1);

        for(int jj=jjj; jj>=0; jj=(jj-1)&jjj) {
            for(size_t i=0; i<MM; i++) {
                for(size_t ii=0; ii<MM; ii++) {
                    int nans=max(ans, dp[i][M-1][j]+dp[ii][M-1][jj]);
                    if(nans>ans) {
                        cerr<<"nans="<<nans<<endl;
                        ans=nans;
                    }
                }
            }
            if(jj==0)
                break;
        }
    }
    cout<<ans<<endl;

}

signed main() {
    solve();
}
