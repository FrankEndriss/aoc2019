/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 * For second part we need to do some math.
 *
 * It is so that we can change the values in the
 * path from humn: to root:, by changing the 
 * value of humn:
 * So we can traverse the tree starting at root:.
 * Note that one vertex is in the path to humn:,
 * the other is not. We want the first one to
 * become the value of the second.
 * Let this be the "expected" value.
 *
 * Then iterate to the child vertex with the expected
 * value.
 * With this expected value, we can again calculate
 * the expected value of the next vertex on the path
 * to humn:... and so on.
 *
 * At some point we are at the humn: vertex, and then
 * can simply output the "expected" value of that 
 * vertex, which is the answer for second star.
 */
const string HUMN="humn";
void solve() {
    string gs=gcin();
    subst(gs, ':', ' ');

    vs ss=split(gs, "\n");
    map<string, pair<string,string>> adj;
    map<string, string> p;  /* parent */
    map<string, int> val;
    map<string, string> op;

    for(string s : ss) {
        vs tok=splitws(s);
        if(tok.size()==2) { /* a leaf */
            val[tok[0]]=stol(tok[1]);
        } else if(tok.size()==4) {
            adj[tok[0]]= { tok[1], tok[3]} ;
            assert(p.find(tok[1])==p.end());
            p[tok[1]]=tok[0];
            assert(p.find(tok[3])==p.end());
            p[tok[3]]=tok[0];
            op[tok[0]]=tok[2];
        } else
            assert(false);
    }


    function<void(string)> dfs=[&](string v) {
        auto it=val.find(v);
        if(it!=val.end())
            return;

        auto itL=val.find(adj[v].first);
        if(itL==val.end())
            dfs(adj[v].first);
        itL=val.find(adj[v].first);

        auto itR=val.find(adj[v].second);
        if(itR==val.end())
            dfs(adj[v].second);
        itR=val.find(adj[v].second);

        if(op[v]=="+")
            val[v]=(itL->second)+(itR->second);
        else if(op[v]=="-")
            val[v]=(itL->second)-(itR->second);
        else if(op[v]=="*")
            val[v]=(itL->second)*(itR->second);
        else if(op[v]=="/")
            val[v]=(itL->second)/(itR->second);
        else
            assert(false);
    };
    dfs("root");    /* now val[] is filled for all monkeys */

    vs pathH;
    {
        string v=HUMN;
        do {
            pathH.push_back(v);
            v=p[v];
        } while(pathH.back()!="root");
        reverse(all(pathH));
    }
    cerr<<"pathH= ";
    for(string s : pathH)
        cerr<<s<<" ";
    cerr<<endl;


    map<string,int> mexp;

    /* iterate tree and carry expected value */
    function<void(int)> dfs2=[&](int d) {
        const string v=pathH[d];
        auto it=mexp.find(v);
        const int exp=mexp[v];
        const int actual=val[v];
        const int diff=exp-actual;

        cerr<<"dfs2, v="<<v<<" exp="<<exp<<" act="<<actual<<" op="<<op[v]<<endl;
        assert(it!=mexp.end());

        if(v==HUMN) {
            cout<<"star2: "<<exp<<endl;
            exit(0);
        }

        d++;

        if(op[v]=="+") {
            if(pathH[d]==adj[v].first)
                mexp[adj[v].first]=val[adj[v].first]+diff;
            else if(pathH[d]==adj[v].second)
                mexp[adj[v].second]=val[adj[v].second]+diff;
            else
                assert(false);
        } else if(op[v]=="-") {
            if(pathH[d]==adj[v].first)
                mexp[adj[v].first]=val[adj[v].first]+diff;
            else if(pathH[d]==adj[v].second)
                mexp[adj[v].second]=val[adj[v].second]-diff;
            else
                assert(false);
        } else  if(op[v]=="*") {
            /* consider actual to be multiplication result,
             * so we must divide the diff by the other value */
            if(pathH[d]==adj[v].first) {
                assert(exp%val[adj[v].second] == 0);
                mexp[adj[v].first]=exp/val[adj[v].second];
            } else if(pathH[d]==adj[v].second) {
                assert(exp%val[adj[v].first] == 0);
                mexp[adj[v].second]=exp/val[adj[v].first];
            } else
                assert(false);
        } else  if(op[v]=="/") {
            if(pathH[d]==adj[v].first)
                mexp[adj[v].first]=exp*val[adj[v].second];
            else if(pathH[d]==adj[v].second)
                mexp[adj[v].second]=val[adj[v].first]/exp;
            else
                assert(false);
        }

        dfs2(d);

    };

    /* go left or right in root, and expect the other value */
    cerr<<"setting mexp of "<<pathH[1]<<endl;
    if(pathH[1]==adj["root"].first) {
        mexp[pathH[1]]=val[adj["root"].second];
    } else if(pathH[1]==adj["root"].second)
        mexp[pathH[1]]=val[adj["root"].first];
    else
        assert(false);

    dfs2(1);

    assert(false);
    //cout<<"star2="<<val["root"]<<endl;
}

signed main() {
    solve();
}
