/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string,
 * substitute lineends by "\n" if they not allready are.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+='\n';
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* vector<string> to vector<int> */
vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

void subst(string &s, char c, string su) {
    string ans;
    for(char cc : s)
        if(cc==c)
            ans+=su;
        else
            ans+=cc;

    s=ans;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string &s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}

/* split each input string and flatten result */
vs split(vs &ss, string sep) {
    vs ans;
    for(size_t i=0; i<ss.size(); i++)
        for(string aux : split(ss[i], sep))
            ans.push_back(aux);
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}

/**
 */
const int INF=1e9;
void solve() {
    string gs=gcin();
    vs s=split(gs, "\n");

    const int n=s.size();
    const int m=s[0].size();

    cerr<<s.size()<<endl;
    cerr<<s[0].size()<<endl;

    int sz=(n*m)/gcd(n,m);
    cerr<<"sz="<<sz<<" "<<sz*n*m<<endl;

    set<pii> pos;
    for(size_t i=0; i<s[0].size(); i++)
        if(s[0][i]=='.') {
            pos.emplace(0,i);
            break;
        }

    pii end= { s.size()-1, 0};
    for(size_t i=0; i<s.back().size(); i++)
        if(s.back()[i]=='.') {
            end.second=i;
            cerr<<"end at: "<<end.first<<" "<<end.second<<endl;
        }


    vi di= { 1, -1, 0, 0 };
    vi dj= { 0, 0, -1, 1 };
    const int Mi=(n-2)*1000;
    const int Mj=(m-2)*1000;
    int r=1;

    cerr<<"r="<<r<<" size="<<pos.size()<<" "<<pos.count(end)<<endl;
    while(pos.count(end)==0 && pos.size()>0) {
        cerr<<"r="<<r<<" size="<<pos.size()<<endl;
        set<pii> pos0;

        for(auto [i,j] : pos) {  /* iterate all positions */

            vector<pii> next;    /* possible next positions */
            next.emplace_back(i,j);

            for(int k=0; k<4; k++) {
                int ii=i+di[k];
                int jj=j+dj[k];

                if(ii>=0 && ii<n && jj>=0 && jj<m && s[ii][jj]!='#') {
                    //cerr<<"ii="<<ii<<" jj="<<jj<<endl;
                    next.emplace_back(ii,jj);
                }
            }

            for(auto [ii,jj] : next) {

                /* Right wiz */
                int jjj=(jj-1-r+Mj)%(m-2)+1;
                //cerr<<"next ii="<<ii<<" jjj="<<jjj<<endl;
                if(ii>0 && s[ii][jjj]=='>')
                    continue;

                /* Left wiz */
                jjj=(jj-1+r)%(m-2)+1;
                //cerr<<"next ii="<<ii<<" jjj="<<jjj<<endl;
                if(ii>0 && s[ii][jjj]=='<')
                    continue;

                /* Down wiz */
                int iii=(ii-1-r+Mi)%(n-2)+1;
                //cerr<<"next iii="<<iii<<" jj="<<jj<<endl;
                if(ii>0 && s[iii][jj]=='v')
                    continue;

                /* Up wiz */
                iii=(ii-1+r)%(n-2)+1;
                //cerr<<"next iii="<<iii<<" jj="<<jj<<endl;
                if(ii>0 && s[iii][jj]=='^')
                    continue;

                pos0.emplace(ii,jj);
            }
        }
        pos=pos0;
        r++;
    }

    cerr<<"r="<<r<<" size="<<pos.size()<<" "<<pos.count(end)<<endl;
    assert(pos.size()>0);

    cout<<"star1: "<<r-1<<endl;
}



signed main() {
    solve();
}
