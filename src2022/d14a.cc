/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 * Implement brute force
 */
const int N=1000;
void solve() {
    string gs=gcin();

    subst(gs, ',', ' ');
    subst(gs, '-', ' ');
    subst(gs, '>', ' ');

    vs a(N, string(N, '.'));

    const int M=500;

    for(string line : split(gs, "\n")) {
        vi data=splitwsi(line);
        cerr<<"line="<<line<<" line.cnt="<<data.size()<<endl;
        int x=data[0];
        int y=data[1];
        for(size_t i=2; i<data.size(); i+=2) {
            cerr<<"x="<<x<<" y="<<y<<" data[i]="<<data[i]<<" data[i+1]="<<data[i+1]<<endl;
            for(int ii=min(data[i],x); ii<=max(data[i],x); ii++) {
                for(int jj=min(data[i+1],y); jj<=max(data[i+1],y); jj++) {
                    a[ii][jj]='#';
                }
            }
            x=data[i];
            y=data[i+1];
        }
    }


    int ans=0;
    while(true) {
        ans++;
        cerr<<"ans="<<ans<<endl;
        int posX=M;
        int posY=0;

        a[posX][posY]='o';
        while(posY<999) {
            if(a[posX][posY+1]=='.') {
                a[posX][posY]='.';
                a[posX][posY+1]='o';
                posY++;
            } else if(a[posX-1][posY+1]=='.') {
                a[posX][posY]='.';
                a[posX-1][posY+1]='o';
                posY++;
                posX--;
            } else if(a[posX+1][posY+1]=='.') {
                a[posX][posY]='.';
                a[posX+1][posY+1]='o';
                posY++;
                posX++;
            } else
                break;
        }

        if(posY>=999)
            break;
    }

    for(int i=480; i<520; i++) {
        for(int j=0; j<20; j++)  {
            cout<<a[i][j];
        }
        cout<<endl;
    }
    cout<<"star1="<<ans-1<<endl;
}

signed main() {
    solve();
}
