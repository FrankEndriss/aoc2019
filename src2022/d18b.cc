/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vvvb= vector<vvb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


int touch(vi p1, vi p2) {
    vi p= { 0, 1, 2};
    do {
        if(p1[p[0]]==p2[p[0]] && p1[p[1]]==p2[p[1]] &&
                abs( p1[p[2]]-p2[p[2]])==1) {
            return 1;
        }
    } while(next_permutation(all(p)));
    return 0;
}

/**
 *
 * star2:
 * iterate a surrounding box.
 * Start with one corner that is not covered by any quad,
 * count all reacheable not covered quads.
 * Count how many sides of them touch an droplet,
 * thats ans.
 */
const int INF=1e9;
void solve() {
    string gs=gcin();

    subst(gs, ',', ' ');

    vs s=splitws(gs);
    vvi a(s.size()/3);
    vi ma(3, -INF);
    for(size_t i=0; i<s.size(); i+=3) {
        for(int j=0; j<3; j++)  {
            a[i/3].push_back(stol(s[i+j])+1);
            ma[j]=max(ma[j], a[i/3][j]+1);
        }

        //a[i/3].push_back(i/3);
    }

    sort(all(a));
    cerr<<"a.size()="<<a.size()<<endl;

    function<bool(vi)> isDrop=[&](vi p) {
        /*
        p.push_back(0);
        for(int k=0; k<a.size(); k++)  {
            p[3]=a[k][3];
            if(p==a[k])
                return true;
        }
        return false;
        */
        assert(p.size()==3);
        auto it=lower_bound(all(a), p);
        return it!=a.end() && (*it)==p;
    };

    using t3=tuple<int,int,int>;

    vvvb vis(ma[0]+1, vvb(ma[1]+1, vb(ma[2]+1)));

    queue<t3> q;
    q.emplace(0, 0, 0);
    vis[0][0][0]=true;

    int ans=0;
    while(q.size()) {
        auto [x,y,z]=q.front();
        q.pop();

        for(int dx=-1; dx<=1; dx++) {
            for(int dy=-1; dy<=1; dy++) {
                for(int dz=-1; dz<=1; dz++) {

                    if(abs(dx)+abs(dy)+abs(dz)!=1) 
                        continue;

                    int xx=x+dx;
                    int yy=y+dy;
                    int zz=z+dz;

                    if(xx>=0 && xx<=ma[0] && yy>=0 && yy<=ma[1] && zz>=0 && zz<=ma[2]) {
                        if(vis[xx][yy][zz])
                            continue;

                        vi drop= { xx, yy, zz };

                        if(isDrop(drop)) {
                            ans++;
                            continue;
                        }

                        q.emplace(xx, yy, zz);
                        vis[xx][yy][zz]=true;
                    }
                }
            }
        }
    }

    cout<<"star2: "<<ans<<endl;

}

signed main() {
    solve();
}
