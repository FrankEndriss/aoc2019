/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 */
void solve() {
    string gs=gcin();
    vi as=splitwsi(gs);

    /*
    cerr<<"as=";
    for(size_t i=0; i<as.size(); i++)
        cerr<<as[i]<<" ";
    cerr<<endl;
    */

    const int n=(int)as.size();
    vector<pii> a(n);

    for(int i=0; i<n; i++) {
        //a[i].first=as[i];
        a[i].first=as[i]*811589153;
        a[i].second=i;
    }

    cerr<<"n="<<n<<endl;

    //const int M=10;
    const int M=10;
    for(int mm=0; mm<M; mm++) {
    for(int i=0; i<n; i++) {
        cerr<<"i="<<i<<endl;

        int pos=-1;
        for(int j=0; j<n; j++) {
            if(a[j].second==i) {
                pos=j;
                break;
            }
        }

        int d=1;
        if(a[pos].first<0)
            d=-1;

        int opos=pos;
        const int steps=abs(a[pos].first);
        int pos2=opos;
        /* old
        for(int j=0; j<steps; j++) {
            int npos=pos2+d;
            if(d==1 && npos==n)
                npos=1;
            else if(d==-1 && (npos==0 || npos==-1))
                npos=n-1;

            pos2=npos;
        }
        */

        // new
        if(a[pos].first>0) {
            if(pos+a[pos].first<n)
                pos=pos+a[pos].first;
            else {
                int v=a[pos].first-(n-pos);
                pos=1+v%(n-1);
            }
        } else if(a[pos].first<0) {
            int v=abs(a[pos].first);
            while(pos>0 && v>0) {
                pos--;
                v--;
            }
            if(v>0 && pos==0) {
                pos=n-2;
                v--;
            }

            pos=n-2-(v%(n-1));
        } else  {
            assert(a[pos].first==0);
            continue;
        }




        cerr<<"opos="<<opos<<" pos="<<pos<<" pos2="<<pos2<<" d="<<d<<" a[opos].first="<<a[opos].first<<endl;
        //assert(pos2==pos);
        if(opos<pos) {
            pii p=a[min(opos,pos)];
            for(int k=min(opos,pos); k<max(opos,pos); k++)
                a[k]=a[k+1];
            a[max(opos,pos)]=p;
        } else if(opos>pos) {
            pii p=a[opos];
            for(int k=opos; k>pos; k--) 
                a[k]=a[k-1];
            a[pos]=p;
        }


        /*
        for(int j=0; j<n; j++)
            cerr<<a[j].first<<" ";
        cerr<<endl;
        */
    }
    }

    int pos=-1;
    for(int j=0; j<n; j++) {
        if(a[j].first==0) {
            pos=j;
            break;
        }
    }
    assert(pos>=0);

    int ans=a[(pos+1000)%n].first
            +a[(pos+2000)%n].first
            +a[(pos+3000)%n].first;

    //assert(ans==8721);
    cerr<<"star2: "<<ans<<endl;
}

signed main() {
    solve();
}
