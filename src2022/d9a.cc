/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
        cerr<<"ans.size="<<ans.size()<<endl;
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 * d9a.cc
 * today we have to...
 * find the visited locations
 * of the tail, of the rope
 *
 * simulate the movement of the head, and
 * from this the movement of the tail
 */
const int N=2e4+7;
void solve() {
    string gs=gcin();

    vvb vis(N, vb(N));

    int xh=1e4;
    int yh=1e4;
    int xt=1e4;
    int yt=1e4;

    vis[xt][yt]=true;

    cerr<<gs<<endl;
    vs s1=splitws(gs);
    cerr<<"s1.size()="<<s1.size();

    for(size_t k=0; k<s1.size(); k+=2) {
        vs ss= { s1[k], s1[k+1] };

        int steps=stol(ss[1]);

        int nx=xh, ny=yh; /* next position of head */

        cerr<<"xh="<<xh<<" yh="<<yh<<endl;

        int dir;
        if(ss[0]=="R") {
            nx=xh+steps;
            dir=1;
        } else if(ss[0]=="L") {
            nx=xh-steps;
            dir=0;
        } else if(ss[0]=="U") {
            ny=yh+steps;
            dir=3;
        } else if(ss[0]=="D") {
            ny=yh-steps;
            dir=2;
        } else
            assert(false);

        vi ddx= { -1, 1, 0, 0 };
        vi ddy= { 0, 0, -1, 1 };

        for(int k0=0; k0<steps; k0++) {
            int xhPrev=xh;
            int yhPrev=yh;

            xh+=ddx[dir];
            yh+=ddy[dir];

            int dx=abs(xh-xt);
            int dy=abs(yh-yt);

            if(dx>1 || dy>1) { /* move the tail after the head only if dist>1 */
                xt=xhPrev;
                yt=yhPrev;
                vis[xt][yt]=true;
            }
        }
        assert(xh==nx);
        assert(yh==ny);
    }

    for(int i=10000; i<10005; i++)  {
        for(int j=10000; j<10006; j++)  {
            if(vis[i][j])
                cerr<<"#";
            else
                cerr<<".";
        }
        cerr<<endl;
    }


    int ans=0;
    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            ans+=vis[i][j];
    cout<<ans<<endl;
}


signed main() {
    solve();
}
