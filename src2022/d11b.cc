/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++) 
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 * day 11 star 1
 *
 * ans= product of two most active monkeys
 *
 * worry level ... somehow
 */
const int N=1000;
void solve() {
    string gs=gcin();
    subst(gs, ',', ' ');

    //cerr<<"gs="<<gs<<endl;

    vs s1=split(gs, "\n\n");

    vvi mitem(N);
    vb mul(N);
    vi op(N); /* operand for operation */
    vi te(N); /* test divisor */
    vi ttrue(N);   /* next if true */
    vi tfalse(N);   /* next if false */

    int i=0;
    for(string s : s1) {
        //cerr<<"s="<<s<<endl;
        vs ss=split(s, "\n");

        //cerr<<"monkey "<< i<<endl;
        for(int j=0; j<17; j++)
            ss[1][j]=' ';
        //cerr<<"ss[1]="<<ss[1]<<endl;
        mitem[i]=splitwsi(ss[1]);
        //cerr<<"items.size()="<<mitem[i].size()<<endl;

        vs s0=splitws(ss[2]);
        if(s0.back()=="old")
            op[i]=-1;
        else {
            //cerr<<"s0.back()="<<s0.back()<<endl;
            op[i]=stol(s0.back());
        }

        if(s0[s0.size()-2]=="*")
            mul[i]=true;
        else if(s0[s0.size()-2]=="+")
            mul[i]=false;
        else {
            //cerr<<"assert="<<s0[s0.size()-2]<<endl;
            assert(false);
        }

        s0=splitws(ss[3]);
        te[i]=stol(s0.back());

        s0=splitws(ss[4]);
        ttrue[i]=stol(s0.back());
        s0=splitws(ss[5]);
        tfalse[i]=stol(s0.back());

        //cerr<<"ttrue=" <<ttrue[i]<<" tfalse="<<tfalse[i]<<endl;
        i++;
    }

    const int n=i;
    int mod=1;
    for(i=0; i<n; i++)  {
        mod=lcm(mod, te[i]);
        //cerr<<mod<<endl;
    }

    vi ans(N);
    for(int r=0; r<10000; r++) {
        //cerr<<"Round="<<r<<endl;

        for(int j=0; j<n; j++) {
            //cerr<<"Monkey, items: "<<j<<endl;
            for(int item : mitem[j]) {
                //cerr<<" "<<item;
                ans[j]++;
                /* throw item item */
                int val=item;
                if(mul[j]) {
                    if(op[j]==-1) 
                        val*=val;
                    else
                        val*=op[j];
                } else {
                    if(op[j]==-1)
                        val+=val;
                    else
                        val+=op[j];
                }


                //val/=3;
                val%=mod;
                if(val%te[j]==0) {
                    mitem[ttrue[j]].push_back(val);
                } else
                    mitem[tfalse[j]].push_back(val);
            }
            //cerr<<endl;
            while(mitem[j].size())
                mitem[j].pop_back();
        }
    }

    sort(all(ans), greater<int>());
    cout<<ans[0]*ans[1]<<endl;
}

signed main() {
    solve();
}
