/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}

int dist(int p1x, int p1y, int p2x, int p2y) {
    return abs(p1x-p2x)+abs(p2y-p1y);
}

/**
 */
const int M=4e6;
const int MUL=4e6;
//const int M=20;
const int L=-1e7;
const int R=1e7;

const int INF=1e9;

void solve() {
    string gs=gcin();
    subst(gs, '=', ' ');
    subst(gs, ',', ' ');
    subst(gs, ':', ' ');

    vb ans(R-L+1);

    vector<pii> sen;    /* sesor coordinates */
    vi senD;    /* distances to beacons */

    map<pii,int> cnt;

    for(string s : split(gs, "\n")) {
        cerr<<s<<endl;
        vs ss=splitws(s);

        const int sx=stol(ss[3]);
        const int sy=stol(ss[5]);
        const int bx=stol(ss[11]);
        const int by=stol(ss[13]);
        int d=dist(sx, sy, bx, by);
        cerr<<"d="<<d<<endl;

        sen.emplace_back(sx, sy);
        senD.emplace_back(d);

        /*
        d++;
        cnt[ {sx,sy-d}]++;
        cnt[ {sx,sy+d}]++;
        cnt[ {sx+d,sy}]++;
        cnt[ {sx-d,sy}]++;

        for(int i=1; i<d; i++) {
            cnt[ {sx-i,sy+d-i}]++;
            cnt[ {sx-i,sy-d+i}]++;
            cnt[ {sx+i,sy-d+i}]++;
            cnt[ {sx+i,sy+d-i}]++;
        }

        cnt[ {bx,by}]=-1000;
        */
    }
    //cerr<<"cnt.size()="<<cnt.size()<<endl;
    //return;

    /*
    for(auto ent : cnt) {
        bool done=false;
        if(ent.second>=2 && ent.first.first>=0 && ent.first.first<=M
                && ent.first.second<=M && ent.first.second>=0) {
            for(size_t j=0; !done && j<sen.size(); j++) {
                if(dist(ent.first.first, ent.first.second, sen[j].first, sen[j].second)<=senD[j]) {
                    done=true;
                    break;
                }
            }
            if(!done) {
                cout<<"coordinates: "<<ent.first.first<<" "<<ent.first.second<<endl;
                int a1=ent.first.first*MUL+ent.first.second;
                cout<<"star2 : "<<a1<<endl;
                return;
                break;
            }
        }
    }
    */



    for(int i=0; i<=M; i++) {
        if(i%(1024*1024)==0) {
            cerr<<"i="<<i<<endl;
        }

        vector<pii> seg;    /* covered segements */
        /* find covered segment in row i for all sensors */
        for(size_t j=0; j<sen.size(); j++) {
            int d1=senD[j]-abs(sen[j].second-i); /* dist on y axis */
            if(d1<0)
                continue;   /* no positions in current row covered */

            /* segment both ends including */
            seg.emplace_back(sen[j].first-d1, sen[j].first+d1);
        }

        seg.emplace_back(-2, -1);
        seg.emplace_back(M+1, M+2);

        sort(all(seg));     /* sort by left end */

        //cerr<<"Zeile "<<i<<endl;
        int ma=seg[0].second;
        for(size_t j=0; j<seg.size(); j++) {
            //cerr<<"seg[j].first="<<seg[j].first<<" ma="<<ma<<endl;
            if(seg[j].first>ma+1) {
                assert(seg[j].first-2==ma);
                cout<<"x="<<i<<" y="<<seg[j].first-1<<endl;
                int aans=(seg[j].first-1)*MUL+i;
                cout<<"star2="<<aans<<endl;
                return;
            } else {
                ma=max(ma, seg[j].second);
            }
        }

    }
    assert(false);



}

signed main() {
    solve();
}
