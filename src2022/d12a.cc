/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 */
const int INF=1e9;
void solve() {
    string gs=gcin();

    vs s=splitws(gs);
    s[0][0]='a';

    const int n=s.size();
    const int m=s[0].size();

    pii start;
    pii end;
    for(int i=0; i<n; i++) {
        for(int j=0; j<m; j++)  {
            if(s[i][j]=='E') {
                end.first=i;
                end.second=j;
                s[i][j]='z';
            }
            if(s[i][j]=='S') {
                start.first=i;
                start.second=j;
                s[i][j]='a';
            }
        }
        cerr<<s[i]<<endl;
    }

    cerr<<"end.first="<<end.first<<" end.second="<<end.second<<endl;

    vvi dp(n, vi(m,INF));
    dp[start.first][start.second]=0;

    queue<pii> q;
    q.emplace(start.first,start.second);

    vi dx= { -1, 1, 0, 0};
    vi dy= { 0, 0, -1, 1};

    while(q.size()) {
        auto [i,j]=q.front();
        cerr<<"i="<<i<<" j="<<j<<endl;
        q.pop();

        for(int k=0; k<4; k++) {
            int ii=i+dx[k];
            int jj=j+dy[k];

            if(ii>=0 && ii<n && jj>=0 && jj<m && dp[ii][jj]>dp[i][j]+1) {
                if(s[i][j]+1>=s[ii][jj]) {
                    dp[ii][jj]=dp[i][j]+1;
                    cerr<<"push "<<ii<<" "<<jj<<endl;
                    q.emplace(ii,jj);

                    if(end.first==ii && end.second==jj) {
                        cout<<dp[ii][jj]<<endl;
                        return;
                    }
                }
            }
        }
    }
    assert(false);

}

signed main() {
    solve();
}
