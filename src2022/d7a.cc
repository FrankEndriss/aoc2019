/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++) 
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 */
void solve() {
    string gs=gcin();
    map<string,bool> isDir;
    map<string,int> sz;
    map<string,string> parent;

    vs lines=split(gs, "\n");
    string curDir;
    for(size_t i=0; i<lines.size(); i++) {
        cout<<lines[i]<<endl;
        vs tok=splitws(lines[i]);

        if(tok[0]=="$" && tok[1]=="cd") {
            if(tok[2]=="/")
                curDir="/";
            else if(tok[2]=="..") {
                size_t idx=0;
                for(size_t k=0; k<curDir.size(); k++) 
                    if(curDir[k]=='/')
                        idx=k;

                curDir=curDir.substr(0,idx);
                cerr<<"after cd .. : "<<curDir<<endl;

            } else {
                if(curDir=="/")
                    curDir+=tok[2];
                else
                    curDir=curDir+"/"+tok[2];
            }

            isDir[curDir]=true;
        } else if(tok[0]=="$" && tok[1]=="ls") {

            for(size_t j=i+1; j<lines.size() && lines[j][0]!='$'; j++ ) {
                cerr<<"found an entry: "<<lines[j]<<endl;
                vs tok2=splitws(lines[j]);
                if(tok2[0]!="dir") { // found a file
                    string fname;
                    if(curDir=="/") 
                        fname=curDir+tok2[1];
                    else
                        fname=curDir+"/"+tok2[1];
                    parent[fname]=curDir;
                    isDir[fname]=false;
                    sz[fname]=stol(tok2[0]);
                } else {
                    string fname;
                    if(curDir=="/") 
                        fname=curDir+tok2[1];
                    else
                        fname=curDir+"/"+tok2[1];
                    isDir[fname]=true;
                    parent[fname]=curDir;
                }
            }

        }
    }

    map<string,int> ans;
    for(auto ent : sz) {
        string chl=ent.first;
        string p;
        do {
            p=parent[chl];
            ans[p]+=ent.second;
            cerr<<"summing f="<<ent.first<<" "<<ent.second<<" p="<<p<<endl;
            chl=p;
        }while(p.size()>0);
    }

    int a=0;
    for(auto ent2 : ans) {
        cerr<<"sz "<<ent2.first<<" ="<<ent2.second<<endl;
        if(ent2.second<=100000)
            a+=ent2.second;
    }

    cout<<a<<endl;
}

signed main() {
    solve();
}
