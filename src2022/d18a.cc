/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++) 
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 *
 * Count touching sides, and subtract
 * from N*6
 */
void solve() {
    string gs=gcin();

    subst(gs, ',', ' ');

    vs s=splitws(gs);
    vvi a(s.size()/3);
    for(size_t i=0; i<s.size(); i+=3) {
        for(int j=0; j<3; j++) 
            a[i/3].push_back(stol(s[i+j]));
        cerr<<"a[i][]: "<<a[i/3][0]<<" "<<a[i/3][1]<<" "<<a[i/3][2]<<endl;

        a[i/3].push_back(i/3);
    }


    const int n=a.size();
    cerr<<"n="<<n<<endl;
    int ans=0;
    vi p={ 0, 1, 2};
    do {
        cerr<<"p="<<p[0]<<" "<<p[1]<<" "<<p[2]<<endl;
        vvi aa=a;
        for(int i=0; i<n; i++)
            for(int j=0; j<3; j++) 
                aa[i][p[j]]=a[i][j];

        sort(all(aa));

        for(size_t i=0; i+1<n; i++) {
            if(aa[i][0]==aa[i+1][0] && aa[i][1]==aa[i+1][1] &&
                    aa[i][2]+1==aa[i+1][2]) {
                ans++;
                cerr<<"contact, id="<<aa[i][3]<<" "<<aa[i+1][3]<<endl;
            }


        }

    }while(next_permutation(all(p)));

    cout<<"star1: "<<n*6-ans<<endl;


}

signed main() {
    solve();
}
