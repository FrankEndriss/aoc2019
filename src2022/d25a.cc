/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string, 
 * substitute lineends by "\n" if they not allready are.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first) 
            ans+='\n';
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* vector<string> to vector<int> */
vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++) 
        ans[i]=stol(s[i]);
    return ans;
}

string subst(string s, char c, string su) {
    string ans;
    for(char cc : s)
        if(cc==c)
            ans+=su;
        else
            ans+=cc;

    return ans;
}

/* substitute c1 by c2 */
string subst(string s, char c1, char c2) {
    string s2;
    s2.push_back(c2);
    return subst(s, c1, s2);
}


/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size()) 
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}

/* split each input string and flatten result */
vs split(vs ss, string sep) {
    vs ans;
    for(size_t i=0; i<ss.size(); i++) 
        for(string aux : split(ss[i], sep))
            ans.push_back(aux);
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}

/**
 */
void solve() {

    int sum=0;
    for(string s : split(gcin(), "\n")) {

        int aa=0;
        for(size_t i=0; i<s.size(); i++) {

            aa*=5;
            if(s[i]=='=') {  // 0
                aa-=2;
            } else if(s[i]=='-') {
                aa--;
            } else if(s[i]=='0') {
            } else if(s[i]=='1') {
                aa++;
            } else if(s[i]=='2') {
                aa+=2;
            }else
                assert(false);
        }
        sum+=aa;
    }


    string ans;
    while(sum) {
        int dig=sum%5;
        sum/=5;

        if(dig<3) 
            ans+='0'+dig;
        else {
            if(dig==3) {
                ans+='=';
            } else if(dig==4) {
                ans+='-';
            }
            sum++;
        }
    }
    reverse(all(ans));

    cout<<"star1: "<<ans<<endl;

}

signed main() {
    solve();
}
