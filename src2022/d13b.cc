/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 */
const int N=1000;
void solve() {
    string gs=gcin();

    function<vvi(string)> parseLine=[&](string s) {

        string ss;
        for(size_t i=0; i<s.size(); i++) {
            if(s[i]=='[')
                ss+="[ ";
            else if(s[i]==']')
                ss+=" ] ";
            else
                ss.push_back(s[i]);
        }

        ss[0]=' ';
        cerr<<"ss="<<ss<<endl;
        vs data=splitws(ss);

        stack<int> st;
        st.push(0);
        vvi a(1);
        int idx=0;
        for(size_t i=0; i<data.size(); i++) {
            if(data[i]=="[") { 
                idx=a.size();
                vi aa;
                a.push_back(aa);

                a[st.top()].push_back(idx);
                st.push(idx);
            } else if(data[i]=="]") {
                st.pop();
            } else {
                int ii=stol(data[i]);
                a[st.top()].push_back(-ii-1);
            }
        }

        return a;
    };


    /*  compare list a1[i1] to list a1[i2]
     *  -1 if left is smaller, 0 for eq, 1 for right is smaller
     */
    function<int(vvi&,int,vvi&,int)> cmp=[&](vvi &a1, int i1, vvi &a2, int i2) {
        cerr<<"cmp i1="<<i1<<" i2="<<i2<<endl;
        assert(i1<a1.size());
        assert(i2<a2.size());
        for(size_t i=0; i<max(a1[i1].size(), a2[i2].size()); i++) {
            if(a1[i1].size()==i) {
                cerr<<"case 1 a1.size() smaller"<<endl;
                return -1LL;
            } else if(a2[i2].size()==i) {
                cerr<<"case 2 a2.size() smaller"<<endl;
                return 1LL;
            } else if(a1[i1][i]<0 && a2[i2][i]<0)  {
                cerr<<"case 3 both int"<<endl;
                if(a1[i1][i]>a2[i2][i])  {
                    return -1LL;
                } else if(a1[i1][i]<a2[i2][i])  {
                    return 1LL;
                }
                //else continue
            } else if(a1[i1][i]>=0 && a2[i2][i]>=0) {
                cerr<<"case 4"<<endl;
                int v=cmp(a1, a1[i1][i], a2, a2[i2][i]);
                if(v!=0)
                    return v;
                // else continue
            } else if(a1[i1][i]<0 && a2[i2][i]>=0) {
                cerr<<"case 5"<<endl;
                int ii=a1.size();
                vi v;
                a1.push_back(v);
                a1.back().push_back(a1[i1][i]);
                int vv= cmp(a1, ii, a2, a2[i2][i]);
                if(vv!=0)
                    return vv;
                // else continue
            } else if(a1[i1][i]>=0 && a2[i2][i]<0) {
                cerr<<"case 6"<<endl;
                int ii=a2.size();
                vi v;
                a2.push_back(v);
                a2.back().push_back(a2[i2][i]);
                int vv= cmp(a1, a1[i1][i], a2, ii);
                if(vv!=0)
                    return vv;
                // else continue
            } else
                assert(false);
        }

        return 0LL;
    };

    subst(gs, ',', ' ');

    int ans=0;
    int cnt=1;
    vvvi data;
    for(string s : split(gs, "\n\n")) {

        vs ss=split(s, "\n");
        assert(ss[0][0]=='[');
        assert(ss[1][0]=='[');

        vvi a1=parseLine(ss[0]);
        data.push_back(a1);

        /*
        for(size_t j=0; j<a1.size(); j++) {
            for(int v : a1[j])
                cerr<<v<<" ";
            cerr<<endl;
        }
        */
        vvi a2=parseLine(ss[1]);
        data.push_back(a2);


        int v=cmp(a1, 0, a2, 0);
        cerr<<"XXX cnt="<<cnt<<" v="<<v<<endl; 
        if(v<=0) {
            ans+=cnt;
        }

        cnt++;
//        if(cnt>3)
//            break;
    }

    cout<<"star1="<<ans<<endl;

    vvi extra1={ 
        { 1 },
        { -3 }
    };
    vvi extra2={
        { 1 },
        { -7 }
    };

    data.push_back(extra1);
    data.push_back(extra2);

    sort(all(data), [&](vvi a1, vvi a2) {
            return cmp(a1,0,a2,0)<0;
    });

    int ans1=-1; 
    int ans2=-1;
    for(size_t i=0; i<data.size(); i++) {
        if(data[i]==extra1)
            ans1=i+1;
        if(data[i]==extra2)
            ans2=i+1;
    }

    cout<<"star2="<<ans1*ans2<<endl;
}

signed main() {
    solve();
}
