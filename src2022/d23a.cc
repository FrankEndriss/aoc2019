/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string,
 * substitute lineends by "\n" if they not allready are.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+='\n';
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

/* vector<string> to vector<int> */
vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

void subst(string &s, char c, string su) {
    string ans;
    for(char cc : s)
        if(cc==c)
            ans+=su;
        else
            ans+=cc;

    s=ans;
}

/* split by whitespace */
vs splitws(string &s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string &s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}

/* split each input string and flatten result */
vs split(vs &ss, string sep) {
    vs ans;
    for(size_t i=0; i<ss.size(); i++)
        for(string aux : split(ss[i], sep))
            ans.push_back(aux);
    return ans;
}

/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}

/**
 */
const int N=100;
const int OFF=15;
const int INF=1e9;
void solve() {
    string gs=gcin();

    vs s1=split(gs, "\n");

    cerr<<"input: "<<endl;
    vs a(N, string(N, '.'));
    for(size_t i=0; i<s1.size(); i++)  {
        for(size_t j=0; j<s1[i].size(); j++)
            a[i+OFF][j+OFF]=s1[i][j];

        cerr<<s1[i]<<endl;
    }
    cerr<<"fini input: "<<endl;

    /*       NW   N  NE   E  SE S  SW   W */
    vi di8= { -1, -1, -1,  0, 1, 1,  1,  0};
    vi dj8= { -1,  0,  1,  1, 1, 0, -1, -1};

    /* N E S W */
    vi di4= { -1, 0, 1, 0 };
    vi dj4= { 0, 1, 0, -1 };

    /* any elf on 8 positions arraound */
    function<bool(int,int)> any8=[&](int i, int j) {
        for(int k=0; k<8; k++)
            if(a[i+di8[k]][j+dj8[k]]=='#')
                return true;
        return false;
    };

    /* any elf on 3 positions starting at k */
    function<bool(int,int,int)> any3=[&](int i, int j, int k) {
        cerr<<"any3, i="<<i<<" j="<<j<<" k="<<k<<endl;
        bool ans=false;
        for(int kk=0; kk<3; kk++)
            if(a[i+di8[(k+kk)%8]][j+dj8[(k+kk)%8]]=='#')
                ans=true;
        cerr<<"ans="<<ans<<endl;
        if(ans)
            return ans;

        return false;
    };

    vi dircheck={ 0, 4, 6, 2 };
    const int M=10;
    for(int k=0; k<M; k++) {
        cerr<<"k="<<k<<endl;

        vvi dir(a.size(), vi(a[0].size(), -1));
        vvi cnt(a.size(), vi(a[0].size(), 0));

        int cnte=0;
        for(int i=1; i+1<a.size(); i++) {
            // DEBUG 
            //if(cnte>3)
            //    break;

            for(int j=1; j+1<a[0].size(); j++) {    /* all positions */
                if(a[i][j]!='#')
                    continue;

                if(!any8(i,j))
                    continue;   /* do not move the elf */

                cerr<<"do check "<<i<<" "<<j<<endl;

                cnte++;
                //cerr<<"some arround, "<<i<<" "<<j<<endl;

                /* check the four directions */
                int go=-1;
                for(int kk=0; go<0 && kk<4; kk++) {
                    if(!any3(i,j,dircheck[kk]))
                        go=dircheck[kk]/2;
                }

                dir[i][j]=go;
                if(go>=0) {
                    cerr<<" want move i="<<i<<" j="<<j<<" go="<<go<<endl;
                    cnt[i+di4[go]][j+dj4[go]]++;
                }
            }
        }
        cerr<<"cnte="<<cnte<<endl;

        /* now move the elves */
        vs aa=a;
        for(int i=1; i<a.size(); i++) {
            for(int j=1; j+1<a[0].size(); j++) {
                if(a[i][j]!='#' || dir[i][j]<0)
                    continue;   

                int ii=i+di4[dir[i][j]];
                int jj=j+dj4[dir[i][j]];
                if(cnt[ii][jj]==1) {
                    aa[ii][jj]='#';
                    aa[i][j]='.';
                }
            }
        }
        a=aa;

        /* rotate checked directions */
        int aux=dircheck[0];
        for(int k=0; k<3; k++) 
            dircheck[k]=dircheck[k+1];
        dircheck[3]=aux;

    }

    /* collect numbers */
    int miI=INF;
    int maI=0;
    int miJ=INF;
    int maJ=0;
    for(int i=0; i<a.size(); i++) 
        for(int j=0; j<a[0].size(); j++) {
            if(a[i][j]=='#') {
                miI=min(miI, i);
                maI=max(maI, i);
                miJ=min(miJ, j);
                maJ=max(maJ, j);
            }
        }

    int ans=0;
    for(int i=0; i<a.size(); i++) {
        for(int j=0; j<a[0].size(); j++) {
            if(a[i][j]=='.' && i>=miI && i<=maI && j>=miJ && j<=maJ)
                ans++;
        }
    }

    /* DEBUG output */
    for(size_t i=0; i<a.size(); i++)
        cerr<<a[i]<<endl;

    cout<<"star1: "<<ans<<endl;
}

signed main() {
    solve();
}
