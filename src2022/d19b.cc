/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vvvi= vector<vvi>;
using vvvvi= vector<vvvi>;
using vvvvvi= vector<vvvvi>;
using vs= vector<string>;

#define endl "\n"

const int INF=1e9;
/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


int toid(string s) {
    if(s=="ore")
        return 0;
    else if(s=="clay")
        return 1;
    else if(s=="obsidian")
        return 2;
    else if(s=="geode")
        return 3;
    else
        assert(false);
}

/**
 * robots have type
 * 1 minute create robot
 *
 * blueprints, choose one
 *
 * each robot creates one of its type
 *
 * state=
 * vi rob[4]: number of created robots so far
 * vi res[4]: number of collected resources so far
 *
 * Foreach resource it is allways optimal to first create as much robots
 * as possible
 *
 * We need to find the order in that we build the robots for each blueprint.
 * vs builds[23];
 *
 * ***************
 * think about DP
 * ...and only first x types
 * Minimum minute we can create number of robots rob[x] and have res[x]:
 * dp[rob[0]][rob[1]][res[0]][res[1]]
 *
 * From that, greedy buy as much as possible from the next type.
 *
 * ...start at only one type
 * ****************
 *
 * So, long story short, it is like heuristics contest:
 * Just add some optimizations will make the brute force work in time.
 *
 * Opt1:
 * Cosider how much of every type can be used by the 
 * factory in the remaining time. If we got more than that,
 * do not produce more of that robot.
 *
 * Opt2:
 * Consider the not buy anything branch: It does never makes
 * sense to not buy a robot if we can, and then buy it in the
 * next move. Because it would have been better to buy it on
 * minute earlier.
 * So remove these branches.
 *
 * Opt3:
 * Foreach robot type there is a max cost for any other robot.
 * It makes never sense to create more robots of the type than
 * these maximum costs.
 *
 * Opt4:
 * If we can build a geode robot, we allways do.
 * Actually not sure how to prove that it allways works, maybe
 * caused by weak test data. But gives a boost of factor 100 
 * in second star.
 *
 *
 * Also consider memoization. There are actually to much states, but
 * if doing BFS it seems after 1e6 entries in memo set the ans
 * does not grow any more.
 */
const int M=32;
void solve() {
    string gs=gcin();
    gs[0]=' ';

    subst(gs, '.', '\n');
    subst(gs, ':', '\n');
    while(true) {
        size_t pos=gs.find("Blue");
        if(pos==string::npos || pos>=gs.size())
            break;

        gs[pos]='\n';
    }
    cerr<<"after Blue"<<endl;

    vs s2=split(gs, "\n\n");
    const int n=s2.size();

    vvvi icost(n, vvi(4, vi(4, 0)));
    for(int i=0; i<n; i++) { /* blueprints */
        vs lines=split(s2[i], "\n");
        cerr<<"Blue "<<i<<endl;
        for(size_t j=1; j<lines.size(); j++) {
            //cerr<<"line="<<lines[j]<<endl;
            vs words=splitws(lines[j]);
            string type=words[1];
            cerr<<"type="<<type<<endl;
            for(size_t k=2; k<words.size(); k++) {
                if(words[k][0]>='0' && words[k][0]<='9') { /* a number */
                    cerr<<"*** "<<words[k+1]<<" "<<stol(words[k])<<endl;
                    icost[i][toid(type)][toid(words[k+1])]=stol(words[k]);
                }
            }
        }
    }

    const int GEODE=toid("geode");

    using t4=tuple<vi,vi,int,vb>;

    int gans=1;
    for(int bluei=0; bluei<n && bluei<3; bluei++)  {
        int ans=0;
        vi maxc(4); /* max costs per type */
        for(int i=0; i<4; i++)
            for(int j=0; j<4; j++)
                maxc[j]=max(maxc[j], icost[bluei][i][j]);

        maxc[GEODE]=1000;
        cerr<<"maxc= "<<maxc[0]<<" "<<maxc[1]<<" "<<maxc[2]<<" "<<maxc[3]<<endl;

        /* check if we have enough res to buy a given robot */
        function<bool(vi,vi)> canBuy=[](vi c, vi b) {
            assert(c.size()==(size_t)4);
            assert(b.size()==(size_t)4);
            for(int i=0; i<4; i++) {
                if(c[i]>b[i])
                    return false;
            }
            return true;
        };

        /* state=
         * v[0..3] number of robots.
         * v[4..7] number of resources
         *
         * notbuy is optimization 2
         */
        //set<vi> memo;
        int rcnt=0;
        function<void(vi,vi,int,vb)> go=[&](vi r, vi s, int t, vb notbuy) {
            /* 
             * dont use memo, its to mem consuming 
            const vi key={ 
                r[0], r[1], r[2], r[3], 
                s[0], s[1], s[2], s[3],
                t, notbuy[0], notbuy[1], notbuy[2], notbuy[3]};

            if(memo.count(key))
                return;
            memo.emplace(key);
            */

            ans=max(ans, s[GEODE]);
            if(t==M)
                return;

            rcnt++;
            if(rcnt%(1024*1024)==0)
                cerr<<"rcnt="<<rcnt<<endl;


            vb buyable(4);
            for(int k=0; k<4; k++)
                buyable[k]=canBuy(icost[bluei][k], s);

            for(int i=0; i<4; i++)
                s[i]+=r[i];

            /* buy nothing */
            /* optimization 4:
             *  If we can build a geode robot, we allwazs do
             */
            if(!buyable[3])
                go(r,s,t+1, buyable);

            /* buy what you can */
            for(int k=0; k<4; k++) {
                /* optimization 4, buy geode robot whenever possible */
                if(k<3 && buyable[3])
                    continue;

                /* Consider optimization 3:
                * If we have "a lot" of a resource, even more than
                * we can use in the remaining time, it does not make sense
                * to buy any more of that robot.
                */
                int maxneed=(M-t)*maxc[k];
                if(maxneed<=s[k]) 
                    continue;

                /* optimization 1 */
                if(r[k]<maxc[k] && buyable[k] && !notbuy[k]) {
                    vi rr=r;
                    vi ss=s;
                    rr[k]++;
                    for(int i=0; i<4; i++)  {
                        ss[i]-=icost[bluei][k][i];
                        assert(ss[i]>=0);
                    }
                    vb bb(4);
                    go(rr,ss,t+1,bb);
                }
            }
        };

        vi r= { 1, 0, 0, 0 };
        vi s= { 0, 0, 0, 0 };
        vb notbuy(4);
        go(r,s,0,notbuy);

        gans*=ans;
        cout<<"ans "<<bluei<<"="<<ans<<endl;
    }

    cout<<"star2="<<gans<<endl;
}

signed main() {
    solve();
}
