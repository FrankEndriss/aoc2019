/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;
using vvs= vector<vs>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}

vvs shapes= {
    {"####"},
    {".#.", "###", ".#."},
    {"..#", "..#", "###"},  /* rotated by 180 degree */
    {"#", "#", "#", "#"},
    {"##", "##"}
};
vi shapeW= { 4, 3, 3, 1, 2 };   /* with of shapes */

/**
 * We can super simple brute force the position of every
 * shape, it will run fast enough, optimization not really
 * needed.
 * Then simply 'try' to fit it into the existing solution.
 */
//const int M=10;
const int M=2022;
void solve() {
    string gs=gcin();
    while(gs.back()!='<' && gs.back()!='>')
        gs.pop_back();

    vs ans; /* answer string array, botton to top */

    /* true if movement of shape s to posLine posCol is possible */
    function<bool(int,int,int)> canMoveTo=[&](int s,int posLine,int posCol) {
        cerr<<"canMoveTo, s="<<s<<" posLine="<<posLine<<" posCol="<<posCol<<endl;
        if(posCol<0 || posCol+shapeW[s]>7 || posLine+1-shapes[s].size()<0 || posLine>=ans.size()) {
            cerr<<"cmt 1"<<endl;
            return false;
        }

        for(size_t i=0; i<shapes[s].size(); i++) { /* lines of shape */
            for(size_t j=0; j<shapes[s][i].size(); j++) {
                if(ans[posLine-i][j+posCol]=='#' && shapes[s][i][j]=='#') {
                    cerr<<"cmt 3"<<endl;
                    return false;
                }
            }
        }
        return true;
    };

    string line=".......";
    int ma=-1;
    int jidx=0;
    for(int i=0; i<M; i++) {
        const int sh=i%shapes.size();
        int j=ma+3+shapes[sh].size(); /* row */

        while(ans.size()<=j)
            ans.push_back(line);

        cerr<<"ans.size()="<<ans.size()<<endl;

        int pos=2;  /* left / right */
        while(true) {
            cerr<<"check blow: "<<gs[jidx%gs.size()]<<endl;
            if(gs[jidx%gs.size()]=='<' && canMoveTo(sh, j, pos-1)) {
                cerr<<" *** blows left"<<endl;
                pos--;
            } else if(gs[jidx%gs.size()]=='>' && canMoveTo(sh, j, pos+1)) {
                cerr<<" *** blows right"<<endl;
                pos++;
            } else
                cerr<<"blows not"<<endl;

            jidx++;

            if(!canMoveTo(sh, j-1, pos)) {
                cerr<<"cannot move down "<<endl;
                break;
            }

            j--;
        }
        ma=max(ma,j);
        cerr<<"shape nr "<<i<<" sh="<<sh<<" pos="<<pos<<" ma="<<ma<<endl;

        /* draw shape into ans  */
        for(int k=0; k<shapes[sh].size(); k++) {
            for(size_t kk=0; kk<shapes[sh][k].size(); kk++)
                if(shapes[sh][k][kk]=='#') {
                    assert(ans[j-k][kk+pos]=='.');
                    ans[j-k][kk+pos]=shapes[sh][k][kk];
                }
        }

        /*
        for(int i=ans.size()-1; i>=0; i--) 
            cerr<<ans[i]<<endl;
        cerr<<endl;
        */
    }


    cout<<"star1="<<ma+1<<endl;

}

signed main() {
    solve();
}
