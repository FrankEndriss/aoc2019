/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


void printGrid(vi xp, vi yp) {
    cerr<<"Grid:"<<endl;
    for(int i=0; i<10; i++)  {
        xp[i]-=9980;
        yp[i]-=9980;
    }

    vs ans(40, string(40, '.'));
    ans[20][20]='s';
    ans[xp[0]][yp[0]];
    for(int i=9; i>=0; i--) {
        ans[xp[i]][yp[i]]=('0'+i);
    }

    for(int i=0; i<40; i++) {
        cerr<<ans[i]<<endl;
    }
}

int sign(int i) {
    if(i<0)
        return -1;
    else if(i>0)
        return 1;
    else
        return 0;
}

/**
 * d9a.cc
 * today we have to...
 * find the visited locations
 * of the tail, of the rope
 *
 * simulate the movement of the head, and
 * from this the movement of the tail
 */
const int N=2e4+7;
const int start=1e4;
void solve() {
    string gs=gcin();

    vvb vis(N, vb(N));

    vi xp(10, start);  /* positions, head=0, tail =9 */
    vi yp(10, start);

    vis[xp.back()][yp.back()]=true;

    vs s1=splitws(gs);

    for(size_t k=0; k<s1.size(); k+=2) {
        vs ss= { s1[k], s1[k+1] };

        //cerr<<ss[0]<<" "<<ss[1]<<endl;

        int steps=stol(ss[1]);
        int dir;
        if(ss[0]=="R") {
            dir=2;
        } else if(ss[0]=="L") {
            dir=3;
        } else if(ss[0]=="U") {
            dir=0;
        } else if(ss[0]=="D") {
            dir=1;
        } else
            assert(false);

        vi ddx= { -1, 1, 0, 0 };
        vi ddy= { 0, 0, 1, -1 };

        /* for some reason this movement is not correct,
         * we need to check again the problem statement.
         *
         * ...I am a bit pissed with todays problem
         * statement, again.
         * Actually, after 4 hours I inspected working
         * solutions to understand how the movement of
         * the tail actually works. That part was not
         * undstandable at all from the statement, even
         * not by trying to understand the examples.
         * For some reason part 1 worked nonetheless, but
         * part2 was not solveable.
         *
         * ...After inspecting "a lot" of solutions,
         * it seems that knots are moving at most
         * one position on each axis, and it allways
         * moves in the direction the next knot is placed before,
         * on both axes independent.
         */
        for(int k0=0; k0<steps; k0++) {
            xp[0]+=ddx[dir];
            yp[0]+=ddy[dir];

            for(int i=1; i<10; i++) {
                const int dx=xp[i-1]-xp[i];
                const int dy=yp[i-1]-yp[i];
                assert(dx<=2);
                assert(dy<=2);
                if(abs(dx)>1 || abs(dy)>1) { /* move the tail if dist>1 */
                    xp[i] += sign(dx);
                    yp[i] += sign(dy);
                }
            }
            //cerr<<xp.back()<<","<<yp.back()<<endl;
            vis[xp.back()][yp.back()]=true;

        }
        //printGrid(xp,yp);
    }

    int ans=0;
    for(int i=0; i<N; i++)
        for(int j=0; j<N; j++)
            ans+=vis[i][j];
    cout<<ans<<endl;
}


signed main() {
    solve();
}
