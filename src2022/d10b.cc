/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* read the whole input into a large string.
 **/
string gcin() {
    string ans;
    bool first=true;
    while(true) {
        if(!first)
            ans+="\n";
        first=false;

        string s;
        getline(cin,s);
        if(!cin.good())
            break;

        ans+=s;
    }
    return ans;
}

vi vstovi(vs s) {
    vi ans(s.size());
    for(size_t i=0; i<s.size(); i++)
        ans[i]=stol(s[i]);
    return ans;
}

/* substitute c1 by c2 */
void subst(string &s, char c1, char c2) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]==c1)
            s[i]=c2;
}

/* split by whitespace */
vs splitws(string s) {
    istringstream iss(s);
    vs ans;
    while(true) {
        string ss;
        iss>>ss;
        if(ss.size()==0)
            break;
        else
            ans.push_back(ss);
    }
    return ans;
}

/* split by separator */
vs split(string s, string sep) {
    vs ans;
    size_t pos=0;
    while(true) {
        size_t next=s.find(sep, pos);
        if(next==string::npos) {
            if(pos<s.size())
                ans.push_back(s.substr(pos));
            return ans;
        }
        ans.push_back(s.substr(pos, next-pos));
        pos=next+sep.size();
    }

    assert(false);
}
/* split by whitespace and convert to int */
vi splitwsi(string s) {
    return vstovi(splitws(s));
}
vi spliti(string s, string sep) {
    return vstovi(split(s,sep));
}
vi spliti(string s, char sep) {
    return spliti(s,string(1,sep));
}


/**
 */
void solve() {
    string gs=gcin();

    int c1=1;
    vi ansx(1000);

    int x=1;
    int c=1;
    int ans=0;
    int next=20;
    int inc=40;
    ansx[c]=x;

    for(string s : split(gs, "\n")) {
        //     cerr<<s<<endl;
        vs ss=splitws(s);
        if(ss[0]=="noop")  {
            c++;
            ansx[c]=x;
            if(c>=next) {
                cerr<<"x="<<x<<" next="<<next<<" "<<x*next<<endl;
                ans+=x*next;
                next+=inc;
                cerr<<"ans="<<ans<<endl;
            }
            ;   // do nothing
        } else if(ss[0]=="addx") {
            ansx[c+1]=x;
            if(c+1>=next) {
                cerr<<"x="<<x<<" next="<<next<<" "<<x*next<<endl;
                ans+=x*next;
                next+=inc;
                cerr<<"ans="<<ans<<endl;
            }
            x+=stol(ss[1]);
            c+=2;
            if(c>=next) {
                cerr<<"x="<<x<<" next="<<next<<" "<<x*next<<endl;
                ans+=x*next;
                next+=inc;
                cerr<<"ans="<<ans<<endl;
            }
            ansx[c]=x;
        }

    }

    vs crt(6, string(40, ' '));
    for(int i=1; i<=240; i++) {
        int idx=i-1;
        cerr<<"i="<<i<<" ansx[c]="<<ansx[i]<<endl;
        int ii=idx/40;
        int jj=idx%40;
        if(ansx[i]>=jj-1 && ansx[i]<=jj+1) {
            crt[ii][jj]='#';
        }

    }

    for(int i=0; i<6; i++) 
        cout<<crt[i]<<endl;

}

/* 
 *star2
 * some sprinte of size 3
 * crt w=40 h=6
 * rowwise
 */

signed main() {
    solve();
}
