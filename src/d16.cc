/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename E>
E rsq_add(E e1, E e2) {
    return e1+e2;
}

template<typename E>
E rsq_neg(E e) {
    return -e;
}

template<typename E>
struct RSQ {
    vector<E> p;
    function<E(E,E)> cummul;
    function<E(E)> neg;

    template<typename Iterator>
    RSQ(Iterator beg, Iterator end, function<E(E,E)> pCummul=rsq_add<E>, function<E(E)> pNeg=rsq_neg<E>)
        : cummul(pCummul), neg(pNeg)
    {
        p.push_back(*beg);
        beg++;
        while(beg!=end) {
            p.push_back(pCummul(p.back(), *beg));
            beg++;
        }
    }

    E get(int l, int r) {
        r=min((int)p.size(), r);
        l=max(0, l);
        if(r<=l)
            return cummul(p[0], neg(p[0])); // neutral

        E ans=p[r-1];
        if(l>0)
            ans=cummul(ans, neg(p[l-1]));
        return ans;
    }
};


vi basepattern= { 0, 1, 0, -1 };
int msgOffs;

void transform2(vi &a) {
    RSQ<ll> stree(a.begin(), a.end());

    vi aux(a.size());
    const int asz=(int)a.size();

    for(int i=0; i<asz; i++) {
        int keylen=i+1;
        int offs=keylen-1;
        int keyidx=1;

        ll val=0;
        while(offs<asz) {
            if(basepattern[keyidx]) {
                val+=stree.get(offs, min(asz, offs+keylen))*basepattern[keyidx];
            }
            keyidx=(keyidx+1)%4;
            offs+=keylen;
        }
        aux[i]=(int)abs(val)%10;
    }
    a.swap(aux);
}

void solve2(vi a) {
    msgOffs=0;
    for(int i=0; i<7; i++) {
        msgOffs*=10;
        msgOffs+=a[i];
    }

    vi a2(10000*a.size());
    for(size_t i=0; i<a2.size(); i++) {
        a2[i]=a[i%a.size()];
    }

    for(int i=0; i<100; i++) {
        cout<<"round "<<i<<"/100"<<endl;
        transform2(a2);
    }

    cout<<"star2 ";
    for(int i=0; i<8; i++)
        cout<<a2[(msgOffs+i)%a2.size()];
    cout<<endl;
}

void solve(vi a) {
    for(int i=0; i<100; i++)
        transform2(a);

    cout<<"star1 ";
    for(int i=0; i<8; i++)
        cout<<a[i];
    cout<<endl;

}

/* Not fast, but at least it is simple code. */
int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);

    cins(s);
    vi a(s.size());

    for(size_t i=0; i<s.size(); i++)
        a[i]=s[i]-'0';

    solve(a);
    solve2(a);
}

