/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename T>
void atoli(string &s, T &m) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]!='-' && (s[i]<'0' || s[i]>'9'))
            s[i]=' ';

    istringstream iss(s);
    ll idx=0;
    while(iss.good()) {
        ll aux;
        iss>>aux;
        m[idx]=aux;
        idx++;
    }
}

/* Incode computer, see https://adventofcode.com/2019/day/9
 * This implementation works with mapped memory, ie memory
 * is kind of infinite, and can have negative addresses, too.
 */
struct prg {
    ll pc;
    ll reloffs;
    map<ll,ll> a;   // memory

    queue<ll> iq;   // stdin
    queue<ll> oq;   // stdout

    /* Give memory/program as parameter, an arbitrary list of integers. */
    prg(string s) : pc(0), reloffs(0) {
        atoli(s, a);
    }

    ll addr(ll lpos, ll mode) {
        ll ret;
        if(mode==0)
            ret=a[lpos];
        else if(mode==2)
            ret=a[lpos]+reloffs;
        else
            assert(false);
        return ret;
    };

    ll lload(ll lpos, ll mode) {
        ll ret;
        if(mode==0)
            ret=a[a[lpos]];
        else if(mode==2)
            ret=a[a[lpos]+reloffs];
        else
            ret=a[lpos];
        return ret;
    };

    /* runs proc reading input from iq and writes output to oq.
     * @return true after halt/99 instruction, else
     * false on waiting for input.
     */
    bool run() {
        while(a[pc]!=99) {
            ll opcode=a[pc]%100;
            ll p1=(a[pc]/100)%10;
            ll p2=(a[pc]/1000)%10;
            ll p3=(a[pc]/10000)%10;

#ifdef DEBUG
            cout<<"prg: a[pc]="<<a[pc]<<" a[pc+1]="<<a[pc+1]<<" a[pc+2]="<<a[pc+2]<<" a[pc+3]="<<a[pc+3]<<" opcode="<<opcode<<" p1="<<p1<<" p2="<<p2<<" p3="<<p3<<endl;
#endif

            if(opcode==1) { // add
                ll sto=addr(pc+3, p3);
                a[sto]=lload(pc+1, p1)+lload(pc+2, p2);
                pc+=4;
            } else if(opcode==2) { // mul
                ll sto=addr(pc+3, p3);
                a[sto]=lload(pc+1, p1)*lload(pc+2, p2);
                pc+=4;
            } else if(opcode==3) {  // input
                if(iq.size()==0) {
#ifdef DEBUG
                    cout<<"prg: input size==0, wait for input return"<<endl;
#endif
                    return false;
                }

                ll aux=iq.front();
                iq.pop();
                assert(aux<=INT_MAX && aux>=INT_MIN);
                ll sto=addr(pc+1, p1);
                a[sto]=aux;
                pc+=2;
            } else if(opcode==4) { // output
                ll outp=lload(pc+1, p1);
                oq.push(outp);
#ifdef DEBUG
                cout<<"prg: outp: "<<outp<<endl;
#endif
                pc+=2;
            } else if(opcode==5) {  // jump if true
                if(lload(pc+1, p1))
                    pc=lload(pc+2, p2);
                else
                    pc+=3;
            } else if(opcode==6) { // jump if false
                if(!lload(pc+1, p1))
                    pc=lload(pc+2, p2);
                else
                    pc+=3;
            } else if(opcode==7) {  // less than
                ll v=lload(pc+1,p1)<lload(pc+2,p2);
                ll sto=addr(pc+3, p3);
                a[sto]=v;
                pc+=4;
            } else if(opcode==8) {  // equals
                ll v=lload(pc+1,p1)==lload(pc+2,p2);
                ll sto=addr(pc+3, p3);
                a[sto]=v;
                pc+=4;
            } else if(opcode==9) {
                ll v=lload(pc+1,p1);
                reloffs+=v;
                pc+=2;
            } else {
                cout<<"unknown opcode: "<<opcode<<endl;
                assert(false);
            }
        }
#ifdef DEBUG
        cout<<"prg: halt"<<endl;
#endif
        return true;
    }
};

string vpath;

void solve(string s) {
    prg proc(s);

    vs m;
    proc.run();
    string str="";
    pii start= {-1,-1};
    while(proc.oq.size()) {
        int o=proc.oq.front();
        proc.oq.pop();
        if(o==35)
            str+="#";
        else if(o==46)
            str+=".";
        else if(o==10) {
            if(str.size()>0)
                m.push_back(str);
            str="";
        } else if(o=='<' || o=='>' || o=='^' || o=='v') {
            start.first=m.size();
            start.second=str.size();
            str+=o;
        }
    }
    if(str.size()>0)
        m.push_back(str);

    size_t ma=0;
    for(int i=0; i<m.size(); i++)
        ma=max(ma, m[i].size());

    for(int i=0; i<m.size(); i++)
        while(m[i].size()<ma)
            m[i]+=" ";

    /* find intersecions */

    // N, E, S, W
    vector<pii> offs= { {-1,0},{0,1},{1,0},{0,-1}};
    int cdir;
    if(m[start.first][start.second]=='^')
        cdir=0;
    else if(m[start.first][start.second]=='>')
        cdir=1;
    else if(m[start.first][start.second]=='v')
        cdir=2;
    else if(m[start.first][start.second]=='<')
        cdir=3;
    else {
        cout<<"unknown dir: "<<m[start.first][start.second]<<endl;
        assert(false);
    }

    set<pii> path;

    int ans=0;
    pii pos=start;
    path.insert(pos);
    while(true) {

        pii p= {pos.first+offs[cdir].first, pos.second+offs[cdir].second};

        if(p.first>=0 && p.second>=0 && p.first<m.size() && p.second<m[p.first].size()) {

            if(path.count(p))    {
                ans+=(p.first*p.second);
                pos=p;
                vpath+="+";
                continue;
            }

            if(m[p.first][p.second]=='#') {
                path.insert(p);
                pos=p;
                vpath+="+";
                continue;
            }
        }

        p= {-1,-1};
        bool ok=false;
        for(int i=0; i<3; i++) {
            cdir=(cdir+1)%4;
            p= {pos.first+offs[cdir].first, pos.second+offs[cdir].second};
            if(p.first<0 || p.second<0 || p.first>=m.size() || p.second>=m[p.first].size())
                continue;

            if(m[p.first][p.second]=='#' && path.count(p)==0) {
                path.insert(p);
                pos=p;
                ok=true;
                if(i==0)
                    vpath+="R+";
                else
                    vpath+="L+";
                break;
            }
        }
        if(!ok)
            break;
    }
    /* dump to stdout */
    /*
        for(int i=0; i<m.size(); i++)
            cout<<m[i]<<endl;
    */

    cout<<"path.size()="<<path.size()<<endl;
    cout<<"star1 "<<ans<<endl;

}

ll atol(string &s) {
    istringstream iss(s);
    ll i;
    iss>>i;
    return i;
}

string itoa(int i) {
    ostringstream oss;
    oss<<i;
    return oss.str();
}

void solve2(string s) {
    prg proc(s);
    assert(proc.a[0]==1);
    proc.a[0]=2;

    cout<<"vpath.size()="<<vpath.size()<<endl;
    cout<<"vpath="<<vpath<<endl;
    const int M=25;

    string A,B,C;
    vs cmdABC(3);
    string cmdMain;
    string mainRot;

    function<string(string)> convert2=[&](string lcmd) {
        string ans;
        int cnt=0;
        for(int j=0; j<lcmd.size(); j++) {
            if(lcmd[j]=='L' || lcmd[j]=='R') {
                if(cnt>0) {
                    if(ans.size()>0) {
                        ans+=',';
                    }
                    string sd=itoa(cnt);
                    for(char c : sd) {
                        ans+=c;
                    }
                }
                if(ans.size()>0) {
                    ans+=',';
                }
                ans+=lcmd[j];
                cnt=0;
            } else
                cnt++;
        }
        if(cnt>0) {
            if(ans.size()>0) {
                ans+=',';
            }
            string sd=itoa(cnt);
            for(char c : sd) {
                ans+=c;
            }
        }
//        cout<<"convert2(), lcmd="<<lcmd<<endl;
//        cout<<"convert2(),  ans="<<ans<<endl;
        return ans;
    };
    cout<<"vpath converted: "<<convert2(vpath)<<endl;

    function<bool()> convert=[&]() {
        //cout<<"mainRot="<<mainRot<<endl;
        for(size_t i=0; i<mainRot.size(); i++) {
            if(i>0) {
                cmdMain+=",";
            }
            cmdMain+=mainRot[i];
        }
        if(cmdMain.size()>M) {
            return false;
        }

        for(string str : {
                    A, B, C
                }) {
            string cmd=convert2(str);
            if(cmd.size()>M)
                return false;
        }
        cout<<"convert ok!!!"<<endl;
        return true;
    };

    function<bool()> check=[&]() {
        mainRot="";
        vs abc= {A,B,C};
//cout<<"check, A.size()="<<A.size()<<" B.size()="<<B.size()<<" C.size()="<<C.size()<<endl;

        size_t pos=0;
        while(pos<vpath.size()) {
            bool ok=false;
            /* check if one of the three routines work at current position */
            for(size_t i=0; i<abc.size(); i++) {
                if(vpath.find(abc[i], pos)==pos) {
                    pos+=abc[i].size();
                    mainRot+=((char)('A'+i));
                    ok=true;
                    break;
                }
            }
            if(!ok)
                break;
        }
        if(pos>=vpath.size()) {
/*
            cout<<"match"<<endl;
            cout<<"A="<<A<<endl;
            cout<<"B="<<B<<endl;
            cout<<"C="<<C<<endl;
*/
            if(convert())
                return true;
        }
        return false;
    };

    bool done=false;
    for(int Asz=2; !done; Asz++) {
        //cout<<"Asz="<<Asz<<endl;
        A=vpath.substr(0,Asz);
        if(convert2(A).size()>M)
            break;

        int Bstart=Asz;
        //cout<<"Bstart="<<Bstart<<endl;
        for(int Bsz=1; !done; Bsz++) {
            B=vpath.substr(Bstart, Bsz);
            if(convert2(B).size()>M)
                break;

            for(int Cstart=Bstart+Bsz; Cstart<200; Cstart++) {
                for(int Csz=1; !done; Csz++) {

/*
if(Asz==31 && Bsz==38 && Csz==45) {
    cout<<"maybe match, Cpos="<<Cstart<<endl;
    cout<<"A="<<convert2(A)<<endl;
    cout<<"B="<<convert2(B)<<endl;
    cout<<"C="<<convert2(C)<<endl;
}
*/
                    C=vpath.substr(Cstart, Csz);
                    if(convert2(C).size()>M)
                        break;

                    if(check()) {
                        done=true;
                    }
                }
            }
        }
    }

    if(!done) {
        cout<<"did not found a working match :/"<<endl;
        return;
    }

    /* Solved by hand
        cmdMain="A,B,A,C,A,B,C,A,B,C";
    */
    for(char c : cmdMain)
        proc.iq.push(c);
    proc.iq.push(10);

    cmdABC[0]=convert2(A);
    cmdABC[1]=convert2(B);
    cmdABC[2]=convert2(C);

// R,8,R,10,R,10, R,4,R,8,R,10,R,12, R,8,R,10,R,10 ,R,12,R,4,L,12,L,12, R,8,R,10,R,10, R,4,R,8,R,10,R,12, R,12,R,4,L,12,L,12, R,8,R,10,R,10, R,4,R,8,R,10,R,12, R,12,R,4,L,12,L,12
// A              B                  A              C                   A              B                  C                   A              B                  C

    /* Solved by hand
        cmdABC[0]="R,8,R,10,R,10";
        cmdABC[1]="R,4,R,8,R,10,R,12";
        cmdABC[2]="R,12,R,4,L,12,L,12";
    */

    for(int i=0; i<3; i++) {
        for(char c : cmdABC[i])
            proc.iq.push(c);
        proc.iq.push(10);
    }

    proc.iq.push('n');
    proc.iq.push(10);

    proc.run();
    string sans;
    int star2;
    while(proc.oq.size()) {
        ll ans=proc.oq.front();
        proc.oq.pop();
        if(ans<128) {
            sans+=(char)ans;
        } else {
            star2=ans;
        }
    }

    cout<<sans<<endl;
    cout<<"star2 "<<star2<<endl;

}

int main() {
    cins(s);
    solve(s);
    solve2(s);
}
