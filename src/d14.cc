/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll atol(string &s) {
    istringstream iss(s);
    ll i;
    iss>>i;
    return i;
}

void atoli(string &s, vll &a) {
    istringstream iss(s);
    int i=0;
    while(iss.good())
        cin>>a[i++];
}

map<string, map<string,int>> m;
map<string, int> mmult;

void parse() {
    string s;
    while(true) {
        getline(cin,s);
        if(s.size()==0)
            break;

        istringstream iss(s);
        vector<pair<int,string>> inp;
        while(true) {
            pair<int,string> p;
            iss>>p.first>>p.second;
            if(p.second.back()==',') {
                p.second.pop_back();
                inp.push_back(p);
            } else {
                inp.push_back(p);
                break;
            }
        }

        string tmp;
        iss>>tmp;   // "=>"
        pair<int,string> outp;
        iss>>outp.first>>outp.second;
    
        mmult[outp.second]=outp.first;
        for(auto p : inp) {
            m[outp.second][p.second]=p.first;
        }
    }
//    cout<<"m.size="<<m.size()<<endl;
}

map<string,ll> store;

ll dfs(string what, ll amt) {
    if(what=="ORE")
        return amt;

    ll cnt=min(store[what],amt);
    amt-=cnt;
    store[what]-=cnt;

//cout<<"producing "<<amt<<" "<<what<<endl;

    ll mamt;

    if(amt%mmult[what]==0)
        mamt=amt/mmult[what];
    else {
        mamt=amt/mmult[what]+1;
    }

    ll ans=0;
    for(auto p : m[what]) {
        ans+=dfs(p.first, p.second*mamt);
    }
/* we produced mmult[what]*mamt of what */

    store[what]+=(mmult[what]*mamt)-amt;

    return ans;
}

const ll N=1000000000000;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        parse();
        ll ans=dfs("FUEL", 1);
        cout<<"star1 "<<ans<<endl;

        ll hi=2;
        while(true) {
            store.clear();
            ans=dfs("FUEL", hi);
            if(ans>N)
                break;
            hi*=2;
        }

        ll lo=1;
        while(lo+1<hi) {
            store.clear();
            ll mid=(lo+hi)/2;
            ans=dfs("FUEL", mid);
            if(ans>N) {
                hi=mid;
            } else {
                lo=mid;
            }
        }
        cout<<"star2 "<<lo<<endl;
}

