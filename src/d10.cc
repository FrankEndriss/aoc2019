/* stolen from https://github.com/kaiskye/adventofcode-2019/blob/master/day10b.cpp
 * Thanks!
 */

#include <bits/stdc++.h>

struct Point {
    int row;
    int col;
};

bool operator==(const Point& a, const Point& b) {
    return a.row == b.row && a.col == b.col;
}

bool operator!=(const Point& a, const Point& b) {
    return !(a == b);
}

int gcd(int a, int b) {
    if (a == 0) {
        return b;
    }
    return gcd(b % a, a);
}

int get_slope(const Point& a, const Point& b) {
    int d_row = b.row - a.row;
    int d_col = b.col - a.col;
    const int m = gcd(std::abs(d_row), std::abs(d_col));
    if (m != 0) {
        d_row /= m;
        d_col /= m;
    }
    return d_row * 10000 + d_col;
}

int count_detected(const std::vector<Point>& points, const Point& origin) {
    std::unordered_set<int> seen;
    for (const Point& p : points) {
        if (p != origin) {
            seen.insert(get_slope(p, origin));
        }
    }
    return seen.size();
}

double get_angle(const Point& a, const Point& b) {
    const double d_row = b.row - a.row;
    const double d_col = b.col - a.col;
    const double rad = std::atan2(d_col, -d_row);
    const double deg = 180.0 + 180.0 * rad / 3.14159265;
    return std::fmod(std::min(std::max(deg, 0.0), 360.0), 360.0);
}

int get_dist(const Point& a, const Point& b) {
    const int d_row = b.row - a.row;
    const int d_col = b.col - a.col;
    return d_row * d_row + d_col * d_col;
}

Point find_vaporized(std::vector<Point> points, const Point& origin, int k) {
    std::unordered_map<int, std::set<int>> seen;
    for (const Point& p : points) {
        const int slope = get_slope(p, origin);
        const int dist = get_dist(p, origin);
        seen[slope].insert(dist);
    }
    const auto sort_order = [&](const Point& p) {
        if (p == origin) {
            return -1.0;
        }
        const int slope = get_slope(p, origin);
        const int dist = get_dist(p, origin);
        const int blocked_by_count = std::distance(seen[slope].begin(), seen[slope].find(dist));
        const double angle = get_angle(p, origin);
        return blocked_by_count * 360.0 + angle;
    };
    std::nth_element(points.begin(), points.begin() + k, points.end(),
    [&](const Point& a, const Point& b) {
        return sort_order(a) < sort_order(b);
    });
    return points[k];
}

std::vector<Point> read_input(const char* filename) {
    std::fstream f(filename);
    if (!f) {
        return {};
    }
    std::vector<Point> result;
    int row = 0;
    std::string s;
    while (std::getline(f, s)) {
        int col = 0;
        for (char c : s) {
            if (c == '#') {
                result.push_back({row, col});
            }
            col++;
        }
        row++;
    }
    return result;
}

int main() {
    std::vector<Point> points = read_input("day10.test");
    if (points.empty()) {
        return 1;
    }
    int station = 0;
    int max_detected = 0;
    for (int i = 0; i < points.size(); i++) {
        const int detected = count_detected(points, points[i]);
        if (detected > max_detected) {
            max_detected = detected;
            station = i;
        }
    }
    const Point& vaporized = find_vaporized(points, points[station], 200);
    std::cout << max_detected << std::endl;
    std::cout << vaporized.col * 100 + vaporized.row << std::endl;
    return 0;
}
