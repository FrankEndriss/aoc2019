/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename T>
void atoli(string &s, T &m) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]!='-' && (s[i]<'0' || s[i]>'9'))
            s[i]=' ';

    istringstream iss(s);
    ll idx=0;
    while(iss.good()) {
        ll aux;
        iss>>aux;
        m[idx]=aux;
        idx++;
    }
}

/* Incode computer, see https://adventofcode.com/2019/day/9
 * This implementation works with mapped memory, ie memory
 * is kind of infinite, and can have negative addresses, too.
 */
struct prg {
    ll pc;
    ll reloffs;
    map<ll,ll> a;       // memory

    queue<ll> iq;   // stdin
    queue<ll> oq;   // stdout

    /* Give memory/program as parameter, an arbitrary list of integers. */
    prg(string &s) : pc(0), reloffs(0) {
        atoli(s, a);
    }

    ll addr(ll lpos, ll mode) {
            ll ret;
            if(mode==0)
                ret=a[lpos];
            else if(mode==2) {
                ret=a[lpos]+reloffs;
            } else
                assert(false);
            return ret;
        };

    ll lload(ll lpos, ll mode) {
            ll ret;
            if(mode==0)
                ret=a[a[lpos]];
            else if(mode==2) {
                ret=a[a[lpos]+reloffs];
            } else
                ret=a[lpos];
            return ret;
        };

    /* runs proc reading input from iq and writes output to oq.
     * @return true after halt/99 instruction, else
     * false on waiting for input.
     */
    bool run() {
        while(a[pc]!=99) {
            ll opcode=a[pc]%100;
            ll p1=(a[pc]/100)%10;
            ll p2=(a[pc]/1000)%10;
            ll p3=(a[pc]/10000)%10;

#ifdef DEBUG
            cout<<"prg: a[pc]="<<a[pc]<<" a[pc+1]="<<a[pc+1]<<" a[pc+2]="<<a[pc+2]<<" a[pc+3]="<<a[pc+3]<<" opcode="<<opcode<<" p1="<<p1<<" p2="<<p2<<" p3="<<p3<<endl;
#endif

            if(opcode==1) { // add
                ll sto=addr(pc+3, p3);
                a[sto]=lload(pc+1, p1)+lload(pc+2, p2);
                pc+=4;
            } else if(opcode==2) { // mul
                ll sto=addr(pc+3, p3);
                a[sto]=lload(pc+1, p1)*lload(pc+2, p2);
                pc+=4;
            } else if(opcode==3) {  // input
                if(iq.size()==0) {
#ifdef DEBUG
                    cout<<"prg: input size==0, wait for input return"<<endl;
#endif
                    return false;
                }

                ll aux=iq.front();
                iq.pop();
                assert(aux<=INT_MAX && aux>=INT_MIN);
                ll sto=addr(pc+1, p1);
                a[sto]=aux;
                pc+=2;
            } else if(opcode==4) { // output
                ll outp=lload(pc+1, p1);
                oq.push(outp);
#ifdef DEBUG
                cout<<"prg: outp: "<<outp<<endl;
#endif
                pc+=2;
            } else if(opcode==5) {  // jump if true
                if(lload(pc+1, p1))
                    pc=lload(pc+2, p2);
                else
                    pc+=3;
            } else if(opcode==6) { // jump if false
                if(!lload(pc+1, p1))
                    pc=lload(pc+2, p2);
                else
                    pc+=3;
            } else if(opcode==7) {  // less than
                ll v=lload(pc+1,p1)<lload(pc+2,p2);
                ll sto=addr(pc+3, p3);
                a[sto]=v;
                pc+=4;
            } else if(opcode==8) {  // equals
                ll v=lload(pc+1,p1)==lload(pc+2,p2);
                ll sto=addr(pc+3, p3);
                a[sto]=v;
                pc+=4;
            } else if(opcode==9) {
                ll v=lload(pc+1,p1);
                reloffs+=v;
                pc+=2;
            } else {
                cout<<"unknown opcode: "<<opcode<<endl;
                assert(false);
            }
        }
#ifdef DEBUG
        cout<<"prg: halt"<<endl;
#endif
        return true;
    }
};

const int INF=1e9;
void solve() {
    cins(s);
    prg proc(s);

#define N (1)
#define S (2)
#define W (3)
#define E (4)

    vi right(5);
    right[N]=E;
    right[E]=S;
    right[S]=W;
    right[W]=N;

    vector<pii> offs(5);
    offs[N]= {-1,0};
    offs[S]= {1,0};
    offs[W]= {0,-1};
    offs[E]= {0,1};

    vs dirname(5);
    dirname[N]="N";
    dirname[S]="S";
    dirname[W]="W";
    dirname[E]="E";

    const int GN=10000;
    vvi m(GN, vi(GN)); // 0==unkown, 1==wall, 2==floor
    pii pos;
    pii fini;

    function<int(int)> mov=[&](int dir) {
        pii npos= {pos.first+offs[dir].first, pos.second+offs[dir].second};

        proc.iq.push(dir);
        proc.run();
        ll o=proc.oq.front();
        proc.oq.pop();
        if(o==0)
            m[npos.first][npos.second]=1;
        else if(o==1 || o==2) {
            m[npos.first][npos.second]=2;
            pos=npos;
            if(o==2)
                fini=pos;
        } else
            assert(false);
        return o;
    };

    const pii start= {GN/2,GN/2}; // y,x  like screen array, first on top
    m[start.first][start.second]=2;

    vvi dp(GN, vi(GN,INF));
    dp[start.first][start.second]=0;

    function<void()> dfs=[&]() {
#ifdef DEBUG
        cout<<"pos="<<pos.first<<" "<<pos.second<<endl;
#endif
        for(int ldir : {
                    N, S, E, W
                }) {
            pii np= {pos.first+offs[ldir].first, pos.second+offs[ldir].second };
            int ndp=dp[pos.first][pos.second]+1;
            if(m[np.first][np.second]!=1 && dp[np.first][np.second]>ndp) {
                if(mov(ldir)) {
                    dp[np.first][np.second]=ndp;
                    dfs();
                    mov(right[right[ldir]]);
                }
            }
        }
    };

    pos=start;
    dfs();

    cout<<"star1 "<<dp[fini.first][fini.second]<<endl;

    /* bfs distance */

    dp=vvi(GN, vi(GN,INF));
    dp[fini.first][fini.second]=0;

    queue<pii> q;
    q.push(fini);
    int ans2=0;
    while(q.size()) {
        pii p=q.front();
        q.pop();
        for(int ldir : {
                    N, S, E, W
                }) {
            pii np= {p.first+offs[ldir].first, p.second+offs[ldir].second };
            if(m[np.first][np.second]==2 && dp[np.first][np.second]==INF) {
                dp[np.first][np.second]=dp[p.first][p.second]+1;
                ans2=max(ans2,dp[np.first][np.second]);
                q.push(np);
            }
        }
    }
    cout<<"star2 "<<ans2<<endl;
}

void run_boost() {
    string s="1102,34463338,34463338,63,1007,63,34463338,63,1005,63,53,1101,3,0,1000,109,988,209,12,9,1000,209,6,209,3,203,0,1008,1000,1,63,1005,63,65,1008,1000,2,63,1005,63,904,1008,1000,0,63,1005,63,58,4,25,104,0,99,4,0,104,0,99,4,17,104,0,99,0,0,1102,35,1,1010,1102,1,33,1013,1101,0,715,1022,1102,1,20,1004,1102,1,24,1012,1101,36,0,1005,1101,0,655,1024,1102,32,1,1014,1101,0,499,1026,1102,1,242,1029,1101,0,25,1002,1101,0,27,1017,1101,708,0,1023,1101,0,21,1016,1101,0,28,1000,1101,0,492,1027,1102,34,1,1015,1102,29,1,1007,1102,247,1,1028,1101,0,39,1011,1102,1,31,1018,1102,1,0,1020,1102,1,37,1006,1101,1,0,1021,1102,26,1,1009,1102,1,38,1008,1101,30,0,1019,1102,1,23,1001,1102,650,1,1025,1101,22,0,1003,109,7,2101,0,-7,63,1008,63,29,63,1005,63,205,1001,64,1,64,1105,1,207,4,187,1002,64,2,64,109,-1,1202,-1,1,63,1008,63,35,63,1005,63,227,1106,0,233,4,213,1001,64,1,64,1002,64,2,64,109,17,2106,0,5,4,239,1105,1,251,1001,64,1,64,1002,64,2,64,109,-1,21108,40,39,-4,1005,1018,271,1001,64,1,64,1106,0,273,4,257,1002,64,2,64,109,-9,1206,8,285,1106,0,291,4,279,1001,64,1,64,1002,64,2,64,109,-13,2108,27,0,63,1005,63,307,1106,0,313,4,297,1001,64,1,64,1002,64,2,64,109,11,2101,0,-5,63,1008,63,37,63,1005,63,339,4,319,1001,64,1,64,1105,1,339,1002,64,2,64,109,13,21101,41,0,-9,1008,1015,41,63,1005,63,365,4,345,1001,64,1,64,1106,0,365,1002,64,2,64,109,-14,1201,-6,0,63,1008,63,22,63,1005,63,385,1106,0,391,4,371,1001,64,1,64,1002,64,2,64,109,-10,1202,3,1,63,1008,63,22,63,1005,63,417,4,397,1001,64,1,64,1105,1,417,1002,64,2,64,109,6,1207,-3,21,63,1005,63,437,1001,64,1,64,1105,1,439,4,423,1002,64,2,64,109,16,21107,42,41,-8,1005,1014,455,1105,1,461,4,445,1001,64,1,64,1002,64,2,64,109,-28,2107,24,7,63,1005,63,481,1001,64,1,64,1106,0,483,4,467,1002,64,2,64,109,33,2106,0,0,1001,64,1,64,1106,0,501,4,489,1002,64,2,64,109,-18,2108,38,-1,63,1005,63,519,4,507,1105,1,523,1001,64,1,64,1002,64,2,64,109,-3,1208,-4,25,63,1005,63,545,4,529,1001,64,1,64,1106,0,545,1002,64,2,64,109,12,21102,43,1,-8,1008,1010,43,63,1005,63,571,4,551,1001,64,1,64,1106,0,571,1002,64,2,64,109,-1,1207,-8,27,63,1005,63,593,4,577,1001,64,1,64,1106,0,593,1002,64,2,64,109,-7,21101,44,0,8,1008,1018,42,63,1005,63,617,1001,64,1,64,1105,1,619,4,599,1002,64,2,64,109,-4,1208,-1,39,63,1005,63,639,1001,64,1,64,1105,1,641,4,625,1002,64,2,64,109,13,2105,1,5,4,647,1106,0,659,1001,64,1,64,1002,64,2,64,109,4,1206,-3,673,4,665,1106,0,677,1001,64,1,64,1002,64,2,64,109,-22,21108,45,45,10,1005,1011,699,4,683,1001,64,1,64,1105,1,699,1002,64,2,64,109,29,2105,1,-7,1001,64,1,64,1105,1,717,4,705,1002,64,2,64,109,-19,21107,46,47,5,1005,1016,739,4,723,1001,64,1,64,1106,0,739,1002,64,2,64,109,-8,2102,1,2,63,1008,63,33,63,1005,63,763,1001,64,1,64,1106,0,765,4,745,1002,64,2,64,109,1,1201,-2,0,63,1008,63,25,63,1005,63,791,4,771,1001,64,1,64,1105,1,791,1002,64,2,64,109,16,1205,0,803,1105,1,809,4,797,1001,64,1,64,1002,64,2,64,109,-8,1205,9,827,4,815,1001,64,1,64,1106,0,827,1002,64,2,64,109,-4,2102,1,-3,63,1008,63,36,63,1005,63,853,4,833,1001,64,1,64,1106,0,853,1002,64,2,64,109,17,21102,47,1,-6,1008,1019,50,63,1005,63,877,1001,64,1,64,1105,1,879,4,859,1002,64,2,64,109,-29,2107,22,5,63,1005,63,897,4,885,1106,0,901,1001,64,1,64,4,64,99,21102,27,1,1,21101,0,915,0,1106,0,922,21201,1,25338,1,204,1,99,109,3,1207,-2,3,63,1005,63,964,21201,-2,-1,1,21101,942,0,0,1105,1,922,22102,1,1,-1,21201,-2,-3,1,21102,957,1,0,1106,0,922,22201,1,-1,-2,1105,1,968,21202,-2,1,-2,109,-3,2106,0,0";

    prg proc(s);

    proc.iq.push(1);
    int code=proc.run();
    cout<<"run_boost fini, code="<<code<<endl;
    while(proc.oq.size()) {
        ll o=proc.oq.front();
        proc.oq.pop();
        cout<<"outp="<<o<<endl;
        assert(o==2955820355);
    }
    cout<<"no more output"<<endl;
}

void copy_self() {
    string s="109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
    prg proc=(s);
    proc.run();
    while(proc.oq.size()) {
        ll o=proc.oq.front();
        proc.oq.pop();
        cout<<"outp="<<o<<endl;
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
    //run_boost();
    //copy_self();
}

