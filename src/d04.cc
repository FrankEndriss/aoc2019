/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll atol(string &s) {
    istringstream iss(s);
    ll i;
    iss>>i;
    return i;
}

void atoli(string &s, vll &a) {
    istringstream iss(s);
    int i=0;
    while(iss.good())
        cin>>a[i++];
}

bool check(int i) {
    vi clen(10, 1);
    vi mlen(10);

    int ans=true;
    int prev=-1;
    while(i) {
        int a=i%10;
        i/=10;

        if(a==prev) {
            clen[a]++;
        } else {
            if(prev>=0) {
                clen[prev]=1;
            }
            clen[a]=1;
        }

        for(int j=0; j<10; j++)
            mlen[j]=max(mlen[j],clen[j]);

        if(prev>=0 && prev<a)
            ans=false;

        prev=a;
    }

    if(!ans)
        return false;

    for(int i=0; i<10; i++)
        if(mlen[i]==2)
            return true;

    return false;
}

void solve() {
    int ans=0;
    for(int i=146810; i<612564+1; i++) {
        if(check(i))
            ans++;
    }
    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        solve();
}

