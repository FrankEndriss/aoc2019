/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

string s;
void solve() {

    vvi cnt;
    for(size_t l=0, idx=0; idx<s.size(); l++) {
        cnt.resize(l+1, vi(3));
        for(int i=0; i<25*6; i++)
            cnt[l][s[idx++]-'0']++;
    }

/* simply sort works sinc we want to sort by number
 * of zeros, and these are at the first index of cnt[i]
 */
    auto it=min_element(cnt.begin(), cnt.end());
    cout<<"i0="<<(*it)[0]<<" i1*i2="<<(*it)[1]*(*it)[2]<<endl;
}

void solve2() {
    vvvi cnt;
    for(size_t l=0, idx=0; idx<s.size(); l++) {
        cnt.resize(l+1, vvi(6, vi(25)));
        for(int i=0; i<6; i++) {
            for(int j=0; j<25; j++) {
                cnt[l][i][j]=s[idx++]-'0';
            }
        }
    }
    for(int j=0; j<6; j++) {
        for(int i=0; i<25; i++) {
            for(size_t k=0; k<cnt.size(); k++) {
                if(cnt[k][j][i]!=2) {
                    cout<<(cnt[k][j][i]==1?"X":" ");
                    break;
                }
            }
        }
        cout<<endl;
    }
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        cin>>s;
        solve();
        solve2();
}

