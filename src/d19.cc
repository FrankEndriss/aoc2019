/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename T>
void atoli(string &s, T &m) {
    for(size_t i=0; i<s.size(); i++)
        if(s[i]!='-' && (s[i]<'0' || s[i]>'9'))
            s[i]=' ';

    istringstream iss(s);
    ll idx=0;
    while(iss.good()) {
        ll aux;
        iss>>aux;
        m[idx]=aux;
        idx++;
    }
}

/* Incode computer, see https://adventofcode.com/2019/day/9
 * This implementation works with mapped memory, ie memory
 * is kind of infinite, and can have negative addresses, too.
 */
struct prg {
    ll pc;
    ll reloffs;
    map<ll,ll> a;   // memory

    queue<ll> iq;   // stdin
    queue<ll> oq;   // stdout

    /* Give memory/program as parameter, an arbitrary list of integers. */
    prg(string s) : pc(0), reloffs(0) {
        atoli(s, a);
    }

    ll addr(ll lpos, ll mode) {
        ll ret;
        if(mode==0)
            ret=a[lpos];
        else if(mode==2)
            ret=a[lpos]+reloffs;
        else
            assert(false);
        return ret;
    };

    ll lload(ll lpos, ll mode) {
        ll ret;
        if(mode==0)
            ret=a[a[lpos]];
        else if(mode==2)
            ret=a[a[lpos]+reloffs];
        else
            ret=a[lpos];
        return ret;
    };

    /* runs proc reading input from iq and writes output to oq.
     * @return true after halt/99 instruction, else
     * false on waiting for input.
     */
    bool run() {
        while(a[pc]!=99) {
            ll opcode=a[pc]%100;
            ll p1=(a[pc]/100)%10;
            ll p2=(a[pc]/1000)%10;
            ll p3=(a[pc]/10000)%10;

#ifdef DEBUG
            cout<<"prg: a[pc]="<<a[pc]<<" a[pc+1]="<<a[pc+1]<<" a[pc+2]="<<a[pc+2]<<" a[pc+3]="<<a[pc+3]<<" opcode="<<opcode<<" p1="<<p1<<" p2="<<p2<<" p3="<<p3<<endl;
#endif

            if(opcode==1) { // add
                ll sto=addr(pc+3, p3);
                a[sto]=lload(pc+1, p1)+lload(pc+2, p2);
                pc+=4;
            } else if(opcode==2) { // mul
                ll sto=addr(pc+3, p3);
                a[sto]=lload(pc+1, p1)*lload(pc+2, p2);
                pc+=4;
            } else if(opcode==3) {  // input
                if(iq.size()==0) {
#ifdef DEBUG
                    cout<<"prg: input size==0, wait for input return"<<endl;
#endif
                    return false;
                }

                ll aux=iq.front();
                iq.pop();
                assert(aux<=INT_MAX && aux>=INT_MIN);
                ll sto=addr(pc+1, p1);
                a[sto]=aux;
                pc+=2;
            } else if(opcode==4) { // output
                ll outp=lload(pc+1, p1);
                oq.push(outp);
#ifdef DEBUG
                cout<<"prg: outp: "<<outp<<endl;
#endif
                pc+=2;
            } else if(opcode==5) {  // jump if true
                if(lload(pc+1, p1))
                    pc=lload(pc+2, p2);
                else
                    pc+=3;
            } else if(opcode==6) { // jump if false
                if(!lload(pc+1, p1))
                    pc=lload(pc+2, p2);
                else
                    pc+=3;
            } else if(opcode==7) {  // less than
                ll v=lload(pc+1,p1)<lload(pc+2,p2);
                ll sto=addr(pc+3, p3);
                a[sto]=v;
                pc+=4;
            } else if(opcode==8) {  // equals
                ll v=lload(pc+1,p1)==lload(pc+2,p2);
                ll sto=addr(pc+3, p3);
                a[sto]=v;
                pc+=4;
            } else if(opcode==9) {
                ll v=lload(pc+1,p1);
                reloffs+=v;
                pc+=2;
            } else {
                cout<<"unknown opcode: "<<opcode<<endl;
                assert(false);
            }
        }
#ifdef DEBUG
        cout<<"prg: halt"<<endl;
#endif
        return true;
    }
};


/* Todays problem is again one of the kind where we need to 
 * figure out what is the question.
 * After having found that the answer is somewhere below 1200...
 * it is streightforward brute force.
 */
void solve(string s) {
    prg proc(s);

    vs m;
    vvb checked(10000, vb(10000, false));

/* We see from display that the solution is not at top left 50/50 */
    for(int i=0; i<50; i++) 
        for(int j=0; j<50; j++)
            checked[i][j]=true;

    function<void(int,int)> load=[&](int x, int y) {
        string s;
        while(m.size()<y)
            m.push_back(s);

        for(int i=0; i<m.size(); i++) {
            while(m[i].size()<x) {
                prg p1=proc;
                p1.iq.push(m[i].size());
                p1.iq.push(i);
                p1.run();

                assert(p1.oq.size());
                int a=p1.oq.front();
                p1.oq.pop();
                if(a) {
                    m[i]+='#';
                } else
                    m[i]+='.';
            }
        }
    };

    function<bool(int,int)> check=[&](int x, int y) {
        return m[y][x]=='#' && m[y+99][x]=='#' && m[y][x+99]=='#' && m[y+99][x+99]=='#';
    };

    int ans2X=0;
    int ans2Y=0;
    function<bool()> checkAll=[&]() {
        for(size_t y=0; y+99<m.size(); y++) {
            for(size_t x=0; x+99<m[y].size(); x++) {
                if(!checked[y][x]) {
                    bool res=check(x,y);
                    checked[y][x]=true;
                    if(res && ans2X==0) {
                        ans2X=x;
                        ans2Y=y;
                        return true;
                    }
                }
            }
        }
        return false;
    };

    int gx=1200;
    int gy=1200;
    load(gx,gy);

    int ans1=0;
    for(int i=0; i<m.size(); i++)
        for(int j=0; j<m[i].size(); j++)
            if(m[i][j]=='#')
                ans1++;
    cout<<"star1 "<<ans1<<endl;

    checkAll();
    while(ans2X==0) {
        gx+=100;
        gy+=100;
        cout<<"gx="<<gx<<endl;
        load(gx,gy);
        if(checkAll())
            break;
    }
    cout<<"star2 "<<ans2X*10000+ans2Y<<endl;
}

int main() {
    cins(s);
    solve(s);
}

