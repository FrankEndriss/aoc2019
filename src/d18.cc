/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const vector<pii> offs= { {-1,0}, {1,0}, {0,-1}, {0,1}};

/* Note that this struct looks nice but is of nearly no use.
 * We can use bitmap<26> or set<int> instead.
 * Or inline all of this and us an simple int.
 */
struct keyset {
    int keys;
    int cnt;

    keyset() : keys(0), cnt(0) {
    }

    void set(int i) {
        if((keys&(1<<i))==0)
            cnt++;
        keys|=(1<<i);
    }

    void unset(int i) {
        if((keys&(1<<i)))
            cnt--;
        keys&=~(1<<i);
    }

    bool isset(int i) {
        if(i<0)
            return false;
        return (keys&(1<<i));
    }
};

bool operator<(const keyset &k1, const keyset k2) {
    return k1.keys<k2.keys;
}

const int INF=1e9;
vector<pii> keypos(256);
vs m;
int gans=INF;
map<pair<keyset,vector<pii>>, int> memo;

ll cnt=0;
/* We need to find the best order of keys, recursive. */
void search(keyset &keys, vector<pii> pos, int steps) {
    if(keys.cnt==26) {
        gans=min(gans, steps);
        return;
    }

    if(steps>=gans)
        return;

    auto k=make_pair(keys,pos);
    auto it=memo.find(k);
    if(it!=memo.end() && it->second<=steps) {
        return;
    }
    memo[k]=steps;

    cnt++;
    if(cnt%1024==0)
        cout<<"search, keys.cnt="<<keys.cnt<<" gans="<<gans<<" steps="<<steps<<" cnt="<<cnt/1024<<endl;

    for(int r=0; r<pos.size(); r++) {    // four robots

        vvi dp(m.size(), vi(m[0].size(), INF));
        dp[pos[r].first][pos[r].second]=0;
        queue<pii> q;
        q.push(pos[r]);

        while(q.size()) {
            pii lrob=q.front();
            q.pop();

            for(pii p : offs) {
                pii np= { lrob.first+p.first, lrob.second+p.second };

                if(np.first<0 || np.first>=m.size() || np.second<0 || np.second>=m[np.first].size())
                    continue;
                if(m[np.first][np.second]=='#')
                    continue;

                int ndp=dp[lrob.first][lrob.second]+1;
                bool canMove=false;
                char mat=m[np.first][np.second];

                if(mat=='.' || keys.isset(mat-'a')) {
                    canMove=true;
                } else if(mat>='a' && mat<='z') {
                    keys.set(mat-'a');
                    pii oldPos=pos[r];
                    pos[r]=np;
                    search(keys, pos, steps+ndp);
                    pos[r]=oldPos;
                    keys.unset(mat-'a');
                } else if(mat>='A' && mat<='Z') {
                    if(keys.isset(mat-'A'))
                        canMove=true;
                }

                if(canMove && dp[np.first][np.second]>ndp) {
                    dp[np.first][np.second]=ndp;
                    q.push(np);
                }
            }
        }
    }
}


void solve(int star) {
    gans=INF;
    memo.clear();

    pii start= {-1,-1};

    while(true) {
        cins(s);
        if(s.size()>0)
            m.push_back(s);
        else
            break;

        for(size_t i=0; i<s.size(); i++) {
            if(s[i]=='@') {
                start=make_pair( (int)m.size()-1, (int)i );
                m.back()[i]='.';
            }

            if(s[i]>='a' && s[i]<='z')
                keypos[s[i]]= {m.size()-1, i};
        }
    }
    assert(start.first>=0);

    vector<pii> vstart;
    if(star==2) {
        m[start.first][start.second]='#';
        m[start.first+1][start.second]='#';
        m[start.first-1][start.second]='#';
        m[start.first][start.second+1]='#';
        m[start.first][start.second-1]='#';
        m[start.first+1][start.second+1]='.';
        m[start.first-1][start.second-1]='.';
        m[start.first-1][start.second+1]='.';
        m[start.first+1][start.second-1]='.';

        vstart= {
            { start.first+1, start.second+1 },
            { start.first-1, start.second-1 },
            { start.first-1, start.second+1 },
            { start.first+1, start.second-1 }
        };
    } else {
        vstart= { start };
    }

    keyset keys;
    search(keys, vstart, 0);
    cout<<"star"<<star<<" "<<gans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    //solve(1);
    solve(2);
}

