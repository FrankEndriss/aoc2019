/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }G
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void mygetline(string &s) {
	getline(cin, s);
	while (s.size()>0 && s.back() == 13)
		s.pop_back();
}

vs m;
map<string, pair<pii, pii>> portals;
map<pii, pii> warps;
map<pii, int> leveloffs;

bool isLetter(char c) {
	return c >= 'A' && c <= 'Z';
}

bool isMap(int y, int x) {
	return y >= 0 && y < m.size() && x >= 0 && x < m[y].size() && (m[y][x] == '.' || m[y][x] == '#' || m[y][x] == '@');
}

pii start;
pii fini;

void addPortal(string label, int y, int x) {
#ifdef DEBUG
	cout << "addPortal=" << label << " y=" << y << " x=" << x << endl;
#endif
	if (label == "AA") {
		start = { y, x };
		return;
	} else if (label == "ZZ") {
		fini = { y, x };
		return;	// ignore
	}

	auto it = portals.find(label);
	if (it == portals.end()) {
		portals[label] = { { y, x }, { -1, -1 } };
	} else {
#ifdef DEBUG
		cout << "addPortal match, portal=" << label << endl;
#endif
		assert(it->second.second.first == -1);
		it->second.second.first = y;
		it->second.second.second = x;
		assert(portals[label].second.first != -1);

		warps[portals[label].first] = portals[label].second;
		warps[portals[label].second] = portals[label].first;

		pii pp = portals[label].second;
		pii pp2 = portals[label].first;
		if (pp.first == 2 || pp.first == 130 || pp.second == 2 || pp.second == 130) {
			if (!(pp2.first == 96 || pp2.second == 96 || pp2.first == 36 || pp2.second == 36)) {
				cout << "assertion fail at portal=" << label << endl;
				assert(pp2.first == 96 || pp2.second == 96 || pp2.first == 36 || pp2.second == 36);
			}
			leveloffs[portals[label].first] = -1;
			leveloffs[portals[label].second] = 1;
		} else { // inner portal
			assert(pp2.first == 2 || pp2.second == 2 || pp2.first == 130 || pp2.second == 130);
			leveloffs[portals[label].first] = 1;
			leveloffs[portals[label].second] = -1;
		}
	}

}

void checkPortals() {
#ifdef DEBUG
	for (auto ent : portals) {
		cout << "portal=" << ent.first << " fr=" << ent.second.first.first << "/" << ent.second.first.second << " to=" << ent.second.second.first << "/"
				<< ent.second.second.second << endl;
		if (warps.find(ent.second.first) == warps.end()) {
			cout << "first notfound: " << ent.first << endl;
		}
		if (warps.find(ent.second.second) == warps.end()) {
			cout << "second notfound: " << ent.first << endl;
		}
	}
#endif
}

void parse() {
	string s;
	while (true) {
		mygetline(s);
		if (s.size() == 0)
			break;
		m.push_back(s);
	}
#ifdef DEBUG
	cout << m[0] << endl;
	cout << m.back() << endl;
#endif

	string label = "  ";
	for (int y = 0; y < m.size(); y++) {
		for (int x = 0; x < m[y].size(); x++) {
			if (isLetter(m[y][x])) {
				if (y + 1 < m.size() && isLetter(m[y + 1][x])) {
					label[0] = m[y][x];
					label[1] = m[y + 1][x];
					if (isMap(y - 1, x)) {
						addPortal(label, y - 1, x);
						if (label != "AA" && label != "ZZ")
							m[y - 1][x] = '@';
					} else {
						addPortal(label, y + 2, x);
						if (label != "AA" && label != "ZZ")
							m[y + 2][x] = '@';
					}
				} else if (x + 1 < m[y].size() && isLetter(m[y][x + 1])) {
					label[0] = m[y][x];
					label[1] = m[y][x + 1];
					if (isMap(y, x + 2)) {
						addPortal(label, y, x + 2);
						if (label != "AA" && label != "ZZ")
							m[y][x + 2] = '@';
					} else {
						addPortal(label, y, x - 1);
						if (label != "AA" && label != "ZZ")
							m[y][x - 1] = '@';
					}
				}
			}
		}

	}
#ifdef DEBUG
	for (string s : m)
		cout << s << endl;
#endif

	checkPortals();
}

const int INF = 1e9;
const vector<pii> offs = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

void solve() {
	assert(start.first > 0 && fini.first > 0);
#ifdef DEBUG
	cout << "start.f=" << start.first << " start.s=" << start.second << endl;
	cout << "fini.f=" << fini.first << " fini.s=" << fini.second << endl;
#endif

	/** Simple dijkstra */
	vvi dp(m.size(), vi(m[0].size(), INF));
	queue<pii> q;
	q.push(start);
	dp[start.first][start.second] = 0;
	while (q.size()) {
		pii p = q.front();
		//cout << "next p, y=" << p.first << " x=" << p.second << endl;
		q.pop();
		for (pii o : offs) {
			int ndp = dp[p.first][p.second] + 1;
			pii np = { p.first + o.first, p.second + o.second };
			if (m[np.first][np.second] == '.' || m[np.first][np.second] == '@') {
				if (m[np.first][np.second] == '@') {
					if (warps.find(np) == warps.end()) {
						cout << "assert fail, np=" << np.first << "," << np.second << endl;
					}
					assert(warps.find(np) != warps.end());
					np = warps[np];
					ndp++;
				}
				if (dp[np.first][np.second] > ndp) {
					dp[np.first][np.second] = ndp;
					q.push(np);
				}
			}
		}
	}
	cout << "star1 " << dp[fini.first][fini.second] << endl;
}

struct pt {
	int l, y, x;
};

/* by level reverse */
bool operator<(const pt &p1, const pt p2) {
	return p2.l < p1.l;
}

void solve2() {
	assert(start.first > 0 && fini.first > 0);
#ifdef DEBUG
	cout << "start.f=" << start.first << " start.s=" << start.second << endl;
	cout << "fini.f=" << fini.first << " fini.s=" << fini.second << endl;
#endif

	pt start2 = { 0, start.first, start.second };
	/** Simple dijkstra */
	vvvi dp(portals.size()+2, vvi(m.size(), vi(m[0].size(), INF)));
	priority_queue<pt> q;
	q.push(start2);
	dp[start2.l][start2.y][start2.x] = 0;

	while (q.size()) {
		pt p = q.top();
/* NOTE we would better use any portal only once per direction, but for that we would
 * have to do DFS instead of BFS, or add the "used portals" list to the current state.
 * Both would not be nice, so we simply restrict the max level to number of portals,
 * which will include the solution and make the algo finite. */
		if (p.l > portals.size())
			break;
#ifdef DEBUG
		cout << "next p, level=" << p.l << " y=" << p.y << " x=" << p.x << endl;
#endif
		q.pop();
		for (pii o : offs) {
			int ndp = dp[p.l][p.y][p.x] + 1;
			pt np = { p.l, p.y + o.first, p.x + o.second };
			if (m[np.y][np.x] == '.' || m[np.y][np.x] == '@') {
				if (m[np.y][np.x] == '@') {
					if (warps.find( { np.y, np.x }) == warps.end()) {
						cout << "assert fail, np=" << np.y << "," << np.x << endl;
					}
					assert(warps.find( { np.y, np.x }) != warps.end());

					np.l -= leveloffs[ { np.y, np.x }];
					pii pw = warps[ { np.y, np.x }];
					np.y = pw.first;
					np.x = pw.second;
					ndp++;
				}
				if (np.l >= 0) {
					if (dp[np.l][np.y][np.x] > ndp) {
						dp[np.l][np.y][np.x] = ndp;
						q.push(np);
						if (np.l == 0 && fini == make_pair(np.y, np.x)) {
#ifdef DEBUG
							cout << "next ans2=" << ndp << endl;
#endif
						}
					}
				}
			}
		}
	}
	cout << "star2 " << dp[0][fini.first][fini.second] << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	try {
		parse();
		solve();
		solve2();
	} catch (exception &e) {
		cout << "EXC exception, what=" << e.what() << endl;
	} catch (const string &e) {
		cout << "EXC string=" << e << endl;
	} catch (const char *e) {
		cout << "EXC *char=" << e << endl;
	} catch (...) {
		cout << "EXC unknown" << endl;
	}
}

