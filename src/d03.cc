/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int atol(string s) {
    istringstream iss(s);
    int a;
    iss>>a;
    return a;
}

void solve() {
    string s1,s2;
    getline(cin,s1);
    getline(cin,s2);

    set<pair<pii,int>> p1;
    set<pii> p2;
    pii pos= {0,0};

    vector<char> c1;
    istringstream iss(s1);
    int steps=0;
    while(true) {
        string ss;
        iss>>ss;
// cout<<ss<<endl;
        int a=atol(ss.substr(1,ss.size()-1));

        for(int i=0; i<a; i++) {

            if(ss[0]=='R') {
                pos.first++;
            } else if(ss[0]=='L') {
                pos.first--;
            } else if(ss[0]=='U') {
                pos.second--;
            } else if(ss[0]=='D')
                pos.second++;
            else
                assert(false);

            steps++;
            p1.insert({pos,steps});
        }

        if(!iss.good())
            break;
    }

    int midist=1e9;
    pii mip= {0,0};
    pos= {0,0};
    vector<char> c2;
    iss=istringstream(s2);
    steps=0;
    while(true) {
        string ss;
        iss>>ss;
        cout<<ss<<endl;
        int a=atol(ss.substr(1,ss.size()-1));

        for(int i=0; i<a; i++) {
            steps++;
            if(ss[0]=='R') {
                pos.first++;
            } else if(ss[0]=='L') {
                pos.first--;
            } else if(ss[0]=='U') {
                pos.second--;
            } else if(ss[0]=='D')
                pos.second++;
            else
                assert(false);

            auto it=p1.upper_bound({pos, -1});
            if(it!=p1.end() && it->first==pos) {
            //if(p1.count(pos)) {
                //int dist=abs(pos.first)+abs(pos.second);
                int dist=steps+it->second;
                if(dist<midist) {
                    midist=dist;
                    mip=pos;
                }
            }
        }

        if(!iss.good())
            break;
    }

    cout<<midist<<" mip.x"<<mip.first<<" mi.y="<<mip.second<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

