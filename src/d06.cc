/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int N=100000;
void solve() {
    string s;
    map<string,int> m;
    int cnt=1;
    vvi tree(N);
    vi par(N);

    while(true) {
        getline(cin,s);
        if(s.size()==0)
            break;

        string s1=s.substr(0,3);
        int p=m[s1];
        if(p==0) {
            p=cnt;
            cnt++;
            m[s1]=p;
        }

        string s2=s.substr(4,3);
        int c=m[s2];
        if(c==0) {
            c=cnt;
            cnt++;
            m[s2]=c;
        }
cout<<s1<<"="<<p<<endl;
cout<<s2<<"="<<c<<endl;

        tree[p].push_back(c);
        par[c]=p;
    }

    int root=-1;
    for(int i=1; i<cnt; i++) {
        if(par[i]==0) {
            root=i;
            break;
        }
    }
    assert(root>0);
    cout<<"root="<<root<<endl;
    cout<<"childs of root= ";
for(int k : tree[root]) 
    cout<<k<<" ";
cout<<endl;

    vi l(N);
    function<int(int,int)> dfs=[&](int node,int level) {
        l[node]=level;
        ll ans=level;
        for(int chi : tree[node]) {
cout<<"dfs node="<<node<<" chi="<<chi<<" level="<<level<<endl;
            int ret=dfs(chi,level+1);
            ans+=ret;
        }
cout<<"dfs node="<<node<<" ans="<<ans<<endl;
        return ans;
    };

    //int ans=dfs(root,0);
    int ans2=dfs(root,0);
    int ans=0;
    for(int i=1; par[i]; i++)
        ans+=l[i];
    
    cout<<"ans2="<<ans2<<endl;
    cout<<ans<<endl;

    cout<<"YOU="<<m["YOU"]<<endl;
    cout<<"SAN="<<m["SAN"]<<endl;
    int i1=m["YOU"];
    int i2=m["SAN"];
    ans=0;
    while(i1!=i2) {
cout<<"i1="<<i1<<" i2="<<i2<<endl;
        if(l[i1]>l[i2])
            i1=par[i1];
        else 
            i2=par[i2];

        ans++;
        assert(i1>0);
        assert(i2>0);
    }
    cout<<"star2="<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        solve();
}

