/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

ll atol(string &s) {
    istringstream iss(s);
    ll i;
    iss>>i;
    return i;
}

void atoli(string &s, vi &a) {
    istringstream iss(s);
    while(iss.good()) {
        int tmp;
        iss>>tmp;
        a.push_back(tmp);
    }
}

void solve() {
    string s;
    getline(cin,s);
    vi a;
    atoli(s,a);

//cout<<"a[...]={"<<a[0]<<" "<<a[1]<<" "<<a[2]<<" "<<a[3]<<endl;


    function<int(int,int)> lload=[&](int pos, int mode) {
        int ret;
        if(mode==0)
            ret=a[a[pos]];
        else
            ret=a[pos];
//cout<<"load, pos="<<pos<<"mode="<<mode<<" ret="<<ret<<endl;
        return ret;
    };

    int inp=5;
    vi outp;
    int pos=0;

    while(a[pos]!=99) {
//       cout<<"pos="<<pos<<endl;
        int opcode=a[pos]%100;
        int p1=(a[pos]/100)%10;
        int p2=(a[pos]/1000)%10;
        int p3=(a[pos]/10000)%10;
//       cout<<"a[pos]="<<a[pos]<<" a[pos+1]="<<a[pos+1]<<" a[pos+2]="<<a[pos+2]<<" a[pos+3]="<<a[pos+3]<<" opcode="<<opcode<<" p1="<<p1<<" p2="<<p2<<" p3="<<p3<<endl;

        if(opcode==1) {
            a[a[pos+3]]=lload(pos+1, p1)+lload(pos+2, p2);
            pos+=4;
        } else if(opcode==2) {
            a[a[pos+3]]=lload(pos+1, p1)*lload(pos+2, p2);
            pos+=4;
        } else if(opcode==3) {  // input
            a[a[pos+1]]=inp;
            pos+=2;
        } else if(opcode==4) { // output
            cout<<"output: "<<lload(pos+1, p1)<<endl;
            pos+=2;
        } else if(opcode==5) {  // jump if true
            if(lload(pos+1, p1))
                pos=lload(pos+2, p2);
            else
                pos+=3;
        } else if(opcode==6) { // jump if false
            if(!lload(pos+1, p1))
                pos=lload(pos+2, p2);
            else
                pos+=3;
        } else if(opcode==7) {  // less than
            int v=lload(pos+1,p1)<lload(pos+2,p2);
            a[a[pos+3]]=v;
            pos+=4;
        } else if(opcode==8) {  // equals
            int v=lload(pos+1,p1)==lload(pos+2,p2);
            a[a[pos+3]]=v;
            pos+=4;
        } else
            assert(false);
    }


}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

