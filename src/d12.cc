/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void step(vi &pos, vi &vel) {
    for(int i=0; i<4; i++) {
        for(int j=0; j<4; j++) {
            if(i==j)
                continue;

            if(pos[j]>pos[i])
                vel[i]+=1;
            else if(pos[j]<pos[i])
                vel[i]-=1;
        }
    }
    for(int i=0; i<4; i++)
        pos[i]+=vel[i];
}


ll lcm(const ll a, const ll b) {
    return a * (b / __gcd(a, b));
}

void solve2(vvi pos) {
    vll cnt(3);
    for(int i=0; i<3; i++) {
        vi vel(4);
        vi iPos=pos[i];
        vi iVel=vel;
        while(true) {
            step(pos[i], vel);
            cnt[i]++;
            if(iPos==pos[i] && iVel==vel) 
                break;
        }
    }
//cout<<"cnt: "<<cnt[0]<<" "<<cnt[1]<<" "<<cnt[2]<<endl;

    ll ans=lcm(lcm(cnt[0], cnt[1]), cnt[2]);
    cout<<"star2 "<<ans<<endl;
    
}

void solve(vvi pos) {
    vvi vel(3, vi(4));


    for(ll c=0; c<1000; c++) {
        for(int k=0; k<3; k++) {
            step(pos[k], vel[k]);
        }
    }

    ll ans=0;
    for(int i=0; i<4; i++) {
        int v=0;
        int p=0;
        for(int k=0; k<3; k++) {
            v+=abs(vel[k][i]);
            p+=abs(pos[k][i]);
        }
        ans=ans+v*p;
    }

    cout<<"star1 "<<ans<<endl;


}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);

    vvi pos(3, vi(4));
    for(int i=0; i<4; i++)
        cin>>pos[0][i]>>pos[1][i]>>pos[2][i];

    solve(pos);
    solve2(pos);
}

