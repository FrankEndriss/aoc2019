/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    string s;
    getline(cin,s);
    for(int i=0; i<s.size(); i++)
        if(s[i]==',')
            s[i]=' ';

    istringstream iss(s);
    vi a;
    while(true) {
        int n;
        iss>>n;
        a.push_back(n);
        if(!iss.good())
            break;
    }
    vi b=a;

    for(int i=0; i<100; i++) {
        for(int j=0; j<100; j++) {
            a=b;
            a[1]=i;
            a[2]=j;
            int pos=0;
            while(a[pos]!=99) {
                if(a[pos]==1) {
                    a[a[pos+3]]=a[a[pos+1]]+a[a[pos+2]];
                } else if(a[pos]==2) {
                    a[a[pos+3]]=a[a[pos+1]]*a[a[pos+2]];
                } else
                    assert(false);
                pos+=4;
            }
            if(a[0]==19690720) {
                cout<<"i="<<i<<" j="<<j<<" ans="<<i*100+j<<endl;
                cout<<a[0]<<endl;
            }
        }

    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve();
}

