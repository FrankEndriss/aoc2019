/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void atoli(string &s, vll &a) {
    istringstream iss(s);
    while(iss.good()) {
        ll tmp;
        iss>>tmp;
        a.push_back(tmp);
    }
}

struct prg {
    vll a;       // memory
    ll pos;    // pc
    ll reloffs;
    map<ll,ll> mem;
};

/* runs proc reading input from qi and writes output to qo.
 * @return true after halt/99 instruction, else
 * false (if needed input is not available).
 */
bool solve2(prg &proc, queue<ll> &qi, queue<ll> &qo) {
    vll &a=proc.a;
    ll &pos=proc.pos;

    ll &reloffs=proc.reloffs;

    function<ll(ll,ll)> addr=[&](ll lpos, ll mode) {
        ll ret;
        if(mode==0)
            ret=proc.a[lpos];
        else if(mode==2) {
            ret=proc.a[lpos]+reloffs;
        } else
            assert(false);
        return ret;
    };

    function<ll(ll,ll)> lload=[&](ll lpos, ll mode) {
        ll ret;
        if(mode==0)
            ret=a[proc.a[lpos]];
        else if(mode==2) {
            ret=a[proc.a[lpos]+reloffs];
        } else
            ret=a[lpos];
        return ret;
    };

    while(a[pos]!=99) {
        ll opcode=a[pos]%100;
        ll p1=(a[pos]/100)%10;
        ll p2=(a[pos]/1000)%10;
        ll p3=(a[pos]/10000)%10;
        //assert(p3==0);
        //p3=1;
//cout<<"a[pos]="<<a[pos]<<" a[pos+1]="<<a[pos+1]<<" a[pos+2]="<<a[pos+2]<<" a[pos+3]="<<a[pos+3]<<" opcode="<<opcode<<" p1="<<p1<<" p2="<<p2<<" p3="<<p3<<endl;

        if(opcode==1) {
            ll sto=addr(pos+3, p3);
            a[sto]=lload(pos+1, p1)+lload(pos+2, p2);
            pos+=4;
        } else if(opcode==2) {
            ll sto=addr(pos+3, p3);
            a[sto]=lload(pos+1, p1)*lload(pos+2, p2);
            pos+=4;
        } else if(opcode==3) {  // input
            if(qi.size()==0) {
//cout<<"input size==0, abort"<<endl;
                return false;
            }

            ll tmp=qi.front();
            qi.pop();
            assert(tmp<=INT_MAX && tmp>=INT_MIN);
            ll sto=addr(pos+1, p1);
            a[sto]=(ll)tmp;
            pos+=2;
        } else if(opcode==4) { // output
            ll outp=lload(pos+1, p1);
            qo.push(outp);
//cout<<"outp: "<<outp<<endl;
            pos+=2;
        } else if(opcode==5) {  // jump if true
            if(lload(pos+1, p1))
                pos=lload(pos+2, p2);
            else
                pos+=3;
        } else if(opcode==6) { // jump if false
            if(!lload(pos+1, p1))
                pos=lload(pos+2, p2);
            else
                pos+=3;
        } else if(opcode==7) {  // less than
            ll v=lload(pos+1,p1)<lload(pos+2,p2);
            ll sto=addr(pos+3, p3);
            a[sto]=v;
            pos+=4;
        } else if(opcode==8) {  // equals
            ll v=lload(pos+1,p1)==lload(pos+2,p2);
            ll sto=addr(pos+3, p3);
            a[sto]=v;
            pos+=4;
        } else if(opcode==9) {
            ll v=lload(pos+1,p1);
            reloffs+=v;
            pos+=2;
        } else {
            cout<<"unknown opcode: "<<opcode<<endl;
            assert(false);
        }
    }

    return true;

}

void clearQ(queue<ll> &q) {
    while(q.size())
        q.pop();
}

void solve() {
    string s;
    getline(cin,s);
    vll a;
    atoli(s,a);
    for(int i=0; i<10; i++)
        a.push_back(-1);

    ll ma=0;
    vll p= { 5, 6, 7, 8, 9 };
    vector<queue<ll>> iq(5);

    do {
        vector<prg> proc(5);

        for(int i=0; i<5; i++) {
            proc[i].a=a;
            proc[i].pos=0;
            clearQ(iq[i]);
            iq[i].push(p[i]);   // first input for proc
        }

        vb fini(5);

        ll ans=0;
        iq[0].push(0);  // initial input

        while(!fini[4]) {
            for(int i=0; i<5; i++) {
                if(fini[i])
                    continue;

                if(iq[i].size()) {
                    fini[i]=solve2(proc[i], iq[i], iq[(i+1)%5]);
                    if(i==4) {
                        ll tmp=iq[0].back();    // last output from proc[4]
                        ans=max(ans,tmp);
#ifdef DEBUG
//cout<<"ans="<<ans<<endl;
#endif
                    }
                }
            }
        }
        ma=max(ma, ans);

    } while(next_permutation(p.begin(), p.end()));
    cout<<"ma="<<ma<<endl;

/* ans to star2 */
    assert(ma==35993240);
}

void solve3() {
    string s;
    getline(cin,s);
    vll a;
    atoli(s,a);
    a.resize(1000000);

    queue<ll> iq;
    queue<ll> oq;
    prg proc;
    proc.a=a;
    proc.pos=0;
    proc.reloffs=0;
    iq.push(2);
    solve2(proc, iq, oq);
    while(oq.size()) {
        ll o=oq.front();
        oq.pop();
        cout<<"oq="<<o<<endl;
    }
}

void solve4() {
    string s;
    getline(cin,s);
    vll a;
    atoli(s,a);
    a.resize(1000000);

    queue<ll> iq;
    queue<ll> oq;

    prg proc;
    proc.a=a;
    proc.pos=0;
    proc.reloffs=0;

    pii pos={0,0};
    char dir='^';

    map<pii,int> m; // m[i]==0 panel is black; 1==white
    set<pii> painted;   // painted panels
    m[pos]=1;

    while(true) {
        iq.push(m[pos]);
        bool fini=solve2(proc, iq, oq);
        if(oq.size()) {
            int col=oq.front();
            oq.pop();
            int lr=oq.front();
            oq.pop();

            m[pos]=col;
            painted.insert(pos);
/* NOTE better use a map here
 * map<char, vector<char>> dm
 * dm['^']={ '<', '>' };
 * dm['<']={ 'v', '^' };
 * ...
 */
            if(dir=='^') {
                if(lr==0)
                    dir='<';
                else
                    dir='>';
            } else if(dir=='<') {
                if(lr==0)
                    dir='v';
                else
                    dir='^';
            } else if(dir=='v') {
                if(lr==0)
                    dir='>';
                else
                    dir='<';
            } else if(dir=='>') {
                if(lr==0)
                    dir='^';
                else
                    dir='v';
            }

/* NOTE better us map to offsets here
 * map<char,pii> doffs;
 * doffs['^']={ -1,  0 };
 * doffs['<']={ 0 , -1 };
 * ...
 * //pos+=doffs[dir]; 
 * pos={pos.first+doffs[dir].first, pos.second+doffs[dir].second};
 */
            if(dir=='^')
                pos={pos.first-1,pos.second};
            else if(dir=='<')
                pos={pos.first,pos.second-1};
            else if(dir=='v')
                pos={pos.first+1,pos.second};
            else if(dir=='>')
                pos={pos.first,pos.second+1};
        }
        
        if(fini)
            break;
    }
    cout<<"star1= "<<painted.size()<<endl;

    const int INF=1e9;
    int miF=INF;
    int miS=INF;
    vector<pii> p2; // painted as vector
    for(pii p : painted) {
        miF=min(miF,p.first);
        miS=min(miS,p.second);
        p2.push_back(p);
    }

    int maF=-INF;
    int maS=-INF;
    for(int i=0;i<p2.size(); i++) {
        p2[i].first-=miF;
        p2[i].second-=miS;
        maF=max(maF,p2[i].first);
        maS=max(maS,p2[i].second);
    }

    vector<string> ms(maF+1, string(maS+1, ' '));
    for(int i=0;i<p2.size(); i++) {
        pii po={ p2[i].first+miF, p2[i].second+miS };
        if(m[po])
            ms[p2[i].first][p2[i].second]='#';
    }

    cout<<"star2="<<endl;
    for(string s : ms)
        cout<<s<<endl;
}


int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve4();
}

