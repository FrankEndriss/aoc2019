pub fn ii32(inp: &mut SplitAsciiWhitespace) -> i32 {
    inp.next().unwrap().parse().unwrap()
}
pub fn iusize(inp: &mut SplitAsciiWhitespace) -> usize {
    inp.next().unwrap().parse().unwrap()
}
pub fn iu128(inp: &mut SplitAsciiWhitespace) -> u128 {
    inp.next().unwrap().parse().unwrap()
}
pub fn ii64(inp: &mut SplitAsciiWhitespace) -> i64 {
    inp.next().unwrap().parse().unwrap()
}
pub fn if64(inp: &mut SplitAsciiWhitespace) -> f64 {
    inp.next().unwrap().parse().unwrap()
}

/*
*/
pub fn solve(inp: &mut SplitAsciiWhitespace) {
    let ans_str = "";
    let mut ans=0usize;
    while let Some(line) = inp.next() {
        let b=line.as_bytes();
        let mut inbrace=false;
        let mut aba=VecDeque::<u8>::new();
        let mut in_list=HashSet::<VecDeque<u8>>::new();
        let mut out_list=HashSet::<VecDeque<u8>>::new();

        for &c in b.iter() {
            if c==b'[' {
                inbrace=true;
                aba.clear();
            } else if c==b']' {
                inbrace=false;
                aba.clear();
            } else {
                aba.push_back(c);
            }

            if aba.len()==3 && aba[0]==aba[2] && aba[0]!=aba[1] {
                if inbrace {
                    in_list.insert(aba.clone());
                } else {
                    out_list.insert(aba.clone());
                }
            }

            while aba.len()>2 {
                aba.pop_front();
            }
        }
        let mut ok=false;
        for vin in in_list.iter() {
            for vout in out_list.iter() {
                if vin[0]==vout[1] && vin[1]==vout[0] {
                    ok=true;
                }
                
            }
        }
        if ok {
            ans+=1;
        }
    }

    println!("{}", ans);
}

extern crate md5;

// dont warn about the imports
#[allow(unused_imports)]
use {
    md5::{Digest, Md5},
    std::{
        cmp::Ordering,
        collections::{
            hash_map::Entry, BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque,
        },
        f64::consts::PI,
        io::{self, stdout, Read, Split, Write},
        net::{Ipv4Addr, Ipv6Addr},
        ops::Add,
        str::SplitAsciiWhitespace,
        time::{SystemTime, UNIX_EPOCH},
    },
};

fn main() {
    let mut buf = String::with_capacity(1024 * 1024);
    io::stdin().read_to_string(&mut buf).unwrap();
    let mut inp = buf.split_ascii_whitespace();
    let t: usize = 1;
    //let t: usize = inp.next().unwrap().parse().unwrap();
    (0..t).for_each(|_| solve(&mut inp));
}
