pub fn ii32(inp: &mut SplitAsciiWhitespace) -> i32 {
    inp.next().unwrap().parse().unwrap()
}
pub fn iusize(inp: &mut SplitAsciiWhitespace) -> usize {
    inp.next().unwrap().parse().unwrap()
}
pub fn iu128(inp: &mut SplitAsciiWhitespace) -> u128 {
    inp.next().unwrap().parse().unwrap()
}
pub fn ii64(inp: &mut SplitAsciiWhitespace) -> i64 {
    inp.next().unwrap().parse().unwrap()
}
pub fn if64(inp: &mut SplitAsciiWhitespace) -> f64 {
    inp.next().unwrap().parse().unwrap()
}

/*
*/
pub fn solve(inp: &mut SplitAsciiWhitespace) {
    let ans_str = "";
    let mut ans=0usize;
    while let Some(line) = inp.next() {
        let b=line.as_bytes();
        let mut inbrace=false;
        let mut prev=VecDeque::<u8>::new();
        let mut aba=VecDeque::<u8>::new();
        let mut bab=VecDeque::<u8>::new();
        let mut ansin=false;
        let mut ansout=false;
        for &c in b.iter() {
            if c==b'[' {
                inbrace=true;
                prev.clear();
                aba.clear();
                bab.clear();
            } else if c==b']' {
                inbrace=false;
                prev.clear();
                aba.clear();
                bab.clear();
            } else {
                prev.push_back(c);
            }

            if prev.len()==4 && prev[0]==prev[3] && prev[1]==prev[2] && prev[0]!=prev[1] {
                if inbrace {
                    ansin=true;
                } else {
                    ansout=true;    
                }
            }
            while prev.len()>3 {
                prev.pop_front();
            }
        }
        if ansout && !ansin {
            ans+=1;
        }
    }

    println!("{}", ans);
}

extern crate md5;

// dont warn about the imports
#[allow(unused_imports)]
use {
    md5::{Digest, Md5},
    std::{
        cmp::Ordering,
        collections::{
            hash_map::Entry, BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque,
        },
        f64::consts::PI,
        io::{self, stdout, Read, Split, Write},
        net::{Ipv4Addr, Ipv6Addr},
        ops::Add,
        str::SplitAsciiWhitespace,
        time::{SystemTime, UNIX_EPOCH},
    },
};

fn main() {
    let mut buf = String::with_capacity(1024 * 1024);
    io::stdin().read_to_string(&mut buf).unwrap();
    let mut inp = buf.split_ascii_whitespace();
    let t: usize = 1;
    //let t: usize = inp.next().unwrap().parse().unwrap();
    (0..t).for_each(|_| solve(&mut inp));
}
