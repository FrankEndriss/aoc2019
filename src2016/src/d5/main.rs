pub fn ii32(inp: &mut SplitAsciiWhitespace) -> i32 {
    inp.next().unwrap().parse().unwrap()
}
pub fn iusize(inp: &mut SplitAsciiWhitespace) -> usize {
    inp.next().unwrap().parse().unwrap()
}
pub fn iu128(inp: &mut SplitAsciiWhitespace) -> u128 {
    inp.next().unwrap().parse().unwrap()
}
pub fn ii64(inp: &mut SplitAsciiWhitespace) -> i64 {
    inp.next().unwrap().parse().unwrap()
}
pub fn if64(inp: &mut SplitAsciiWhitespace) -> f64 {
    inp.next().unwrap().parse().unwrap()
}

extern crate md5;
use md5::{Digest, Md5};

/*
*/
pub fn solve(inp: &mut SplitAsciiWhitespace) {
    let mut ans="".to_string();
    let str="wtnhxymk";
    for i in 1.. {
        let s=format!("{}{}",str,i);
        let mut hasher = Md5::new();
        hasher.update(s.as_bytes());
        let result = hasher.finalize();
        let hex_result = format!("{:x}", result);
        if hex_result.starts_with("00000") {
            ans+=format!("{}",hex_result.chars().nth(5).unwrap()).as_str();
            eprintln!("ans={}",ans);
            if ans.len()>=8 {
                break;
            }
        }
    }
    println!("{}",ans);
}

// dont warn about the imports
#[allow(unused_imports)]
use std::{
    cmp::Ordering,
    collections::{hash_map::Entry, BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque},
    f64::consts::PI,
    io::{self, stdout, Read, Split, Write},
    net::{Ipv4Addr, Ipv6Addr},
    ops::Add,
    str::SplitAsciiWhitespace,
    time::{SystemTime, UNIX_EPOCH},
};

fn main() {
    let mut buf = String::with_capacity(1024 * 1024);
    io::stdin().read_to_string(&mut buf).unwrap();
    let mut inp = buf.split_ascii_whitespace();
    let t: usize = 1;
    //let t: usize = inp.next().unwrap().parse().unwrap();
    (0..t).for_each(|_| solve(&mut inp));
}
