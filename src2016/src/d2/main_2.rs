pub fn ii32(inp: &mut SplitAsciiWhitespace) -> i32 {
    inp.next().unwrap().parse().unwrap()
}
pub fn iusize(inp: &mut SplitAsciiWhitespace) -> usize {
    inp.next().unwrap().parse().unwrap()
}
pub fn iu128(inp: &mut SplitAsciiWhitespace) -> u128 {
    inp.next().unwrap().parse().unwrap()
}
pub fn ii64(inp: &mut SplitAsciiWhitespace) -> i64 {
    inp.next().unwrap().parse().unwrap()
}
pub fn if64(inp: &mut SplitAsciiWhitespace) -> f64 {
    inp.next().unwrap().parse().unwrap()
}

/*
*/
pub fn solve(inp: &mut SplitAsciiWhitespace) {
    let mut x=0i32;
    let mut y=2i32;
    let val=[ 
        [b' ', b' ', b'5', b' ', b' '],
        [b' ', b'2', b'6', b'A', b' '],
        [b'1', b'3', b'7', b'B', b'D'],
        [b' ', b'4', b'8', b'C', b' '],
        [b' ', b' ', b'9', b' ', b' '],
    ];
    let mut ans=String::new();
    while let Some(line)=inp.next() {
        for b in line.bytes() {
            if b==b'U' && y>=1 {
                y-=1;
                if val[x as usize][y as usize]==b' ' {
                    y+=1;
                }
            } else if b==b'R' && x<4 {
                x+=1;
                if val[x as usize][y as usize]==b' ' {
                    x-=1;
                }
            } else if b==b'D' && y<4 {
                y+=1;
                if val[x as usize][y as usize]==b' ' {
                    y-=1;
                }
            } else if b==b'L' && x>=1 {
                x-=1;
                if val[x as usize][y as usize]==b' ' {
                    x+=1;
                }
            }
        }
        let v=vec![val[x as usize][y as usize]];
        ans+= format!("{}", String::from_utf8(v).unwrap()).as_str(); 
    }
    println!("{}",ans);
}

// dont warn about the imports
#[allow(unused_imports)]
use std::{
    cmp::Ordering,
    collections::{hash_map::Entry, BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque},
    f64::consts::PI,
    io::{self, stdout, Read, Split, Write},
    net::{Ipv4Addr, Ipv6Addr},
    ops::Add,
    str::SplitAsciiWhitespace,
    time::{SystemTime, UNIX_EPOCH},
};

fn main() {
    let mut buf = String::with_capacity(1024 * 1024);
    io::stdin().read_to_string(&mut buf).unwrap();
    let mut inp = buf.split_ascii_whitespace();
    let t: usize = 1;
    //let t: usize = inp.next().unwrap().parse().unwrap();
    (0..t).for_each(|_| solve(&mut inp));
}
