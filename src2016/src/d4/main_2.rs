pub fn ii32(inp: &mut SplitAsciiWhitespace) -> i32 {
    inp.next().unwrap().parse().unwrap()
}
pub fn iusize(inp: &mut SplitAsciiWhitespace) -> usize {
    inp.next().unwrap().parse().unwrap()
}
pub fn iu128(inp: &mut SplitAsciiWhitespace) -> u128 {
    inp.next().unwrap().parse().unwrap()
}
pub fn ii64(inp: &mut SplitAsciiWhitespace) -> i64 {
    inp.next().unwrap().parse().unwrap()
}
pub fn if64(inp: &mut SplitAsciiWhitespace) -> f64 {
    inp.next().unwrap().parse().unwrap()
}

/*
*/
pub fn solve(inp: &mut SplitAsciiWhitespace) {
    let mut ans=0usize;
    while let Some(line_str) = inp.next() {
        //eprintln!("line: {}",line_str);
        let mut line=line_str.as_bytes().iter().map(|x| *x).collect::<Vec<_>>();
        for i in 0..line.len() {
            if line[i]==b'-' || line[i]==b'[' || line[i]==b']' {
                line[i]=b' ';
            }
        }
        let word_str=String::from_utf8(line).unwrap();
        let mut words=word_str.split_ascii_whitespace();

        let mut v=vec!["".to_string();0];

        let mut sec=0usize;
        while let Some(word)=words.next() {
            //eprintln!("WORD: {}",word);
            let b=word.as_bytes()[0];
            if b>=b'0' && b<=b'9' {
                sec=word.parse::<usize>().unwrap();
                break;
            } else {
                v.push(word.to_string());
            }
        }
        for i in 0..v.len() {
            let mut b:Vec<_>=v[i].as_bytes().iter().map(|x| *x).collect();
            for j in 0..b.len() {
                b[j]=(((b[j]-b'a') as usize +sec)%26) as u8+b'a';
            }
            v[i]=String::from_utf8(b).unwrap();
        }
        if v.len()==3 && v[0]=="northpole" && v[1]=="object" && v[2]=="storage" {
            println!("ans2: {}", sec);
            return;
        } else {
            for i in 0..v.len() {
                print!("{} ", v[i]);
            }
            println!();
        }
    }
    println!("{}",ans);
}

// dont warn about the imports
#[allow(unused_imports)]
use std::{
    cmp::Ordering,
    collections::{hash_map::Entry, BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque},
    f64::consts::PI,
    io::{self, stdout, Read, Split, Write},
    net::{Ipv4Addr, Ipv6Addr},
    ops::Add,
    str::SplitAsciiWhitespace,
    time::{SystemTime, UNIX_EPOCH},
};

fn main() {
    let mut buf = String::with_capacity(1024 * 1024);
    io::stdin().read_to_string(&mut buf).unwrap();
    let mut inp = buf.split_ascii_whitespace();
    let t: usize = 1;
    //let t: usize = inp.next().unwrap().parse().unwrap();
    (0..t).for_each(|_| solve(&mut inp));
}
