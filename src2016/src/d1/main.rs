pub fn ii32(inp: &mut SplitAsciiWhitespace) -> i32 {
    inp.next().unwrap().parse().unwrap()
}
pub fn iusize(inp: &mut SplitAsciiWhitespace) -> usize {
    inp.next().unwrap().parse().unwrap()
}
pub fn iu128(inp: &mut SplitAsciiWhitespace) -> u128 {
    inp.next().unwrap().parse().unwrap()
}
pub fn ii64(inp: &mut SplitAsciiWhitespace) -> i64 {
    inp.next().unwrap().parse().unwrap()
}
pub fn if64(inp: &mut SplitAsciiWhitespace) -> f64 {
    inp.next().unwrap().parse().unwrap()
}

/*
*/
pub fn solve(inp: &mut SplitAsciiWhitespace) {
    let mut dir = 0; // North=0, East=1, South=2, West=3
    let mut y = 0;
    let mut x = 0;
    let mut hs = HashSet::<(i32, i32)>::new();
    hs.insert((x, y));
    while let Some(tok) = inp.next() {
        let b = tok.as_bytes();
        if b[0] == b'L' {
            dir = (dir + 4 - 1) % 4;
        } else if b[0] == b'R' {
            dir = (dir + 1) % 4;
        }

        let mut num = 0i32;
        for i in 1..b.len() {
            if b[i] == b',' {
                break;
            }
            num *= 10;
            num = num + (b[i] - b'0') as i32;
        }
        for _ in 0..num {
            if dir == 0 {
                y -= 1;
            } else if dir == 1 {
                x += 1;
            } else if dir == 2 {
                y += 1;
            } else if dir == 3 {
                x -= 1;
            }
            if hs.contains(&(x, y)) {
                let ans2 = (x.abs() + y.abs()) as i32;
                println!("ans2: {}", ans2);
                return;
            }
            hs.insert((x, y));
        }
    }
    let ans = (x.abs() + y.abs()) as i32;
    println!("{}", ans);
}

// dont warn about the imports
#[allow(unused_imports)]
use std::{
    cmp::Ordering,
    collections::{hash_map::Entry, BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque},
    f64::consts::PI,
    io::{self, stdout, Read, Split, Write},
    net::{Ipv4Addr, Ipv6Addr},
    ops::Add,
    str::SplitAsciiWhitespace,
    time::{SystemTime, UNIX_EPOCH},
};

fn main() {
    let mut buf = String::with_capacity(1024 * 1024);
    io::stdin().read_to_string(&mut buf).unwrap();
    let mut inp = buf.split_ascii_whitespace();
    let t: usize = 1;
    //let t: usize = inp.next().unwrap().parse().unwrap();
    (0..t).for_each(|_| solve(&mut inp));
}
