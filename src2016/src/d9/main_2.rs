pub fn ii32(inp: &mut SplitAsciiWhitespace) -> i32 {
    inp.next().unwrap().parse().unwrap()
}
pub fn iusize(inp: &mut SplitAsciiWhitespace) -> usize {
    inp.next().unwrap().parse().unwrap()
}
pub fn iu128(inp: &mut SplitAsciiWhitespace) -> u128 {
    inp.next().unwrap().parse().unwrap()
}
pub fn ii64(inp: &mut SplitAsciiWhitespace) -> i64 {
    inp.next().unwrap().parse().unwrap()
}
pub fn if64(inp: &mut SplitAsciiWhitespace) -> f64 {
    inp.next().unwrap().parse().unwrap()
}

/*
For Day9 part2 we need to somehow recurse...
first understand the problem statement.
-> Ok, on a '(' we parse until ')' and recurse call
  with the copied content of length len.
  return the decompressed length, and multiply it by mul on the caller side.
*/
pub fn go(line: &Vec<u8>) -> usize {
        let mut pos = 0usize;
        let mut ans=0usize;
        while pos < line.len() {
            if line[pos] == b'(' {
                // parse len and mul
                pos += 1;

                let mut len = 0usize;
                while line[pos].is_ascii_digit() {
                    len *= 10;
                    len += (line[pos] - b'0') as usize;
                    pos += 1;
                }
                assert!(line[pos] == b'x');
                pos += 1;
                let mut mul = 0usize;
                while line[pos].is_ascii_digit() {
                    mul *= 10;
                    mul += (line[pos] - b'0') as usize;
                    pos += 1;
                }
                assert!(line[pos] == b')');
                pos += 1;

                let mut v0=vec![0u8;0];
                for _ in 0..len {
                    v0.push(line[pos]);
                    pos+=1;
                }

                ans += go(&v0) * mul;
                //eprintln!("len={}, mul={}", len, mul);

            } else {
                ans += 1;
                pos += 1;
            }
        }
        ans
    }

pub fn solve(inp: &mut SplitAsciiWhitespace) {

    let line: Vec<_> = inp.next().unwrap().as_bytes().iter().map(|x| *x).collect();

    let ans = go(&line);

    println!("star2: {}", ans);
}

extern crate md5;

// dont warn about the imports
#[allow(unused_imports)]
use {
    md5::{Digest, Md5},
    std::{
        cmp::Ordering,
        collections::{
            hash_map::Entry, BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque,
        },
        f64::consts::PI,
        io::{self, stdout, Read, Split, Write},
        net::{Ipv4Addr, Ipv6Addr},
        ops::Add,
        str::SplitAsciiWhitespace,
        time::{SystemTime, UNIX_EPOCH},
    },
};

fn main() {
    let mut buf = String::with_capacity(1024 * 1024);
    io::stdin().read_to_string(&mut buf).unwrap();
    let mut inp = buf.split_ascii_whitespace();
    let t: usize = 1;
    //let t: usize = inp.next().unwrap().parse().unwrap();
    (0..t).for_each(|_| solve(&mut inp));
}
