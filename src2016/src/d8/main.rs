pub fn ii32(inp: &mut SplitAsciiWhitespace) -> i32 {
    inp.next().unwrap().parse().unwrap()
}
pub fn iusize(inp: &mut SplitAsciiWhitespace) -> usize {
    inp.next().unwrap().parse().unwrap()
}
pub fn iu128(inp: &mut SplitAsciiWhitespace) -> u128 {
    inp.next().unwrap().parse().unwrap()
}
pub fn ii64(inp: &mut SplitAsciiWhitespace) -> i64 {
    inp.next().unwrap().parse().unwrap()
}
pub fn if64(inp: &mut SplitAsciiWhitespace) -> f64 {
    inp.next().unwrap().parse().unwrap()
}

/*
*/
pub fn solve(inp: &mut SplitAsciiWhitespace) {
    let mut ans=vec![vec![b' ';50];6];  // [h][w]

    while let Some(word) = inp.next() {
        if word=="rect" {
            let sz=inp.next().unwrap().split_once('x').unwrap();
            let x=sz.0.parse::<usize>().unwrap();
            let y=sz.1.parse::<usize>().unwrap();

            eprintln!("rect x={x} y={y}");

            for i in 0..y {
                for j in 0..x {
                    ans[i][j]=b'#';
                }
            }
        } else if word=="rotate" {
            let dir=inp.next().unwrap();
            if dir=="column" {
                let col=inp.next().unwrap().split_once("=").unwrap().1.parse::<usize>().unwrap();
                inp.next().unwrap();
                let cnt=inp.next().unwrap().parse::<usize>().unwrap();

                for i in 0..cnt {
                    let mut aux=ans[5][col];
                    for j in 0..6 {
                        let tmp=ans[j][col];
                        ans[j][col]=aux;
                        aux=tmp;
                    }
                }

            } else if dir=="row" {
                let row=inp.next().unwrap().split_once("=").unwrap().1.parse::<usize>().unwrap();
                inp.next().unwrap();
                let cnt=inp.next().unwrap().parse::<usize>().unwrap();

                for j in 0..cnt {
                    let mut aux=ans[row][49];
                    for i in 0..50 {
                        let tmp=ans[row][i];
                        ans[row][i]=aux;
                        aux=tmp;
                    }
                }
            } else {
                panic!("bad dir: {}", dir);
            }
        }
    }

    let mut cnt=0usize;
        for i in 0..6usize {
            println!("{}", String::from_utf8(ans[i].clone()).unwrap());
            for j in 0..50usize {
                if ans[i][j]==b'#' {
                    cnt+=1;
                }
            }
        }
        println!("star1: {}", cnt);
}

extern crate md5;

// dont warn about the imports
#[allow(unused_imports)]
use {
    md5::{Digest, Md5},
    std::{
        cmp::Ordering,
        collections::{
            hash_map::Entry, BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque,
        },
        f64::consts::PI,
        io::{self, stdout, Read, Split, Write},
        net::{Ipv4Addr, Ipv6Addr},
        ops::Add,
        str::SplitAsciiWhitespace,
        time::{SystemTime, UNIX_EPOCH},
    },
};

fn main() {
    let mut buf = String::with_capacity(1024 * 1024);
    io::stdin().read_to_string(&mut buf).unwrap();
    let mut inp = buf.split_ascii_whitespace();
    let t: usize = 1;
    //let t: usize = inp.next().unwrap().parse().unwrap();
    (0..t).for_each(|_| solve(&mut inp));
}
