pub fn ii32(inp: &mut SplitAsciiWhitespace) -> i32 {
    inp.next().unwrap().parse().unwrap()
}
pub fn iusize(inp: &mut SplitAsciiWhitespace) -> usize {
    inp.next().unwrap().parse().unwrap()
}
pub fn iu128(inp: &mut SplitAsciiWhitespace) -> u128 {
    inp.next().unwrap().parse().unwrap()
}
pub fn ii64(inp: &mut SplitAsciiWhitespace) -> i64 {
    inp.next().unwrap().parse().unwrap()
}
pub fn if64(inp: &mut SplitAsciiWhitespace) -> f64 {
    inp.next().unwrap().parse().unwrap()
}

extern crate md5;
use md5::{Digest, Md5};

/*
*/
pub fn solve(inp: &mut SplitAsciiWhitespace) {
    let mut f=vec![vec![0usize;26];8];
    while let Some(line) = inp.next() {
        let b=line.as_bytes();
        assert!(b.len()==8);
        for i in 0..8 {
            f[i][(b[i]-b'a') as usize]+=1;
        }
    }

    let mut ans_str=String::new();
    for i in 0..8 {
        let mut ans=0usize;
        let mut cnt=30000usize;
        for j in 0..26 {
            if f[i][j]<cnt {
                ans=j;
                cnt=f[i][j];
            }
        }
        ans_str.push((b'a'+ans as u8) as char);
    }
    println!("{}", ans_str);

}

// dont warn about the imports
#[allow(unused_imports)]
use std::{
    cmp::Ordering,
    collections::{hash_map::Entry, BTreeMap, BTreeSet, BinaryHeap, HashMap, HashSet, VecDeque},
    f64::consts::PI,
    io::{self, stdout, Read, Split, Write},
    net::{Ipv4Addr, Ipv6Addr},
    ops::Add,
    str::SplitAsciiWhitespace,
    time::{SystemTime, UNIX_EPOCH},
};

fn main() {
    let mut buf = String::with_capacity(1024 * 1024);
    io::stdin().read_to_string(&mut buf).unwrap();
    let mut inp = buf.split_ascii_whitespace();
    let t: usize = 1;
    //let t: usize = inp.next().unwrap().parse().unwrap();
    (0..t).for_each(|_| solve(&mut inp));
}
