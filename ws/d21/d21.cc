/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
template<typename T>
void atoli(string &s, T &m) {
	for (size_t i = 0; i < s.size(); i++)
		if (s[i] != '-' && (s[i] < '0' || s[i] > '9'))
			s[i] = ' ';

	istringstream iss(s);
	ll idx = 0;
	while (iss.good()) {
		ll aux;
		iss >> aux;
		m[idx] = aux;
		idx++;
	}
}

/* Incode computer, see https://adventofcode.com/2019/day/9
 * This implementation works with mapped memory, ie memory
 * is kind of infinite, and can have negative addresses, too.
 */
struct prg {
	ll pc;
	ll reloffs;
	map<ll, ll> a;   // memory

	queue<ll> iq;   // stdin
	queue<ll> oq;   // stdout

	/* Give memory/program as parameter, an arbitrary list of integers. */
	prg(string s) :
			pc(0), reloffs(0) {
		atoli(s, a);
	}

	ll addr(ll lpos, ll mode) {
		ll ret;
		if (mode == 0)
			ret = a[lpos];
		else if (mode == 2)
			ret = a[lpos] + reloffs;
		else
			assert(false);
		return ret;
	}
	;

	ll lload(ll lpos, ll mode) {
		ll ret;
		if (mode == 0)
			ret = a[a[lpos]];
		else if (mode == 2)
			ret = a[a[lpos] + reloffs];
		else
			ret = a[lpos];
		return ret;
	}
	;

	/* runs proc reading input from iq and writes output to oq.
	 * @return true after halt/99 instruction, else
	 * false on waiting for input.
	 */
	bool run() {
		while (a[pc] != 99) {
			ll opcode = a[pc] % 100;
			ll p1 = (a[pc] / 100) % 10;
			ll p2 = (a[pc] / 1000) % 10;
			ll p3 = (a[pc] / 10000) % 10;

#ifdef DEBUG
			cout << "prg: a[pc]=" << a[pc] << " a[pc+1]=" << a[pc + 1] << " a[pc+2]=" << a[pc + 2] << " a[pc+3]=" << a[pc + 3] << " opcode=" << opcode << " p1="
					<< p1 << " p2=" << p2 << " p3=" << p3 << endl;
#endif

			if (opcode == 1) { // add
				ll sto = addr(pc + 3, p3);
				a[sto] = lload(pc + 1, p1) + lload(pc + 2, p2);
				pc += 4;
			} else if (opcode == 2) { // mul
				ll sto = addr(pc + 3, p3);
				a[sto] = lload(pc + 1, p1) * lload(pc + 2, p2);
				pc += 4;
			} else if (opcode == 3) {  // input
				if (iq.size() == 0) {
#ifdef DEBUG
					cout << "prg: input size==0, wait for input return" << endl;
#endif
					return false;
				}

				ll aux = iq.front();
				iq.pop();
				assert(aux<=INT_MAX && aux>=INT_MIN);
				ll sto = addr(pc + 1, p1);
				a[sto] = aux;
				pc += 2;
			} else if (opcode == 4) { // output
				ll outp = lload(pc + 1, p1);
				oq.push(outp);
#ifdef DEBUG
				cout << "prg: outp: " << outp << endl;
#endif
				pc += 2;
			} else if (opcode == 5) {  // jump if true
				if (lload(pc + 1, p1))
					pc = lload(pc + 2, p2);
				else
					pc += 3;
			} else if (opcode == 6) { // jump if false
				if (!lload(pc + 1, p1))
					pc = lload(pc + 2, p2);
				else
					pc += 3;
			} else if (opcode == 7) {  // less than
				ll v = lload(pc + 1, p1) < lload(pc + 2, p2);
				ll sto = addr(pc + 3, p3);
				a[sto] = v;
				pc += 4;
			} else if (opcode == 8) {  // equals
				ll v = lload(pc + 1, p1) == lload(pc + 2, p2);
				ll sto = addr(pc + 3, p3);
				a[sto] = v;
				pc += 4;
			} else if (opcode == 9) {
				ll v = lload(pc + 1, p1);
				reloffs += v;
				pc += 2;
			} else {
				cout << "unknown opcode: " << opcode << endl;
				assert(false);
			}
		}
#ifdef DEBUG
		cout << "prg: halt" << endl;
#endif
		return true;
	}
};

struct cres {
	int code;
	string pic;
};

void skipline(prg &proc) {
	while (proc.oq.size()) {
		int d = proc.oq.front();
		proc.oq.pop();
		if (d == 10)
			break;
	}
}

cres runcode(prg &proc, string code) {
	for (char c : code)
		proc.iq.push(c);
	proc.run();

	if (!proc.oq.size())
		return {-1, ""};
	else {
		skipline(proc);
		skipline(proc);
		skipline(proc);
		skipline(proc);
		int d = proc.oq.front();
		proc.oq.pop();
		if (d > 255)
			return {d, ""};
		else {
			string ans = "";
			ans += (char) d;
			while (proc.oq.size()) {
				d = proc.oq.front();
				proc.oq.pop();
				ans += (char) d;
			}
			return {0, ans};
		}
	}

}

string code;

void addc(string line) {
	code += line;
	code += (char) 10;
}

/** Jumps:
 * #####.#....######
 * #####..#....######
 * #####...#....######
 * #####....#....######
 *
 * #####.##....######
 */
void solve(prg proc) {
	code = "";

	/* If one of A B C is not ground, and D is, then jump. */
	addc("NOT A T");
	addc("NOT B J");
	addc("OR J T");
	addc("NOT C J");
	addc("OR J T");
	addc("AND D T");
	// copy T to J
	addc("NOT T T");
	addc("NOT T J");

	addc("WALK");
	cres cr = runcode(proc, code);
	cout << "star1 " << cr.code << endl;
	cout << cr.pic << endl;
}

void solve2(prg proc) {
	code = "";

	addc("NOT T J");
	addc("AND J T");
	addc("NOT T J");	// J==true

	addc("AND A J");
	addc("AND B J");
	addc("AND C J");
	addc("NOT J J");	// J=="any hole in ABC"
	addc("AND D J");	// J=="any hole in ABC" and D is ground

	addc("OR H T");	// J&= ( H || E)
	addc("OR E T");
	addc("AND T J");

	addc("RUN");
	cres cr = runcode(proc, code);
	cout << "star2 " << cr.code << endl;
	if (cr.code < 255)
		cout << cr.pic << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cins(s);
	prg proc(s);
	solve(proc);
	solve2(proc);
}

