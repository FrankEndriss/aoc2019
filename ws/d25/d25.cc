/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
/*
 #ifndef DEBUG
 #define endl "\n"
 #endif
 */

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename T>
void atoli(string &s, T &m) {
	for (size_t i = 0; i < s.size(); i++)
		if (s[i] != '-' && (s[i] < '0' || s[i] > '9'))
			s[i] = ' ';

	istringstream iss(s);
	ll idx = 0;
	while (iss.good()) {
		ll aux;
		iss >> aux;
		m[idx] = aux;
		idx++;
	}
}

/* Incode computer, see https://adventofcode.com/2019/day/9
 * This implementation works with mapped memory, ie memory
 * is kind of infinite, and can have negative addresses, too.
 */
struct prg {
	ll pc = 0;
	ll reloffs = 0;
	map<ll, ll> a;   // memory

	queue<ll> iq;   // stdin
	queue<ll> oq;   // stdout

	ll ic = -1;	// instruction counter

	/* Debug utility. Set cb=0 to switch off.
	 * Implement for breakpoint etc. */
	function<bool(prg&, int, int)> cb = [](prg &pProc, int pIc, int pRic) {
		ll opcode = pProc.a[pProc.pc] % 100;
		ll p1 = (pProc.a[pProc.pc] / 100) % 10;
		ll p2 = (pProc.a[pProc.pc] / 1000) % 10;
		ll p3 = (pProc.a[pProc.pc] / 10000) % 10;
		cout << "ic=" << right << setw(4) << pProc.ic << "  a[pc]=" << setw(5) << pProc.a[pProc.pc] << "  a[+1]=" << setw(5) << pProc.a[pProc.pc + 1]
				<< "  a[+2]=" << setw(5) << pProc.a[pProc.pc + 2] << "  a[+3]=" << setw(5) << pProc.a[pProc.pc + 3] << " opc=" << opcode << " p1=" << p1
				<< " p2=" << p2 << " p3=" << p3 << endl;
		return true;
	};

	/* Give memory/program as parameter, an arbitrary list of integers. */
	prg(string s) {
		atoli(s, a);
	}

	ll addr(ll lpos, ll mode) {
		ll ret;
		if (mode == 0)
			ret = a[lpos];
		else if (mode == 2)
			ret = a[lpos] + reloffs;
		else
			assert(false);
		return ret;
	}
	;

	ll lload(ll lpos, ll mode) {
		ll ret;
		if (mode == 0)
			ret = a[a[lpos]];
		else if (mode == 2)
			ret = a[a[lpos] + reloffs];
		else
			ret = a[lpos];
		return ret;
	}
	;

	/* runs proc reading input from iq and writes output to oq.
	 * @return true after halt/99 instruction, else
	 * false on waiting for input.
	 */
	bool run() {
		ll ric = -1;
		while (a[pc] != 99) {
			ic++;
			ric++;
			if (cb && !cb(*this, ic, ric))
				return true;	// halted by debugger

			ll opcode = a[pc] % 100;
			ll p1 = (a[pc] / 100) % 10;
			ll p2 = (a[pc] / 1000) % 10;
			ll p3 = (a[pc] / 10000) % 10;

#ifdef DEBUG
//			cout << "prg: a[pc]=" << a[pc] << " a[pc+1]=" << a[pc + 1] << " a[pc+2]=" << a[pc + 2] << " a[pc+3]=" << a[pc + 3] << " opcode=" << opcode << " p1="
//					<< p1 << " p2=" << p2 << " p3=" << p3 << endl;
#endif

			if (opcode == 1) { // add
				ll sto = addr(pc + 3, p3);
				a[sto] = lload(pc + 1, p1) + lload(pc + 2, p2);
				pc += 4;
			} else if (opcode == 2) { // mul
				ll sto = addr(pc + 3, p3);
				a[sto] = lload(pc + 1, p1) * lload(pc + 2, p2);
				pc += 4;
			} else if (opcode == 3) {  // input
				if (iq.size() == 0) {
#ifdef DEBUG
					cout << "prg: input size==0, wait for input return" << endl;
#endif
					return false;
				}

				ll aux = iq.front();
				iq.pop();
				assert(aux<=INT_MAX && aux>=INT_MIN);
				ll sto = addr(pc + 1, p1);
				a[sto] = aux;
				pc += 2;
			} else if (opcode == 4) { // output
				ll outp = lload(pc + 1, p1);
				oq.push(outp);
#ifdef DEBUG
				cout << "prg: outp: " << outp << endl;
#endif
				pc += 2;
			} else if (opcode == 5) {  // jump if true
				if (lload(pc + 1, p1))
					pc = lload(pc + 2, p2);
				else
					pc += 3;
			} else if (opcode == 6) { // jump if false
				if (!lload(pc + 1, p1))
					pc = lload(pc + 2, p2);
				else
					pc += 3;
			} else if (opcode == 7) {  // less than
				ll v = lload(pc + 1, p1) < lload(pc + 2, p2);
				ll sto = addr(pc + 3, p3);
				a[sto] = v;
				pc += 4;
			} else if (opcode == 8) {  // equals
				ll v = lload(pc + 1, p1) == lload(pc + 2, p2);
				ll sto = addr(pc + 3, p3);
				a[sto] = v;
				pc += 4;
			} else if (opcode == 9) {
				ll v = lload(pc + 1, p1);
				reloffs += v;
				pc += 2;
			} else {
				cout << "unknown opcode: " << opcode << endl;
				assert(false);
			}
		}
#ifdef DEBUG
		cout << "prg: halt" << endl;
#endif
		return true;
	}

	/* ascii interface */
	void writeln(string line) {
		for (char c : line)
			iq.push(c);
		iq.push(10);
	}

	string readln() {
		string ans = "";
		while (oq.size()) {
			int d = oq.front();
			oq.pop();
			if (d == 10)
				break;
			ans += (char) d;
		}
		return ans;
	}
	/* true if next output char 1..255 */
	bool nextAscii() {
		return oq.size() && oq.front() > 0 && oq.front() < 256;
	}
};

map<pii, char> m;

#define Y first
#define X second

string opDoor(string door) {
	if (door == "north")
		return "south";
	else if (door == "south")
		return "north";
	else if (door == "west")
		return "east";
	else if (door == "east")
		return "west";
	else
		assert(false);
}

/* pathes from start */
map<string, string> locs = { { "Hull Breach", "" }, { "Engeneering", "S" }, { "Sick bay", "SS" }, { "Hall way", "SSSS" }, { "Stables", "W" },
		{ "Kitchen", "N" }, { "Hot Choc", "NW" }, { "Science Lab", "NN" }, { "Corridor", "NNW" }, { "Observatory", "NNN" }, { "Gift Wrapping Center", "NNNN" },
		{ "Holodeck", "NNNNN" }, { "Passage", "NNNW" }, { "Warp drive", "NNNWS" }, { "Navigation", "NNNWSS" }, { "Arcade", "NNNWSW" }, { "Security checkpoint",
				"NNNWSWWS" }, { "PS Floor", "NNNWSWWSE" } };

map<char, string> dirs = { { 'N', "north" }, { 'S', "south" }, { 'W', "west" }, { 'E', "east" } };

string rev(string s) {
	string ret = s;
	reverse(ret.begin(), ret.end());
	for (size_t i = 0; i < ret.size(); i++)
		if (ret[i] == 'N')
			ret[i] = 'S';
		else if (ret[i] == 'S')
			ret[i] = 'N';
		else if (ret[i] == 'E')
			ret[i] = 'W';
		else if (ret[i] == 'W')
			ret[i] = 'E';
		else
			assert(false);

	return ret;
}

void run(string path, prg &proc, bool back = true) {
	for (char c : path) {
		proc.writeln(dirs[c]);
	}
	if (back) {
		run(rev(path), proc, false);
	}
}

void collect(string loc, string item, prg &proc, bool back = true) {
	run(locs[loc], proc, false);
	if (item.size() > 0)
		proc.writeln("take " + item);
	if (back)
		run(rev(locs[loc]), proc, false);
}

vs rooms = { "Stables", "Kitchen", "Engeneering", "Warp drive", "Science Lab", "Arcade", "Passage", "Hall way" };
vs citems = { "pointer", "coin", "whirled peas", "klein bottle", "astronaut ice cream", "mutex", "dark matter", "festive hat" };

bool guess(prg proc, int bitems) {
	for (size_t i = 0; i < rooms.size(); i++)
		collect(rooms[i], citems[i], proc);

	for (size_t i = 0; i < rooms.size(); i++) {
		if (bitems & (1 << i))
			proc.writeln("drop " + citems[i]);
	}
	collect("PS Floor", "", proc, false);

	proc.run();
	while (proc.nextAscii()) {
		string s = proc.readln();
		cout << s << endl;
		if (s == "A loud, robotic voice says \"Alert! Droids on this ship are lighter than the detected value!\" and you are ejected back to the checkpoint.")
			return false;
	}
	return true;
}

/* to solve the game we need to collect certain items, then go to  room "PS Floor".
 * If collected the right combination of items, this will then output the
 * solution code.
 * We need to detect existing rooms, items and paths to rooms by hand, then run this driver.
 * It tries all possible combinations of items.
 */
void solve(prg proc) {
	m.clear();
	proc.cb = 0;

	int g = 0;
	while (!guess(proc, g))
		g++;

	/* dont collect("Sick bay", "giant electromagnet", proc, false); // cant move!!! */
	/* dont collect("Gift Wrapping Center", "infinite loop", proc); // loop */
	/* dont collect("Holodeck", "molten lava", proc, false); // to hot */
	/* dont collect("Corridor", "photons", proc, false); // eaten by grue */
	/* dont collect("Observatory", "escape pod", proc, false); // shoot to space */

	/*
	 collect("Stables", "pointer", proc);
	 collect("Kitchen", "coin", proc);
	 collect("Engeneering", "whirled peas", proc);
	 collect("Warp drive", "klein bottle", proc);
	 collect("Science Lab", "astronaut ice cream", proc);
	 collect("Arcade", "mutex", proc);
	 collect("Passage", "dark matter", proc);
	 collect("Hall way", "festive hat", proc);
	 */

//collect("PS Floor", "", proc, false);
// TODO Hall way, optimized for something ???
// TODO Arcade, playing not possible without power
// TODO run(locs["PS Floor"], proc, false);
}

int main() {
	cins(s);
	prg proc(s);
	solve(proc);
}

