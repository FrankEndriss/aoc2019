/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename T>
void atoli(string &s, T &m) {
	for (size_t i = 0; i < s.size(); i++)
		if (s[i] != '-' && (s[i] < '0' || s[i] > '9'))
			s[i] = ' ';

	istringstream iss(s);
	ll idx = 0;
	while (iss.good()) {
		ll aux;
		iss >> aux;
		m[idx] = aux;
		idx++;
	}
}

/* Incode computer, see https://adventofcode.com/2019/day/9
 * This implementation works with mapped memory, ie memory
 * is kind of infinite, and can have negative addresses, too.
 */
struct prg {
	ll pc = 0;
	ll reloffs = 0;
	map<ll, ll> a;   // memory

	queue<ll> iq;   // stdin
	queue<ll> oq;   // stdout

	ll ic = -1;	// instruction counter

	/* Debug utility. Set cb=0 to switch off.
	 * Implement for breakpoint etc. */
	function<bool(prg&, int, int)> cb = [](prg &pProc, int pIc, int pRic) {
		ll opcode = pProc.a[pProc.pc] % 100;
		ll p1 = (pProc.a[pProc.pc] / 100) % 10;
		ll p2 = (pProc.a[pProc.pc] / 1000) % 10;
		ll p3 = (pProc.a[pProc.pc] / 10000) % 10;
		cout << "ic=" << right << setw(4) << pProc.ic << "  a[pc]=" << setw(5) << pProc.a[pProc.pc] << "  a[+1]=" << setw(5) << pProc.a[pProc.pc + 1]
				<< "  a[+2]=" << setw(5) << pProc.a[pProc.pc + 2] << "  a[+3]=" << setw(5) << pProc.a[pProc.pc + 3] << " opc=" << opcode << " p1=" << p1
				<< " p2=" << p2 << " p3=" << p3 << endl;
		return true;
	};

	/* Give memory/program as parameter, an arbitrary list of integers. */
	prg(string s = "") {
		atoli(s, a);
	}

	ll addr(ll lpos, ll mode) {
		ll ret;
		if (mode == 0)
			ret = a[lpos];
		else if (mode == 2)
			ret = a[lpos] + reloffs;
		else
			assert(false);
		return ret;
	}
	;

	ll lload(ll lpos, ll mode) {
		ll ret;
		if (mode == 0)
			ret = a[a[lpos]];
		else if (mode == 2)
			ret = a[a[lpos] + reloffs];
		else
			ret = a[lpos];
		return ret;
	}
	;

	/* runs proc reading input from iq and writes output to oq.
	 * @return true after halt/99 instruction, else
	 * false on waiting for input.
	 */
	bool run() {
		ll ric = -1;
		while (a[pc] != 99) {
			ic++;
			ric++;
			if (cb && !cb(*this, ic, ric))
				return true;	// halted by debugger

			ll opcode = a[pc] % 100;
			ll p1 = (a[pc] / 100) % 10;
			ll p2 = (a[pc] / 1000) % 10;
			ll p3 = (a[pc] / 10000) % 10;

			if (opcode == 1) { // add
				ll sto = addr(pc + 3, p3);
				a[sto] = lload(pc + 1, p1) + lload(pc + 2, p2);
				pc += 4;
			} else if (opcode == 2) { // mul
				ll sto = addr(pc + 3, p3);
				a[sto] = lload(pc + 1, p1) * lload(pc + 2, p2);
				pc += 4;
			} else if (opcode == 3) {  // input
				if (iq.size() == 0) {
#ifdef DEBUG
					cout << "prg: input size==0, wait for input return" << endl;
#endif
					return false;
				}

				ll aux = iq.front();
				iq.pop();
				//assert(aux<=INT_MAX && aux>=INT_MIN);
				ll sto = addr(pc + 1, p1);
				a[sto] = aux;
				pc += 2;
			} else if (opcode == 4) { // output
				ll outp = lload(pc + 1, p1);
				oq.push(outp);
#ifdef DEBUG
				cout << "prg: outp: " << outp << endl;
#endif
				pc += 2;
			} else if (opcode == 5) {  // jump if true
				if (lload(pc + 1, p1))
					pc = lload(pc + 2, p2);
				else
					pc += 3;
			} else if (opcode == 6) { // jump if false
				if (!lload(pc + 1, p1))
					pc = lload(pc + 2, p2);
				else
					pc += 3;
			} else if (opcode == 7) {  // less than
				ll v = lload(pc + 1, p1) < lload(pc + 2, p2);
				ll sto = addr(pc + 3, p3);
				a[sto] = v;
				pc += 4;
			} else if (opcode == 8) {  // equals
				ll v = lload(pc + 1, p1) == lload(pc + 2, p2);
				ll sto = addr(pc + 3, p3);
				a[sto] = v;
				pc += 4;
			} else if (opcode == 9) {
				ll v = lload(pc + 1, p1);
				reloffs += v;
				pc += 2;
			} else {
				cout << "unknown opcode: " << opcode << endl;
				assert(false);
			}
		}
#ifdef DEBUG
		cout << "prg: halt" << endl;
#endif
		return true;
	}

	/* ascii interface */
	void writeln(string line) {
		for (char c : line)
			iq.push(c);
		iq.push(10);
	}
	string readln() {
		string ans = "";
		while (oq.size()) {
			int d = oq.front();
			oq.pop();
			if (d == 10)
				break;
			ans += (char) d;
		}
		return ans;
	}
	/* true if next output char 1..255 */
	bool nextAscii() {
		return oq.size() && oq.front() > 0 && oq.front() < 256;
	}
};

void solve() {
	cins(s);
	prg p(s);
	p.cb = 0;
	vector<prg> procs;
	for (int i = 0; i < 50; i++) {
		procs.push_back(p);
		procs[i].iq.push(i);
		procs[i].iq.push(-1);
	}

	bool done = false;
	ll ans = -1;
	ll natx = -1;
	ll naty = -1;
	ll pnaty = -1;
	while (!done) {
		bool qEmpty = true;
		for (int i = 0; i < 50 && !done; i++) {
			if (procs[i].iq.size())
				qEmpty = false;

			if (procs[i].iq.size())
				procs[i].run();

			while (!done && procs[i].oq.size()) {
				qEmpty = false;
				int addr = procs[i].oq.front();
				procs[i].oq.pop();
				ll x = procs[i].oq.front();
				procs[i].oq.pop();
				ll y = procs[i].oq.front();
				procs[i].oq.pop();

				//cout << "pack from=" << i << " to=" << addr << " x=" << x << " y=" << y << endl;
				if (addr == 255) {
					if (ans == -1)	// star1
						ans = y;

					natx = x;
					naty = y;
				} else {
					procs[addr].iq.push(x);
					procs[addr].iq.push(y);
				}
			}
		}
		if (qEmpty) {
			if (pnaty == naty) {
				done = true;
				break;
			}
			pnaty = naty;
			procs[0].iq.push(natx);
			procs[0].iq.push(naty);
		}
	}
	cout << "star1 " << ans << endl;
	cout << "star2 " << naty << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

