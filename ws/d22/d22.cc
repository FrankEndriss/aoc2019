/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void mygetline(string &s) {
	getline(cin, s);
	while (s.size() && s.back() == 13)
		s.pop_back();
}

ll atol(string &s) {
	istringstream iss(s);
	ll i;
	iss >> i;
	return i;
}

void atoli(string &s, vll &a) {
	istringstream iss(s);
	int i = 0;
	while (iss.good())
		cin >> a[i++];
}

const int N = 10007;
vi deck(10007);

void doCut(int n) {
	if (n < 0)
		n += N;

	vi a(N);
	for (int i = 0; i < N; i++)
		a[i] = deck[(i + n) % N];

	a.swap(deck);
}

void doNewStack() {
	reverse(deck.begin(), deck.end());
}

void doWithInc(int n) {
	vi a(N);
	for (ll i = 0; i < N; i++) {
		ll pos = i * n;
		a[pos % N] = deck[i];
	}
	a.swap(deck);
}

void solve() {
	string s;
	iota(deck.begin(), deck.end(), 0);

	while (true) {
		cins(w1);
		if (w1 == "cut") {
			cini(n);
			doCut(n);
		} else if (w1 == "deal") {
			cins(w2);
			if (w2 == "into") {
				cins(w3); // new
				cins(w4); // stack
				doNewStack();
			} else if (w2 == "with") {
				cins(w3);	// increment
				cini(n);
				doWithInc(n);
			}
		} else if (w1 == "end") {
			break;
		} else {
			cout << "parse error, w1=" << w1 << endl;
			assert(false);
		}
	}

	int ans = -1;
	for (int i = 0; i < N; i++)
		if (deck[i] == 2019)
			ans = i;

	cout << "star1 " << ans << endl;
}

const ll MCARDS = 119315717514047;
const ll MSHUFF = 101741582076661;
const int POS = 2020;

struct op {
	int t;	// 0=cut 1=deal into 2=deal with
	int n;	// the number;
};

/** @return position of card at position n before cut */
ll revCut(ll pos, int n) {
	pos += n;
	if (pos < 0)
		pos += MCARDS;
	pos %= MCARDS;
	return pos;
}

ll revInto(ll pos) {
	return MCARDS - pos - 1;
}

ll revWith(ll pos, int n, ll MC = MCARDS) {
	int offs = 0; /* pos of first card per round */
	int offsd = n - (MC % n);

	ll ans = -1;

	// TODO test???
	while (true) {
		//cout << "offs=" << offs << endl;
		const ll mcards = MC - offs;
		if (pos >= offs && (pos - offs) % n == 0) {
			ans += (pos - offs) / n + 1;
			break;
		} else
			ans += mcards / n + 1;

		offs += offsd;
		offs %= n;
		assert(offs != 0);
	}
	return ans;
}

void testRevWith() {
	ll t;
	/*
	 ll t = revWith(0, 10);
	 assert(t == 0);

	 t = revWith(6, 3, 10);
	 assert(t == 2);
	 */

	t = revWith(7, 3, 10);
	cerr << "t=" << t << endl;
	assert(t == 9);

	cout << "did test assert" << endl;
}

void solve2() {
	string s;

	vector<op> ops;
	while (true) {
		cins(w1);
		if (w1 == "cut") {
			cini(n);
			ops.push_back( { 0, n });
		} else if (w1 == "deal") {
			cins(w2);
			if (w2 == "into") {
				cins(w3); // new
				cins(w4); // stack
				ops.push_back( { 1, 0 });
			} else if (w2 == "with") {
				cins(w3);	// increment
				cini(n);
				ops.push_back( { 2, n });
			}
		} else if (w1 == "end") {
			break;
		} else {
			cout << "parse error, w1=" << w1 << endl;
			assert(false);
		}
	}
	reverse(ops.begin(), ops.end());

	ll pos = POS;
	ll prev = pos;
	set<ll> positions;
	set<ll> diffs;
	for (int round = 0; round < 10000000; round++) {
		if (positions.count(pos)) {
			cout << "cycle found in round=" << round << endl;
			break;
		}
		positions.insert(pos);
		prev = pos;

		for (size_t i = 0; i < ops.size(); i++) {
			if (ops[i].t == 0)
				pos = revCut(pos, ops[i].n);
			else if (ops[i].t == 1)
				pos = revInto(pos);
			else if (ops[i].t == 2)
				pos = revWith(pos, ops[i].n);
			else
				assert(false);
		}
		if (diffs.count(prev - pos)) {
			cout << "diff repeats: " << prev - pos << endl;
			break;
		}
		diffs.insert(prev - pos);
	}

	cout << "pos after rev=" << pos << endl;
	assert(pos < MCARDS);

	int ans = 42;
	cout << "star2 " << ans << endl;
}
int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve2();
	//testRevWith();
}

