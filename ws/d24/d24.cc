/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

map<pii, int> m;
set<ll> bio;
ll ans;

const vector<pii> offs = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

bool isBio() {
	int cnt = 0;
	ll b = 0;
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			if (m[ { i, j }])
				b |= (1 << cnt);
			cnt++;
		}
	}
	bool ret = bio.count(b);
	bio.insert(b);
	ans = b;
	return ret;
}

int stepcnt = 0;
void step() {
	stepcnt++;
	map<pii, int> m2;
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			int cnt = 0;
			for (pii p : offs) {
				cnt += m[ { i + p.first, j + p.second }];
			}
			if (m[ { i, j }] && cnt == 1)
				m2[ { i, j }] = 1;
			else if (!m[ { i, j }] && (cnt == 1 || cnt == 2))
				m2[ { i, j }] = 1;
		}
	}
	m = m2;
}

typedef pair<int, pii> cell;
set<cell> mm;
#define L first
#define I second.first
#define J second.second

// inner ==1 , outer==-1
/* get adjents of a cell for star2 definition */
void getAdj(cell c, vector<cell> &ans) {

	// South
	if (c.I == 1 && c.J == 2) {	// cell 8
		for (int i = 0; i < 5; i++)
			ans.push_back( { c.L + 1, { 0, i } });
	} else if (c.I == 4) { // cells U,V,W,X,Y
		ans.push_back( { c.L - 1, { 3, 2 } });
	} else if (c.I + 1 < 5) { // allways <5
		ans.push_back( { c.L, { c.I + 1, c.J } });
	}

	// North
	if (c.I == 3 && c.J == 2) { // cell 18
		for (int i = 0; i < 5; i++)
			ans.push_back( { c.L + 1, { 4, i } });
	} else if (c.I == 0) { // A,B,C,D,E
		ans.push_back( { c.L - 1, { 1, 2 } });
	} else if (c.I - 1 >= 0) {
		ans.push_back( { c.L, { c.I - 1, c.J } });
	}

	// West
	if (c.I == 2 && c.J == 3) { // cell 14
		for (int i = 0; i < 5; i++)
			ans.push_back( { c.L + 1, { i, 4 } });
	} else if (c.J == 0) { // A,F,K,P,U
		ans.push_back( { c.L - 1, { 2, 1 } });
	} else if (c.J - 1 >= 0) {
		ans.push_back( { c.L, { c.I, c.J - 1 } });
	}

	// East
	if (c.I == 2 && c.J == 1) { // cell 12
		for (int i = 0; i < 5; i++)
			ans.push_back( { c.L + 1, { i, 0 } });
	} else if (c.J == 4) { // E,J,O,T,Y
		ans.push_back( { c.L - 1, { 2, 3 } });
	} else if (c.J + 1 < 5) {
		ans.push_back( { c.L, { c.I, c.J + 1 } });
	}

}

int step2cnt = 0;
// inner ==1 , outer==-1

void step2() {
	step2cnt++;
	//cout << "step2(), cnt=" << step2cnt << endl;

	set<cell> adj; /* cells adj to living cells and living cells */

	for (auto c : mm) {
		vector<cell> aux;
		getAdj(c, aux);
		for (auto c2 : aux)
			adj.insert(c2);

		adj.insert(c);
	}

	set<cell> mm2;
	for (auto c : adj) {
		vector<cell> aux;
		getAdj(c, aux);
		int cnt = 0;
		for (auto c2 : aux)
			cnt += mm.count(c2);

		if (mm.count(c) && cnt == 1)
			mm2.insert(c);
		else if (mm.count(c) == 0 && (cnt == 1 || cnt == 2))
			mm2.insert(c);
	}

	mm = mm2;
}

void dump() {
	cout << "step " << stepcnt << endl;
	for (int i = 0; i < 5; i++) {
		for (int j = 0; j < 5; j++) {
			if (m[ { i, j }])
				cout << "#";
			else
				cout << ".";
		}
		cout << endl;
	}
}

void solve() {
	for (int i = 0; i < 5; i++) {
		cins(s);
		assert(s.size() == 5);
		for (int j = 0; j < 5; j++) {
			if (s[j] == '#') {
				m[ { i, j }] = 1;
				mm.insert( { 0, { i, j } });
			}
		}
	}

	while (!isBio()) {
		step();
	}
	cout << "star1 " << ans << endl;

	for (int i = 0; i < 200; i++) {
		//cout << "s2, i=" << i << " mm.size()=" << mm.size() << endl;
		step2();
	}

	cout << "star2 " << mm.size() << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

